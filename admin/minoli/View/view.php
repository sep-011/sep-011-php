<?php

class View
{
    /** To view, delete and send emails to Feedback
     * @param $q - selected option from the drop down
     * @param $q1- row id
     */

    public function Feedback($q , $q1)
    {
        $option = $q;
        $rowId = $q1;

        // if selected option is feedback
        if($option == 'fb' )
        {
        $query = "SELECT * FROM feedback";
        $query1= mysql_query($query);

    ?>
    <!-- list all feedback -->
    <table data-role="table" data-mode="columntoggle" class="ui-responsive ui-shadow" id="myTable" border="4" >

        <tr>
            <th>ID</th>
            <th>Users' like</th>
            <th>About improvement</th>
            <th>Rate</th>
            <th>Action</th>
            <th>Delete</th>
            <th nowrap="nowrap">E-mail</th>
            <th>View</th>

        </tr>

        <?php

        while($row= mysql_fetch_array($query1)) {

            $rate = $row['rate'];
            $action = $row['action'];
            $i =0;
            $g_id =$row['id'];
            $u_id =$row['uid'];
            ?>
            <tr>
                <td><?php echo $row['id'] ;?></td>
                <td><?php echo $row['liketo'] ;?></td>
                <td><?php echo $row['improve'] ;?></td>
                <td nowrap="nowrap">
                    <?php for($i;$i<$rate;$i++)
                    { ?>
                        <img src="images/rate.jpg">
                    <?php
                    } ?>
                </td>
                <td><?php echo $action ;?></td>
                <td>
                    <a href="javascript:deleteF(<?php echo $g_id ?>);">
                        <center> <img src="images/delete.jpg" align="middle"></center>
                    </a>
                </td>
                <td>
                    <a href="javascript:email(<?php echo $u_id ?>);">
                        <center><img src="images/email.jpg" align="middle"></center>
                    </a>
                </td>
                <td>
                    <a href="javascript:viewM(<?php echo $g_id ?>);">
                        <input type="submit" value="View">
                    </a>
                </td>

            </tr>


        <?php
        }

        ?>

    </table>

    <?php
    }
        // delete feedback
        else if($option == 'dl')
    {
        $query2 = "delete FROM feedback where id=$rowId";
        mysql_query($query2);

        $query = "SELECT * FROM feedback";
        $query1= mysql_query($query);

    ?>
        <!-- list all feedback after deleting -->
    <table data-role="table" data-mode="columntoggle" class="ui-responsive ui-shadow" id="myTable" border="4" >

        <tr>
            <th>ID</th>
            <th>Users' like</th>
            <th>About improvement</th>
            <th>Rate</th>
            <th>Action</th>
            <th>Delete</th>
            <th nowrap="nowrap">E-mail</th>
            <th>View</th>

        </tr>

        <?php

        while($row= mysql_fetch_array($query1)) {

            $rate = $row['rate'];
            $action = $row['action'];
            $i =0;
            $g_id =$row['id'];
            $u_id = $row['uid'];
            ?>
            <tr>
                <td><?php echo $row['id'] ;?></td>
                <td><?php echo $row['liketo'] ;?></td>
                <td><?php echo $row['improve'] ;?></td>
                <td nowrap="nowrap">
                    <?php for($i;$i<$rate;$i++)
                    { ?>
                        <img src="images/rate.jpg">
                    <?php
                    } ?>
                </td>
                <td><?php echo $action ;?></td>
                <td>
                    <a href="javascript:deleteF(<?php echo $g_id ?>);">
                        <center><img src="images/delete.jpg" align="middle"></center>
                    </a>
                </td>
                <td>
                    <a href="javascript:email(<?php echo $u_id ?>);">
                        <center><img src="images/email.jpg" align="middle"></center>
                    </a>
                </td>
                <td>
                    <a href="javascript:viewM(<?php echo $g_id ?>);">
                        <input type="submit" value="View">
                    </a>
                </td>

            </tr>


        <?php
        }

        ?>

    </table>
    <?php
    }
        // send emails to feedback
        else if($option == 'em')
{

    $query3 = "select fname,lname,email from userinfo where uid=$rowId";
    $query4= mysql_query($query3);

    $fname ="";
    $lname ="";
    $email ="";

    while($row= mysql_fetch_array($query4))
    {
        $fname =$row['fname'];
        $lname =$row['lname'];
        $email =$row['email'];
    }

    $full = $fname." ".$lname;
    ?>
    <!-- load send email form -->
    <table data-role="table" data-mode="columntoggle" class="ui-responsive ui-shadow" id="myTable" border="4" >
        <tr>
            <td>User</td>
            <td><textarea name="user_name" id="user_name" rows="1" cols="37" readonly><?php echo $full; ?></textarea></td>
        </tr>

        <tr>
            <td>Email</td>
            <td><textarea name="user_email" id="user_email" rows="1" cols="37" readonly><?php  echo $email; ?></textarea></td>
        </tr>

        <tr>
            <td>Subject</td>
            <td><textarea name="user_sub" id="user_sub" rows="1" cols="37"  readonly><?php  echo "Thank you for the feedback"; ?></textarea></td>
        </tr>

        <tr>
            <td>Description</td>
            <td><textarea name="user_des" id="user_des" rows="7" cols="37"></textarea></td>
        </tr>

        <tr>
            <td>

                <input type="submit" value="SUBMIT" onclick="send_email()">
            </td>
            <td>
                <form action="http://sep.esy.es/admin/minoli/mainNavigation.php">
                    <input type="submit" value="CANCEL" >
                </form>
            </td>
        </tr>
    </table>
<?php
}
        // view details of the selected feedback
        else if($option == 'view')
{
    $viewQ = "SELECT * FROM feedback where id=$rowId";
    $viewQr= mysql_query($viewQ);

    $user_id = "";
    $feedback ="";
    $improve ="";
    $rating="";


    while($row= mysql_fetch_array($viewQr))
    {
        $user_id = $row['uid'];
        $feedback = $row['liketo'];
        $improve = $row['improve'];
        $rating= $row['rate'];

    }

    ?>

    <!-- load feedback information form -->
    <table data-role="table" data-mode="columntoggle" class="ui-responsive ui-shadow" id="myTable" border="4" >
        <tr>
            <td>UserID</td>
            <td><input type="text" name="user_name" id="user_name"  value="<?php echo $user_id; ?>" readonly></td>
        </tr>

        <tr>
            <td>User Like</td>
            <td><input type="text" name="user_email" id="user_email"  value="<?php  echo $feedback; ?>" readonly></td>
        </tr>

        <tr>
            <td>Improve</td>
            <td><input type="text" name="user_sub" id="user_sub" value="<?php  echo $improve; ?>" readonly></td>
        </tr>

        <tr>
            <td>Rate</td>
            <td><input type="text" name="user_des" id="user_des"  value="<?php  echo $rating; ?>" readonly></td>
        </tr>

        <tr>

            <td>
                <input type="submit" value="OK" onclick="updateAction(<?php echo $rowId ?>)">
            </td>

        </tr>
    </table>
<?php
}
        // update action as READ for feedback
        else if($option == 'actionF')
{
    $qry = "update feedback set action='Read' where id =$rowId";
    mysql_query($qry);


    $query = "SELECT * FROM feedback";
    $query1= mysql_query($query);

    ?>

    <table data-role="table" data-mode="columntoggle" class="ui-responsive ui-shadow" id="myTable" border="4" >

        <tr>
            <th>ID</th>
            <th>Users' like</th>
            <th>About improvement</th>
            <th>Rate</th>
            <th>Action</th>
            <th>Delete</th>
            <th nowrap="nowrap">E-mail</th>
            <th>View</th>

        </tr>

        <?php

        while($row= mysql_fetch_array($query1)) {

            $rate = $row['rate'];
            $action = $row['action'];
            $i =0;
            $g_id =$row['id'];
            ?>
            <tr>
                <td><?php echo $row['id'] ;?></td>
                <td><?php echo $row['liketo'] ;?></td>
                <td><?php echo $row['improve'] ;?></td>
                <td nowrap="nowrap">
                    <?php for($i;$i<$rate;$i++)
                    { ?>
                        <img src="images/rate.jpg">
                    <?php
                    } ?>
                </td>
                <td><?php echo $action ;?></td>
                <td>
                    <a href="javascript:deleteF(<?php echo $g_id ?>);">
                        <center><img src="images/delete.jpg" align="middle"></center>
                    </a>
                </td>
                <td>
                    <a href="javascript:email(<?php echo $row['uid']?>);">
                        <center><img src="images/email.jpg" align="middle"></center>
                    </a>
                </td>
                <td>
                    <a href="javascript:viewM(<?php echo $g_id ?>);">
                        <input type="submit" value="View">
                    </a>
                </td>

            </tr>


        <?php
        }

        ?>

    </table>
<?php  }
        else{
        //
        }


    }


    /** To view, delete and send emails to reported problems
     * @param $q - selected option from the drop down
     * @param $q1- row id
     */
    public function Report($q , $q1)
    {

        $option = $q;
        $rowId = $q1;

        // if selected option is reported problem
        if($option == 'rp' )
        {
            $query = "SELECT * FROM reportproblem";
            $query1= mysql_query($query);

        ?>
        <!-- list all reported problems -->
        <table data-role="table" data-mode="columntoggle" class="ui-responsive ui-shadow" id="myTable" border="4" >

        <tr>
            <th>ID</th>
            <th>Problem</th>
            <th>Description</th>
            <th>URL</th>
            <th>Action</th>
            <th>Delete</th>
            <th nowrap="nowrap">E-mail</th>
            <th>View</th>

        </tr>

        <?php

        while($row= mysql_fetch_array($query1)) {


            $action = $row['action'];
            $g_id =$row['uid'];
            $id= $row['id'];
            ?>
            <tr>
                <td><?php echo $row['id'] ;?></td>
                <td><?php echo $row['optionval'] ;?></td>
                <td><?php echo $row['details'] ;?></td>
                <td nowrap="nowrap"><?php echo $row['url'] ;  ?> </td>
                <td><?php echo $action ;?></td>
                <td>
                    <a href="javascript:deleteRep(<?php echo $id ?>);">
                        <center> <img src="images/delete.jpg" align="middle"></center>
                    </a>
                </td>
                <td>
                    <a href="javascript:emailRep(<?php echo $g_id ?>);">
                        <center><img src="images/email.jpg" align="middle"></center>
                    </a>
                </td>
                <td>
                    <a href="javascript:viewR(<?php echo $id ?>);">
                        <input type="submit" value="View">
                    </a>
                </td>
            </tr>


        <?php
        }

        ?>

    </table>
        <?php
        }
        // delete reported problem
        else if($option == 'dl')
{
    $query2 = "delete FROM  reportproblem where id=$rowId";
    mysql_query($query2);

    $query = "SELECT * FROM reportproblem";
    $query1= mysql_query($query);

    ?>
    <!-- list all reported problem after deleting -->
    <table data-role="table" data-mode="columntoggle" class="ui-responsive ui-shadow" id="myTable" border="4" >

        <tr>
            <th>ID</th>
            <th>Problem</th>
            <th>Description</th>
            <th>URL</th>
            <th>Action</th>
            <th>Delete</th>
            <th nowrap="nowrap">E-mail</th>
            <th>View</th>


        </tr>

        <?php

        while($row= mysql_fetch_array($query1)) {

            $action = $row['action'];
            $g_id = $row['uid'];
            $id = $row['id'];
            ?>
            <tr>
                <td><?php echo $row['id'] ;?></td>
                <td><?php echo $row['optionval'] ;?></td>
                <td><?php echo $row['details'] ;?></td>
                <td nowrap="nowrap"><?php echo $row['url'] ;  ?> </td>
                <td><?php echo $action ;?></td>
                <td>
                    <a href="javascript:deleteRep(<?php echo $id ?>);">
                        <center><img src="images/delete.jpg" align="middle"></center>
                    </a>
                </td>
                <td>
                    <a href="javascript:emailRep(<?php echo $g_id ?>);">
                        <center><img src="images/email.jpg" align="middle"></center>
                    </a>
                </td>
                <td>
                    <a href="javascript:viewR(<?php echo $id ?>);">
                        <input type="submit" value="View">
                    </a>
                </td>

            </tr>


        <?php
        }

        ?>

    </table>
<?php
}
        // send emails to reported problem
        else if($option == 'em')
{

    $query3 = "select fname,lname,email from userinfo where uid=$rowId";
    $query4= mysql_query($query3);

    $fname ="";
    $lname ="";
    $email ="";

    while($row= mysql_fetch_array($query4))
    {
        $fname =$row['fname'];
        $lname =$row['lname'];
        $email =$row['email'];
    }

    $full = $fname." ".$lname;
    ?>
    <!-- load send email form -->
    <table data-role="table" data-mode="columntoggle" class="ui-responsive ui-shadow" id="myTable" border="4" >
        <tr>
            <td>User</td>
            <td><textarea name="user_name" id="user_name" rows="1" cols="37" readonly><?php echo $full; ?></textarea></td>
        </tr>

        <tr>
            <td>Email</td>
            <td><textarea name="user_email" id="user_email" rows="1" cols="37" readonly><?php  echo $email; ?></textarea></td>
        </tr>

        <tr>
            <td>Subject</td>
            <td><textarea name="user_sub" id="user_sub" rows="1" cols="37"  readonly><?php  echo "Thank you for the comment"; ?></textarea></td>
        </tr>

        <tr>
            <td>Description</td>
            <td><textarea name="user_des" id="user_des" rows="7" cols="37"></textarea></td>
        </tr>

        <tr>
            <td>

                <input type="submit" value="SUBMIT" onclick="send_email()">
            </td>
            <td>
                <form action="http://sep.esy.es/admin/minoli/mainNavigation.php">
                    <input type="submit" value="CANCEL" >
                </form>
            </td>
        </tr>
    </table>
<?php
}
        // view details of the selected reported problem
        else if($option == 'view')
{
    $viewQ = "SELECT * FROM reportproblem where id=$rowId";
    $viewQr= mysql_query($viewQ);

    $user_id = "";
    $problem ="";
    $description ="";
    $url ="";

    while($row= mysql_fetch_array($viewQr))
    {
        $user_id = $row['uid'];
        $problem = $row['optionval'];
        $description = $row['details'];
        $url= $row['url'];

    }

    ?>
    <!-- load reported problem information form -->
    <table data-role="table" data-mode="columntoggle" class="ui-responsive ui-shadow" id="myTable" border="4" >
        <tr>
            <td>UserID</td>
            <td><input type="text" name="user_name" id="user_name"  value="<?php echo $user_id; ?>" readonly></td>
        </tr>

        <tr>
            <td>Problem</td>
            <td><input type="text" name="user_email" id="user_email"  value="<?php  echo $problem; ?>" readonly></td>
        </tr>

        <tr>
            <td>Description</td>
            <td><input type="text" name="user_sub" id="user_sub" value="<?php  echo $description; ?>" readonly></td>
        </tr>

        <tr>
            <td>URL</td>
            <td><input type="text" name="user_des" id="user_des"  value="<?php  echo $url; ?>" readonly></td>
        </tr>

        <tr>

            <td>
                <input type="submit" value="OK" onclick="updateActionReport(<?php echo $rowId ?>)">
            </td>

        </tr>
    </table>
<?php
}
        // update action as READ for reported problem
        else if($option == 'actionF')
{

    $qry = "update reportproblem set action='Read' where id =$rowId";
    mysql_query($qry);


    $query = "SELECT * FROM reportproblem";
    $query1= mysql_query($query);

    ?>

    <table data-role="table" data-mode="columntoggle" class="ui-responsive ui-shadow" id="myTable" border="4" >

        <tr>
            <th>ID</th>
            <th>Problem</th>
            <th>Description</th>
            <th>URL</th>
            <th>Action</th>
            <th>Delete</th>
            <th nowrap="nowrap">E-mail</th>
            <th>View</th>

        </tr>

        <?php

        while($row= mysql_fetch_array($query1)) {


            $action = $row['action'];
            $g_id =$row['uid'];
            $id= $row['id'];
            ?>
            <tr>
                <td><?php echo $row['id'] ;?></td>
                <td><?php echo $row['optionval'] ;?></td>
                <td><?php echo $row['details'] ;?></td>
                <td nowrap="nowrap"><?php echo $row['url'] ;  ?> </td>
                <td><?php echo $action ;?></td>
                <td>
                    <a href="javascript:deleteRep(<?php echo $id ?>);">
                        <center> <img src="images/delete.jpg" align="middle"></center>
                    </a>
                </td>
                <td>
                    <a href="javascript:emailRep(<?php echo $g_id ?>);">
                        <center><img src="images/email.jpg" align="middle"></center>
                    </a>
                </td>
                <td>
                    <a href="javascript:viewR(<?php echo $id ?>);">
                        <input type="submit" value="View">
                    </a>
                </td>

            </tr>


        <?php
        }

        ?>

    </table>
<?php
}
        else
{

}



    }

    /** To view and delete locations
     * @param $value1 - selected option from the drop down
     * @param $value2- location id
     */
    public function ViewPlaces($value1, $value2)
    {

        $glob =0;
        // view all locations
        if( $value1=='mainfun')
        {
            $query = "SELECT * FROM locations";
            $query1= mysql_query($query);
            ?>

            <table data-role="table" data-mode="columntoggle" class="ui-responsive ui-shadow" id="myTable" border="4" >

                <tr>
                    <th>ID</th>
                    <th>Name</th>
                    <!--   <th>Description</th> -->
                    <th>District</th>
                    <th>Area</th>
                    <th>Image</th>
                    <th nowrap="nowrap">Latitude</th>
                    <th nowrap="nowrap">Longitude</th>
                    <th>URL</th>
                    <th>Delete</th>
                    <th>Edit</th>

                </tr>
                <?php
                while($row= mysql_fetch_array($query1))
                {
                    $id = $row['Location_id'];
                    ?>
                    <tr>
                        <td><?php echo $id ;?></td>
                        <td><?php echo $row['Location_Name'] ;?></td>
                        <!--   <td><?php echo $row['Description'] ;?></td> -->
                        <td><?php echo $row['District'] ;?></td>
                        <td><?php echo $row['Area'] ;?></td>
                        <td><img src="<?php echo $row['Image'] ;?>" width="300" height="250"></td>
                        <td><?php echo $row['latitude'] ;?></td>
                        <td><?php echo $row['longitude'] ;?></td>
                        <td><a href="<?php echo $row['detail_url'] ;?>"><?php echo $row['detail_url'] ;?></a></td>
                        <td> <a href="javascript:deleteLoc(<?php echo $id ?>);">
                                <center> <img src="images/delete.jpg" align="middle"></center>
                            </a>
                        </td>
                        <td> <a href="javascript:EditLoc(<?php echo $id ?>);">
                                <center> <img src="images/edit.jpg" align="middle"></center>
                            </a>
                        </td>

                    </tr>

                <?php

                }
                ?>


            </table>
        <?php
        }
        // delete selected location
        else if($value1 == 'dloc')
        {

            $query2 = "delete FROM locations where Location_id=$value2";
            mysql_query($query2);

            $query = "SELECT * FROM locations";
            $query1= mysql_query($query);
            ?>

            <table data-role="table" data-mode="columntoggle" class="ui-responsive ui-shadow" id="myTable" border="4" >

                <tr>
                    <th>ID</th>
                    <th>Name</th>
                    <!--   <th>Description</th> -->
                    <th>District</th>
                    <th>Area</th>
                    <th>Image</th>
                    <th nowrap="nowrap">Latitude</th>
                    <th nowrap="nowrap">Longitude</th>
                    <th>URL</th>
                    <th>Delete</th>
                    <th>Edit</th>


                </tr>
                <?php
                while($row= mysql_fetch_array($query1))
                {
                    $id = $row['Location_id'];
                    ?>
                    <tr>
                        <td><?php echo $id ;?></td>
                        <td><?php echo $row['Location_Name'] ;?></td>
                        <!--   <td><?php echo $row['Description'] ;?></td> -->
                        <td><?php echo $row['District'] ;?></td>
                        <td><?php echo $row['Area'] ;?></td>
                        <td><img src="<?php echo $row['Image'] ;?>" width="300" height="250"></td>
                        <td><?php echo $row['latitude'] ;?></td>
                        <td><?php echo $row['longitude'] ;?></td>
                        <td><a href="<?php echo $row['detail_url'] ;?>"><?php echo $row['detail_url'] ;?></a></td>
                        <td> <a href="javascript:deleteLoc(<?php echo $id ?>);">
                                <center> <img src="images/delete.jpg" align="middle"></center>
                            </a>
                        </td>
                        <td> <a href="javascript:EditLoc(<?php echo $id ?>);">
                                <center> <img src="images/edit.jpg" align="middle"></center>
                            </a>
                        </td>

                    </tr>

                <?php

                }
                ?>


            </table>
        <?php

        }
        // load edit location form
        else if($value1 == 'edit')
        {
            $query3 = "select * from locations where Location_id=$value2";
            $query4= mysql_query($query3);

            while($row= mysql_fetch_array($query4))
            { $glob = $row['Location_id'];
                ?>
                <table data-role="table" data-mode="columntoggle" class="ui-responsive ui-shadow" id="myTable" border="4" >
                    <tr>
                        <td>ID</td>
                        <td><textarea name="loc_id" id="loc_id" rows="1" cols="50" readonly><?php echo $row['Location_id']; ?></textarea></td>
                    </tr>

                    <tr>
                        <td>Name</td>
                        <td><textarea name="loc_name" id="loc_name" rows="1" cols="50"><?php  echo $row['Location_Name']; ?></textarea></td>
                    </tr>

                    <tr>
                        <td>Description</td>
                        <td><textarea name="loc_des" id="loc_des" rows="10" cols="50" ><?php  echo $row['Description']; ?></textarea></td>
                    </tr>

                    <tr>
                        <td>District</td>
                        <td><textarea name="loc_dis" id="loc_dis" rows="1" cols="50"><?php echo $row['District'] ;?></textarea></td>
                    </tr>
                    <tr>
                        <td>Area</td>
                        <td><textarea name="loc_area" id="loc_area" rows="1" cols="50" ><?php echo $row['Area'] ;?></textarea></td>
                    </tr>

                    <tr>
                        <td>Image</td>
                        <td><img src="<?php echo $row['Image'] ;?>" width="300" height="250"></td>

                    </tr>

                    <tr>
                        <td>Latitude</td>
                        <td><textarea name="loc_lat" id="loc_lat" rows="1" cols="50"  ><?php echo $row['latitude'] ;?></textarea></td>
                    </tr>
                    <tr>
                        <td>Longitude</td>
                        <td><textarea name="loc_long" id="loc_long" rows="1" cols="50"  ><?php echo $row['longitude'] ;?></textarea></td>
                    </tr>

                    <tr>
                        <td>URL</td>
                        <td><textarea name="loc_url" id="loc_url" rows="1" cols="50"><?php echo $row['detail_url'] ;?></textarea></td>
                    </tr>




                    <tr>
                        <td>

                            <input type="submit" value="SUBMIT" onclick="upload_location(<?php echo $row['Location_id']; ?>)">
                        </td>
                        <td>
                            <form action="mainRedirectPlace.php">
                                <input type="submit" value="CANCEL" >
                            </form>
                        </td>
                    </tr>
                </table>
            <?php } ?>
            <br>
            <br>

            <form action="upload.php" method="post" enctype="multipart/form-data">
                Select image to upload:
                <input type="file" name="fileToUpload" id="fileToUpload">
                <input name="glob" value="<?php echo $glob ?>" type="hidden">
                <input type="submit" value="Upload Image" name="submit">
            </form
    <?php

}
        else
        {
        //blank
        }


    }

    /** To update locations
     * @param $id - location id
     * @param $name- location name
     * @param $description - location description
     * @param $dist- district of the location
     * @param $area- area of the location
     * @param $latitude - latitude of the location
     * @param $longitude - longitude of the location
     * @param $url - location url
     */
    public function UpdatePlaces($id,$name,$description,$dist,$area,$latitude,$longitude,$url)
    {
        $query5 = "update locations set Location_Name='$name', Description='$description', District='$dist', Area='$area', latitude=$latitude, longitude=$longitude, detail_url='$url' where Location_id=$id";
        mysql_query($query5);

        $query = "SELECT * FROM locations";
        $query1= mysql_query($query);
        ?>

        <table data-role="table" data-mode="columntoggle" class="ui-responsive ui-shadow" id="myTable" border="4" >

            <tr>
                <th>ID</th>
                <th>Name</th>
                <!--  <th>Description</th> -->
                <th>District</th>
                <th>Area</th>
                <th>Image</th>
                <th nowrap="nowrap">Latitude</th>
                <th nowrap="nowrap">Longitude</th>
                <th>URL</th>
                <th>Edit</th>
                <th>Delete</th>

            </tr>
            <?php
            while($row= mysql_fetch_array($query1))
            {
                $id = $row['Location_id'];
                ?>
                <tr>
                    <td><?php echo $id ;?></td>
                    <td><?php echo $row['Location_Name'] ;?></td>
                    <!--   <td><?php echo $row['Description'] ;?></td> -->
                    <td><?php echo $row['District'] ;?></td>
                    <td><?php echo $row['Area'] ;?></td>
                    <td><img src="<?php echo $row['Image'] ;?>" width="300" height="250"></td>
                    <td><?php echo $row['latitude'] ;?></td>
                    <td><?php echo $row['longitude'] ;?></td>
                    <td><a href="<?php echo $row['detail_url'] ;?>"><?php echo $row['detail_url'] ;?></a></td>
                    <td> <a href="javascript:deleteLoc(<?php echo $id ?>);">
                            <center> <img src="images/delete.jpg" align="middle"></center>
                        </a>
                    </td>
                    <td> <a href="javascript:EditLoc(<?php echo $id ?>);">
                            <center> <img src="images/edit.jpg" align="middle"></center>
                        </a>
                    </td>

                </tr>

            <?php

            }
            ?>


        </table>
    <?php

    }


}