<html>
<head>
    <link href="css/index.css" rel="stylesheet" />
    <script type="text/javascript" src="scripts/ad_up_places.js"></script>
    <link rel="stylesheet" href="http://code.jquery.com/mobile/1.4.5/jquery.mobile-1.4.5.min.css">
</head>
<body>

<?php
// if submit button and upload file is selected
if(isset($_POST["submit"] ) && !empty($_FILES["fileToUpload"]["tmp_name"]))
{
    $name = $_POST['loc_name'];
    $des = $_POST['loc_des'];
    $dis = $_POST['loc_dis'];
    $area = $_POST['loc_area'];
    $lat = $_POST['loc_lat'];
    $long = $_POST['loc_long'];
    $url = $_POST['loc_url'];


    $target_dir = "../../sep/images/";
    $target_file = $target_dir . basename($_FILES["fileToUpload"]["name"]);
    $uploadOk = 1;
    $errormsg = "";

    $imageFileType = pathinfo($target_file,PATHINFO_EXTENSION);

    // Check if image file is a actual image or fake image
    $check = getimagesize($_FILES["fileToUpload"]["tmp_name"]);

    if($check !== false) {

        $uploadOk = 1;
    }
    else {
        $errormsg = "File is not an image!!!\\n" .$errormsg;
        $uploadOk = 0;
    }
    // Check if file already exists
    if (file_exists($target_file)) {
        $errormsg = "File already exists.!!!\\n" .$errormsg;
        $uploadOk = 0;
    }
    // Check file size
    if ($_FILES["fileToUpload"]["size"] > 500000) {
        $errormsg = "Your file is too large!!\\n".$errormsg;
        $uploadOk = 0;
    }
    // Allow certain file formats
    if($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg" && $imageFileType != "gif" && $imageFileType != "PNG" ) {
        $errormsg = "Only JPG, JPEG, PNG & GIF files are allowed.!!!\\n".$errormsg;
        $uploadOk = 0;
    }
    // Check if $uploadOk is set to 0 by an error
    if ($uploadOk == 0)
    {
    $errormsg = "Sorry, your file was not uploaded.!!!\\n".$errormsg;
    echo '<script type="text/javascript">alert("'.$errormsg.'");window.location="mainRedirectAddPlaces.php";</script>';
    }
    // if everything is ok, try to upload file
    else
    {
        // move uploaded file to target directory
        if (move_uploaded_file($_FILES["fileToUpload"]["tmp_name"], $target_file))
    {
        // check all the fields are filled
        if($name!= " " && $area!=" " && $des!= " " && $dis!=" " && $lat!=" " && $long!= " " && $url!=" ")
        {
            include_once('db.php');

            $query = "insert into locations  (Location_Name,Description, District, Area,Image,latitude, longitude, detail_url) values('$name','$des','$dis','$area','$target_file',$lat,$long,'$url') ";
            $query1= mysql_query($query);

            echo "<script type='text/javascript'>alert('Successfully added the record');window.location='mainRedirectPlace.php';</script>";
        }
        else
        {
            // if empty
        }

    }
        else // if error occurred
    {
        echo "<script type='text/javascript'>alert('Sorry, there was an error uploading your file.');window.location='mainRedirectAddPlaces.php';</script>";

    }
    }
}
// if cancel button is pressed
else if(isset($_POST['cancel']))
{

    echo "<script type='text/javascript'>window.location='mainRedirectAddPlaces.php';</script>";
}
// if image is not selected
else
{
    echo "<script type='text/javascript'>alert('Please select an image');window.location='mainRedirectAddPlaces.php';</script>";
}
?>

</body>
</html>