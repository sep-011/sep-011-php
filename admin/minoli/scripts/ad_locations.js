window.onload = function () {

    var mapOptions = {
        center: new google.maps.LatLng(6.92, 79.85),
        zoom: 14,
        mapTypeId: google.maps.MapTypeId.ROADMAP
    };

    var infoWindow = new google.maps.InfoWindow();

    var latlngbounds = new google.maps.LatLngBounds();

    var map = new google.maps.Map(document.getElementById("map"), mapOptions);

    google.maps.event.addListener(map, 'click', function (e) {

        document.getElementById('loc_lat').value  = e.latLng.lat();
        document.getElementById('loc_long').value  = e.latLng.lng();
    });
}

