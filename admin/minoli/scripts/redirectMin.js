// to redirect to pages
function phpRedirect(URL,page,id) {

    // Create our XMLHttpRequest object
    var request = new XMLHttpRequest();

    // Create some variables which redirect to PHP file
    var url = URL;

    var vars = "q=" + page +"&q1=" + id;
    // alert(vars);
    request.open("POST", url, true);

    // Set content type header information for sending url encoded variables in the request
    request.setRequestHeader("Content-type", "application/x-www-form-urlencoded");

    // Access the onreadystatechange event for the XMLHttpRequest object
    request.onreadystatechange = function () {
        if (request.readyState == 4 && request.status == 200) {

            //get return value
            var return_data = request.responseText;

            //store return value
            document.getElementById("result").innerHTML = return_data;


        }
        else
        {
            document.getElementById("result").innerHTML = request.status;
        }
    }
    // Send the data to PHP now... and wait for response to update the status div
    request.send(vars); // Actually execute the request

   // alert("processing...");
    document.getElementById("result").innerHTML = "processing...";



}
/*
*  Feedback form
* */

 /* to delete selected row */
function deleteF(id)
{
    // confirm before delete the row
    var r = confirm("Are you sure you want to delete this record??");
    if (r == true)
    {
        var page = "dl";
        var url = "http://sep.esy.es/admin/minoli/Controller/Controller.php";
        phpRedirect(url,page,id);
    }
    else
    {
        var page = "fb";
        var url = "http://sep.esy.es/admin/minoli/Controller/Controller.php";
        phpRedirect(url,page,id);

    }

}

/* to view/Read selected row */
function viewM(id)
{
    var page = "view";
    var url = "http://sep.esy.es/admin/minoli/Controller/Controller.php";
    phpRedirect(url, page, id);
}

/* to load send email form */
function email(id)
{
    var page = "em";
    var url = "http://sep.esy.es/admin/minoli/Controller/Controller.php";
    phpRedirect(url, page, id);
}

/* to  make action as Read */
function updateAction(id)
{
    var page = "actionF";
    var url = "http://sep.esy.es/admin/minoli/Controller/Controller.php";
    phpRedirect(url, page, id);

}

/* to send email for selected user */
function send_email()
{
    var name = document.getElementById("user_name").value;
    var email = document.getElementById("user_email").value;
    var sub = document.getElementById("user_sub").value;
    var des = document.getElementById("user_des").value;

    if(des === '')
    {
        alert("Please fill the description!!")
    }
    else
    {
        // Create our XMLHttpRequest object
        var request = new XMLHttpRequest();

        // Create some variables which redirect to PHP file
        var url = "send_email.php";

        var vars = "q=" + name + "&q1=" + email + "&q2=" + sub + "&q3=" + des;
        //alert(vars);
        request.open("POST", url, true);

        // Set content type header information for sending url encoded variables in the request
        request.setRequestHeader("Content-type", "application/x-www-form-urlencoded");

        // Access the onreadystatechange event for the XMLHttpRequest object
        request.onreadystatechange = function () {
            if (request.readyState == 4 && request.status == 200) {

                //get return value
                var return_data = request.responseText;

                //store return value
                document.getElementById("email").innerHTML = return_data;


            }
        }
        // Send the data to PHP now... and wait for response to update the status div
        request.send(vars); // Actually execute the request
    }
}

/*
 *  Report problem form
 * */

/* to delete selected row */
 function deleteRep(id)
{
    // confirm before delete the row
    var r = confirm("Are you sure you want to delete this record??");
    if (r == true)
    {
        var page = "dl";
        var url = "http://sep.esy.es/admin/minoli/Controller/Controller_Report.php";
        phpRedirect(url,page,id);
    }
    else
    {
        var page = "rp";
        var url = "http://sep.esy.es/admin/minoli/Controller/Controller_Report.php";
        phpRedirect(url,page,id);

    }
}

/* to load send email form */
function emailRep(id)
{
    var page = "em";
    var url = "http://sep.esy.es/admin/minoli/Controller/Controller_Report.php";
    phpRedirect(url, page, id);
}

/* to view/Read selected row */
function viewR(id)
{

    var page = "view";
    var url = "http://sep.esy.es/admin/minoli/Controller/Controller_Report.php";
    phpRedirect(url, page, id);
}

/* to  make action as Read */
function updateActionReport(id)
{
    var page = "actionF";
    var url = "http://sep.esy.es/admin/minoli/Controller/Controller_Report.php";
    phpRedirect(url, page, id);

}