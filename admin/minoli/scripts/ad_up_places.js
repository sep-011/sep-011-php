 /** navigate to Controller.php  */
function phpRedirect(page,id) {

    // Create our XMLHttpRequest object
    var request = new XMLHttpRequest();

    // Create some variables which redirect to PHP file
    var url ="http://sep.esy.es/admin/minoli/Controller/Controller.php" ;

    var vars = "value1=" + page + "&value2=" + id;
  //  alert(vars);
    request.open("POST", url, true);

    // Set content type header information for sending url encoded variables in the request
    request.setRequestHeader("Content-type", "application/x-www-form-urlencoded");

    // Access the onreadystatechange event for the XMLHttpRequest object
    request.onreadystatechange = function () {
        if (request.readyState == 4 && request.status == 200) {

            //get return value
            var return_data = request.responseText;

            //store return value
            document.getElementById("result").innerHTML = return_data;

         //   alert(return_data);
        }
        else
        {
            document.getElementById("result").innerHTML = request.status;
        }
    }
    // Send the data to PHP now... and wait for response to update the status div
    request.send(vars); // Actually execute the request

    alert("processing...");

}

/** navigate to view location page */
function phpfakeRedirect()
{
    var page = "mainfun";
    var id = -99;
    phpRedirect(page,id);
}

/** delete location */
function deleteLoc(id)
{

    var r = confirm("Are you sure you want to delete this record??");
    if (r == true)
    {
        var page = "dloc";

        phpRedirect(page,id);
    }
    else
    {
        var page = "mainfun";
        var kid = -99;
        phpRedirect(page,kid);

    }
}

/** load edit location form */
function EditLoc(id)
{
    var page = "edit";
    phpRedirect(page,id);
}

/** to update location details */
function upload_location(id)
{
   // var page = "update";

    // getting values from the location update form
    var name = document.getElementById("loc_name").value;
    var desc = document.getElementById("loc_des").value;
    var dis = document.getElementById("loc_dis").value;
    var area = document.getElementById("loc_area").value;
    var latitude = document.getElementById("loc_lat").value;
    var longitude = document.getElementById("loc_long").value;
    var locUrl = document.getElementById("loc_url").value;


    // Create our XMLHttpRequest object
    var request = new XMLHttpRequest();

    // Create some variables which redirect to PHP file
    var url = "http://sep.esy.es/admin/minoli/Controller/Controller.php";

    var vars = "id=" + id + "&name=" + name + "&desc=" + desc + "&dis=" + dis + "&area=" + area + "&latitude=" + latitude + "&longitude=" + longitude + "&locUrl=" + locUrl ;
    //alert(vars);
    request.open("POST", url, true);

    // Set content type header information for sending url encoded variables in the request
    request.setRequestHeader("Content-type", "application/x-www-form-urlencoded");

    // Access the onreadystatechange event for the XMLHttpRequest object
    request.onreadystatechange = function () {
        if (request.readyState == 4 && request.status == 200) {

            //get return value
            var return_data = request.responseText;

            //store return value
            document.getElementById("result").innerHTML = return_data;


        }
    }
    // Send the data to PHP now... and wait for response to update the status div
    request.send(vars); // Actually execute the request
}
