<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <title>AdminreportResponse</title>

    <!-- AdminreportResponse references -->
    <link href="css/index.css" rel="stylesheet" />
    <script type="text/javascript" src="scripts/ad_locations.js"></script>
    <link rel="stylesheet" href="http://code.jquery.com/mobile/1.4.5/jquery.mobile-1.4.5.min.css">
    <script src="https://maps.googleapis.com/maps/api/js?v=3.exp&signed_in=true"></script>


    <script>
        google.maps.event.addDomListener(window, 'load', initialize);
    </script>
</head>
<body >

<?php


?>




<br /><br />
<div id="map" style="width:500px;height:380px;"></div>
<br /><br />
<form action="add_location.php" method="post" enctype="multipart/form-data">
<table data-role="table" data-mode="columntoggle" class="ui-responsive ui-shadow" id="myTable" border="4" >

    <tr>
        <td>Name</td>
        <td><textarea name="loc_name" id="loc_name" rows="1" cols="50"></textarea></td>
    </tr>

    <tr>
        <td>Description</td>
        <td><textarea name="loc_des" id="loc_des" rows="10" cols="50" ></textarea></td>
    </tr>

    <tr>
        <td>District</td>
        <td><textarea name="loc_dis" id="loc_dis" rows="1" cols="50"></textarea></td>
    </tr>
    <tr>
        <td>Area</td>
        <td><textarea name="loc_area" id="loc_area" rows="1" cols="50" ></textarea></td>
    </tr>

    <tr>
        <td>Image</td>
        <td>

                <input type="file" name="fileToUpload" id="fileToUpload">


        </td>

    </tr>

    <tr>
        <td>Latitude</td>
        <td><textarea name="loc_lat" id="loc_lat" rows="1" cols="50"  ></textarea></td>
    </tr>
    <tr>
        <td>Longitude</td>
        <td><textarea name="loc_long" id="loc_long" rows="1" cols="50"  ></textarea></td>
    </tr>

    <tr>
        <td>URL</td>
        <td><textarea name="loc_url" id="loc_url" rows="1" cols="50"></textarea></td>
    </tr>




    <tr>
        <td>

            <input type="submit" value="ADD" name="submit">
        </td>
        <td>
            <input type="submit" value="CANCEL" name="cancel">
        </td>
    </tr>
</table>
</form>
<span id="places"></span>

</body>
</html>