<html>
<head>
    <link href="css/index.css" rel="stylesheet" />
    <script type="text/javascript" src="scripts/ad_up_places.js"></script>
    <link rel="stylesheet" href="http://code.jquery.com/mobile/1.4.5/jquery.mobile-1.4.5.min.css">
</head>
<body>

<?php
$updated = false;
// if submit button and upload file is selected
if(isset($_POST["submit"])&& !empty($_FILES["fileToUpload"]["tmp_name"]))
{
    $updated = true;
    $errormsg = "";
    $locationid = $_POST['glob'];

$target_dir = "../../sep/images/";
$target_file = $target_dir . basename($_FILES["fileToUpload"]["name"]);
$uploadOk = 1;
$imageFileType = pathinfo($target_file,PATHINFO_EXTENSION);
// Check if image file is a actual image or fake image

    $check = getimagesize($_FILES["fileToUpload"]["tmp_name"]);
    if($check !== false)
    {
       // echo "File is an image - " . $check["mime"] . ".";
        $uploadOk = 1;
    }
    else
    {
        $errormsg = "File is not an image!!!\\n" .$errormsg;
        $uploadOk = 0;
    }

// Check if file already exists
if (file_exists($target_file))
{
    $errormsg = "File already exists.!!!\\n" .$errormsg;

    $uploadOk = 0;
}
// Check file size
if ($_FILES["fileToUpload"]["size"] > 500000)
{
    $errormsg = "Your file is too large!!\\n".$errormsg;
    $uploadOk = 0;
}
// Allow certain file formats
if($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg" && $imageFileType != "gif" && $imageFileType != "PNG" )
{
    $errormsg = "Only JPG, JPEG, PNG & GIF files are allowed.!!!\\n".$errormsg;
    $uploadOk = 0;
}
// Check if $uploadOk is set to 0 by an error
if ($uploadOk == 0)
{
    $errormsg = "Sorry, your file was not uploaded.!!!\\n".$errormsg;

    echo '<script type="text/javascript">alert("'.$errormsg.'");window.location="mainRedirectPlace.php";</script>';


}// if everything is ok, try to upload file
else {
    if (move_uploaded_file($_FILES["fileToUpload"]["tmp_name"], $target_file)) {

        echo "<script type='text/javascript'>alert('Successfully Uploaded the image');window.location='update_imageRedirectFake.php?image=$target_file&locat=$locationid';</script>";

    }
    else
    {

       echo "<script type='text/javascript'>alert('Sorry, there was an error uploading your file.');window.location='mainRedirectPlace.php';</script>";
    }
}
}

// if submit button not pressed or upload file has not choose
else
{

    echo "<script type='text/javascript'>alert('Please select an image');window.location='mainRedirectPlace.php';</script>";
}
?>

</body>
</html>