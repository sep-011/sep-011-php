<html>
<body>

<?php
/**
 *
 * Use to control the view request from the application
 */


include_once($_SERVER["DOCUMENT_ROOT"].'/admin/minoli/connection.php');
include_once($_SERVER["DOCUMENT_ROOT"].'/admin/minoli/View/view.php');


$connection=new connection();
$conn=$connection->connect();
$view= new View();

if(!$conn)
{
    echo "Server connection terminated. please try again in few seconds";

}
else {

    // feedback and report problem
    if (isset($_POST['q']) && isset($_POST['q1'])) {

        $p = $_POST["q"];
        $k = $_POST["q1"];

        $view->Feedback($p, $k);
    }

    // view and delete places
    if (isset($_POST['value1']) && isset($_POST['value2'])) {

        $value1 = $_POST["value1"];
        $value2 = $_POST["value2"];

        $view->ViewPlaces($value1, $value2);
    }

    // update places
    if(isset($_POST['id']) && isset($_POST['name']) && isset($_POST['desc']) && isset($_POST['dis']) && isset($_POST['area']) && isset($_POST['latitude']) && isset($_POST['longitude']) && isset($_POST['locUrl']))
    {
        $id = $_POST["id"];
        $name =$_POST["name"];
        $description = $_POST["desc"];
        $dist = $_POST["dis"];
        $area = $_POST["area"];
        $latitude = $_POST["latitude"];
        $longitude = $_POST["longitude"];
        $url = $_POST["locUrl"];


        $view->UpdatePlaces($id,$name,$description,$dist,$area,$latitude,$longitude,$url);
    }


}


?>

</body>
</html>