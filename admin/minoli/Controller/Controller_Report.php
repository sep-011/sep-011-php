<html>
<body>

<?php
/**
 *
 * Use to control the view request from the application
 */


include_once($_SERVER["DOCUMENT_ROOT"].'/admin/minoli/connection.php');
include_once($_SERVER["DOCUMENT_ROOT"].'/admin/minoli/View/view.php');


$connection=new connection();
$conn=$connection->connect();
$view= new View();

if(!$conn)
{
    echo "Server connection terminated. please try again in few seconds";

}
else {

    // feedback and report problem
    if (isset($_POST['q']) && isset($_POST['q1'])) {

        $p = $_POST["q"];
        $k = $_POST["q1"];

        $view->Report($p, $k);
    }


}


?>

</body>
</html>