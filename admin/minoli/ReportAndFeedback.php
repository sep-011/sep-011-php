<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <title>AdminreportResponse</title>

    <!-- AdminreportResponse references -->
    <link href="css/index.css" rel="stylesheet" />
    <script type="text/javascript" src="scripts/redirectMin.js"></script>
    <link rel="stylesheet" href="http://code.jquery.com/mobile/1.4.5/jquery.mobile-1.4.5.min.css">

    <script type="text/javascript">

        // view values from selected option
        function myFunc()
        {
            // get selected value from drop down
            var val=  document.getElementById("Problem");
            var selected_val=val.options[val.selectedIndex].value;

            if(selected_val == 1)//if  select feedback
            {
                var url = "http://sep.esy.es/admin/minoli/Controller/Controller.php";
                var page = "fb";
                var id = -99;
                phpRedirect(url, page,id);
            }
            else if(selected_val == 2)//if  select report_problem
            {
                var url = "http://sep.esy.es/admin/minoli/Controller/Controller_Report.php";
                var page = "rp";
                var id = -99;
                phpRedirect(url, page,id);
            }
            else
            {
                alert("Please select an option");
            }
        }

    </script>

</head>
<body>

<!-- Create a drop down -->
<table>
    <tr>
        <td>Problem</td>
        <td>
            <select id="Problem" onchange="myFunc()">
                <option value="0">select an option</option>
                <option value="1">feedback</option>
                <option value="2">reported problems</option>

            </select>
        </td>
    </tr>
</table>

<br /><br />
<!-- to view results  -->
<span id="result"></span>
<!-- to view email form -->
<span id="email"></span>

</body>
</html>