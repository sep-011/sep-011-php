<section id="loginBox">
    <h3>Recover password</h3>
<?php
    include_once('db.php');
    $valid = false;
    if(isset($_GET['u']) && isset($_GET["c"])){
        $mail = $_GET['u'];
        $code = $_GET['c'];

        $query = "Select resetCode,email from admins";
        $result2 = mysqli_query($conn,$query) or die(mysqli_connect_error());

        while($row=mysqli_fetch_array($result2)){
            if(md5($row[0])==$code && md5($row[1])==$mail){
                $valid=true;
                $email=$row[1];
                $code2=$row[0];
?>
    <script>
        function resetPwd() {
            var pwd = document.getElementById("newpw").value;
            var confpw = document.getElementById("confpw").value;
            var code = <?php echo json_encode($code2); ?>;
            var mail = <?php echo json_encode($email); ?>;
            if(checkPassword(pwd,confpw,code)) {

                    var hr = new XMLHttpRequest();
                    var url = "http://sep.esy.es/admin/forgotpwProcess.php";
                    var query = mail;
                    var query2 = code;
                    var vars = "mail=" + query + "&code=" + code + "&pwd=" + pwd;
                    hr.open("POST", url, true);
                    hr.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
                    hr.onreadystatechange = function () {
                        if (hr.readyState == 4 && hr.status == 200) {
                            var return_data = hr.responseText;
                            if(return_data=="success") {
                                alert("Password Successfully reset\nPlease login");
                                window.location.replace("http://sep.esy.es/admin/Login.php");
                            }
                            else {
                                document.getElementById("status2").innerHTML = return_data;
                            }

                        }
                    }
                    hr.send(vars);
                    document.getElementById("status2").innerHTML = "processing...";

            }

        }

        function checkPassword(pwd,confpwd,code) {
            if(code.length != 6){
                document.getElementById("status2").innerHTML = "Invalid Reset code.Reset code should be 6 digits";
                return false;
            }

            if(pwd.length < 5){
                document.getElementById("status2").innerHTML = "Password Length Should Be greater than 5 ";
                return false;
            }
            else if(pwd!=confpwd){
                document.getElementById("status2").innerHTML = "Password Not Match !";
                return false;
            }
            else{
                return true;
            }

        }

    </script>



    <form method="post" class="minimal" name="login">
        New Password:
        <input type="password" name="newpw" id="newpw" placeholder="Enter New Password" required="required" />

        Confirm Password:
        <input type="password" name="confpw" id="confpw" placeholder="Confirm Password" required="required" />

        <div align="right">
            <input type="button" class="btn-minimal"  name="reset" id="reset" value="Reset" onclick="resetPwd()">
        </div>

        <div id="status">
        </div>

        <div id="status2">
        </div>

    </form>
<?php
            }
        }
    }
    if($valid==false){
        echo "Invalid Link";
    }
?>

</section>