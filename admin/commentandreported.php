<?php
session_start();
if(!isset($_SESSION['uid_sep']))
{    header("Location: Login.php");}
?>
<!DOCTYPE html>
<html class="csstransforms no-csstransforms3d csstransitions"><head>
    <meta http-equiv="content-type" content="text/html; charset=UTF-8">
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <title>Tropical Vacations</title>
	<link rel="stylesheet" href="css/log.css">
    <link rel="stylesheet" type="text/css" href="css/font-awesome.css">
    <link rel="stylesheet" type="text/css" href="css/menu.css">
    <link rel="stylesheet" type="text/css" href="css/styles.css">
	


    <script type="text/javascript" src="js/jquery.js"></script>
    <script type="text/javascript" src="js/function.js"></script>

    <!--font-->
    <link href='http://fonts.googleapis.com/css?family=Source+Sans+Pro' rel='stylesheet' type='text/css'>
	
	<link href="samith/_cssNotification/cssforcommentview.css" rel="stylesheet" type="text/css">
        <link rel="stylesheet" href="samith/usersuggestiontable.css" type="text/css">
		<link rel="stylesheet" href="samith/loadingiconcss.css" type="text/css">
		<link href="samith/_cssNotification/animated-notifications.css" rel="stylesheet" type="text/css" />
		<script type="text/javascript" src="samith/_scriptsNotification/jquery-2.0.2.min.js"></script>
		<script type="text/javascript" src="samith/_scriptsNotification/jquery-ui-1.10.4.min.js"></script>
		<script type="text/javascript" src="samith/_scriptsNotification/animated-notifications.js"></script>
		<script type="text/javascript" src="samith/locationsearchsam.js"></script>
		<!--for boostrap-->
		
		<link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
		<script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
        
        
        
</head>
<body onload="shownotification()">
<div class="wrapper">
  <div style="background:#333333; font-size:30px; text-align:center; color:#FFF; font-weight:bold; height:70px; padding-top:50px;">
        <a class="logo" href="#">
            <img src="images/Tropical-vacations-300x300.png" height="140px" width="140px"  alt="fresh design web" id="titleimg">
        </a>

        

      Tropical Vacations 
    </div>

    <div id="wrap">
        <header>
            <div class="inner relative">
                <a class="logo" href="#"><!--img src="images/RM.png" height="63px" width="336px"  alt="fresh design web"--></a>
                <a id="menu-toggle" class="button dark" href="#"><i class="icon-reorder"></i></a>
                <nav id="navigation">
                    <ul id="main-menu">
                        <li class="current-menu-item"><a href="Admin.php">Home</a></li>
                        <li class="current-menu-item">
                            <a href="">Blog</a>
                            
                        </li>
                        <li class="current-menu-item">
                            <a href="">About Us</a>
                            
                        </li>
                        <li class="current-menu-item"><a href="">Contact Us</a></li>
                        <!--<li class="current-menu-item"><a href="">Contact</a></li> -->
                    </ul>
                </nav>
                <div class="clear"></div>
            </div>
        </header>


    </div>




    <div id="content">

        <div class="header_02">
            Welcome to Tropical Vacations Sri Lanka
        </div>


        <div id="contentsec1">
             <?php include './samith/ajaxLocationsearch_body.html';?>
        </div>
        <div class="push"></div>
    </div>


</div>

<div class="footer">

</div>



</body></html>