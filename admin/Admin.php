<?php
session_start();
?>
<!DOCTYPE html>
<html class="csstransforms no-csstransforms3d csstransitions"><head>
    <meta http-equiv="content-type" content="text/html; charset=UTF-8">
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <title>Tropical Vacations</title>
    <link rel="stylesheet" type="text/css" href="css/font-awesome.css">
    <link rel="stylesheet" type="text/css" href="css/menu.css">
    <link rel="stylesheet" type="text/css" href="css/styles.css">
	


    <script type="text/javascript" src="js/jquery.js"></script>
    <script type="text/javascript" src="js/function.js"></script>

    <!--font-->
    <link href='http://fonts.googleapis.com/css?family=Source+Sans+Pro' rel='stylesheet' type='text/css'>

</head>
<body>
<div class="wrapper">
  <div style="background:#333333; font-size:30px; text-align:center; color:#FFF; font-weight:bold; height:70px; padding-top:50px;">
        <a class="logo" href="#">
            <img src="images/Tropical-vacations-300x300.png" height="140px" width="140px"  alt="fresh design web" id="titleimg">
        </a>

        <!--logout link-->
        <div id="logout">
            <a href="Admin.php?logout="" name="logout">LOGOUT</a>        
		</div>

      Tropical Vacations 
    </div>

    <div id="wrap">
        <header>
            <div class="inner relative">
                <a class="logo" href="#"><!--img src="images/RM.png" height="63px" width="336px"  alt="fresh design web"--></a>
                <a id="menu-toggle" class="button dark" href="#"><i class="icon-reorder"></i></a>
                <nav id="navigation">
                    <ul id="main-menu">
                        <li class="current-menu-item"><a href="#">Home</a></li>

                        <li class="parent">
                            <a href="">AppUsers</a>
                            <ul class="sub-menu">
                                <li><a href="../admin/SearchRevokeUser/Search_userLive.php"><i class="icon-file"></i>Search Users</a></li>
                                <li><a href="../admin/SearchRevokeUser/ViewRevoked.php"><i class="icon-file"></i>Revoked/Reactivated Users</a></li>
                                <li><a href="../admin/SearchRevokeUser/GetMostTraveled.php"><i class="icon-file"></i>Favorite places</a></li>
								<li><a href="/admin/usersuggestionlocationserver.php"><i class="icon-file"></i>User Suggestions</a></li>
							</ul>
                        </li>
						<li class="parent">
                            <a href="">Facilities</a>
                            <ul class="sub-menu">
                                <li><a href="../admin/minoli/mainNavigation.php"><i class="icon-edit"></i> Feedback / Reported problems</a></li>
                                <li><a href="../admin/minoli/mainRedirectPlace.php"><i class="icon-edit"></i> Location Details</a></li>
                                <li><a href="../admin/minoli/mainRedirectAddPlaces.php"><i class="icon-edit"></i> Add New Locations </a></li>
								<li><a href="/admin/admin_labels/loadall.php"><i class="icon-edit"></i> AR Labels</a></li>
								<li><a href="/admin/commentandreported.php"><i class="icon-edit"></i>User Comments</a></li>
								
                            </ul>
                        </li>

                        <li class="current-menu-item">
                            <a href="">Blog</a>
                            
                        </li>
                        <li class="current-menu-item">
                            <a href="">About Us</a>
                            
                        </li>
                        <li class="current-menu-item"><a href="">Contact Us</a></li>
                        <!--<li class="current-menu-item"><a href="">Contact</a></li> -->
                    </ul>
                </nav>
                <div class="clear"></div>
            </div>
        </header>


    </div>




    <div id="content">

        <div class="header_02">
            Welcome to Tropical Vacations Sri Lanka
        </div>


        <div id="contentsec1">
			<?php include 'body_admin.php';?>
        </div>
        <div class="push"></div>
    </div>


</div>

<div class="footer">

</div>



</body></html>