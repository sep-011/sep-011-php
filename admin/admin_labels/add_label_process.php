<?php
include_once('db.php');
require_once('db_access_r.php');

$db_access = new db_access_r();

//To add or update a location label
if(isset($_POST['submitLbl'])){
	
	//To check that current location already has a label
    $status = $_POST['labelStatus'];
    $id = $_POST['locId'];
    $newTitle = $_POST['title'];
    $newDescr = $_POST['descr'];
    $visibility = $_POST['radioVisible'];
    if(isset($_POST['radiobtn2'])){
        $altitude = "default";
    }
    else{
        $altitude = $_POST['altValHidden'];
    }
    
	$db_access->addUpdate_label($status, $id, $newTitle, $newDescr, $visibility, $altitude);
}
