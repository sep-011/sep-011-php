<?php
//Host URL
$hosturl="http://sep.esy.es/";	//"http://localhost/";

include_once('db.php');
require_once('db_access_r.php');

$db_access = new db_access_r();

$title=$descr=$imgLink=$dbImg=$visibility=$altitude=$location="";
$defaultImg="sep/images/ar/marker_idle.png";
$id='';

//get the location id from url and load current values
if(isset($_GET['id'])){

    $id = $_GET['id'];
    if(is_numeric($id)) {
        //$query = "SELECT * FROM locations WHERE Location_id={$id}";
        //$result = mysql_query($query) or die(mysql_error());
		$result = $db_access->getLocationDetails($id);

        if(mysql_num_rows($result)) {
            while($row=mysql_fetch_array($result)){
                $location=$row['Location_Name'];
            }
            //$query = "SELECT * FROM ar_labels WHERE location={$id}";
            //$result = mysql_query($query) or die(mysql_error());
			$result = $db_access->getLabelDetails($id);
            $status = mysql_num_rows($result);            

			//Check that location already has a Label
            if ($status) {
                while ($row = mysql_fetch_array($result)) {
                    $title = $row['title'];
                    $descr = $row['description'];
                    $dbImg = $row['labelPic'];
                    $visibility = $row['visibility'];
                    $altitude = $row['altitude'];

                    if($altitude!="default"){
						//Change the slider value to current altitude
                        echo "<script> $(function(){changeSlider({$altitude})}); </script>";
                    }
                }
            } else {
                //echo "No AR Label for this location.";
            }
            $imageurl = $dbImg=="default"||$dbImg==""?"sep/images/ar/marker_idle.png":$dbImg;
            //$imageurl = $dbImg=="default"||$dbImg==""?$defaultImg:$dbImg; // remove !!!!!!!!!!!!

            $imgLink = "{$hosturl}{$imageurl}";
        }
        else{
            echo "Invalid Link";
            return;
        }
    }
    else{
        echo "Invalid Link";
        return;
    }

}
else{
    echo "Invalid Link";
    return;
}
?>
<h3 class="loc"><?php echo $location; ?></h3>
<label class="error" id="hiddenLabel">
    <?php
    echo $visibility=='0'?"This Label is hidden":"";
    ?>
</label>
<?php if($status==0) echo "No AR Label for this location."; ?>
<form id="form1" method="post" enctype="multipart/form-data" onsubmit="return ajax_post2()">
    <table border="0" id="table1">
        <tr>
            <td><label for="">Title</label></td>
            <td colspan="3"><input class="inpField" type="text" name="title" id="" required placeholder="Label Title" value='<?php echo $title; ?>' ></td>
        </tr>
        <tr>
            <td><label for="">Description</label></td>
            <td colspan="3"><input class="inpField" type="text" name="descr" id="" placeholder="Label Description" required value='<?php echo $descr; ?>' ></td>
        </tr>
        <tr>
            <td><label for="">Altitude</label></td>
            <td colspan="3"><input type="radio" name="radiobtn2" id="radiobtn2"
                    <?php echo($altitude=="default"||$dbImg==""?"checked":""); ?> />User level altitude
                <br><b style="margin-left: 30%;">OR</b><br>
                <div id="slider"></div> <label id="altVal" for="slider"></label>
                <input type="hidden" id="altValHidden" name="altValHidden">
            </td>
        </tr>
        <tr>
            <td><label for="">Label icon</label></td>
            <td>
                <input type="radio" name="radiobtn1" id="radiobtn1"
                    <?php echo($dbImg=="default"||$dbImg==""?"checked":""); ?> />Default Label
                <br><b style="margin-left: 30%;">OR</b><br>
                <input type="file" name="fileUpload" id="fileupload" value="asd" accept=".jpg,.png" onclick="radioUncheck('radiobtn1');"/><br><br>
                <a href="ar_image_guide.php" target="_blank">Click here</a> for label icon guide</td><br>
            </td>
            <td>
                <img src='<?php echo $imgLink; ?>' alt="" id="labelImg" style="max-height: 100px;max-width: 100px"/>
            </td>
            <td>
                <img src='<?php echo "{$hosturl}sep/images/ar/Undo-icon.png"; ?>' alt="" id="undoImg">
            </td>
        </tr>
        <tr>
            <td>Label Visibility</td>
            <td colspan="3"><input type="radio" name="radioVisible" id="visibleOn" value="1" <?php echo $visibility!="0"?"checked":"";?> />On<br>
                <input type="radio" name="radioVisible" id="visibleOff" value="0" <?php echo $visibility=="0"?"checked":"";?> />Off
            </td>
        </tr>
        <tr>
            <td colspan="4" align="right"><button type="submit" class="myButton" name="submitLbl">Add/Update</button></td>
        </tr>
    </table>
</form>

<script>

	/**
	* To uncheck a radio button
	* @param {string} radioButton : radio button element Id
	*/
    function radioUncheck(radioButton){
        document.getElementById(radioButton).checked = false;
    }
	
	/**
	* To check a radio button
	* @param {string} radioButton : radio button element Id
	*/
    function radioCheck(radioButton){
        document.getElementById(radioButton).checked = true;
    }
    function setDefault(){
        $("#radiobtn1").prop("checked", true)
    }
	
	/**
	* To remove a selected image file
	*/
    function clearFile(){
        var control = $("#fileupload");
        control.replaceWith( control = control.clone( true ) );
    }
	
	/**
	* To validate the selected image file
	* @param input : image chooser element
	*/
    function readURL(input) {
        if (input.files && input.files[0]) {
            var err='';
            var type = input.files[0].type;
            if(type == "image/jpeg" || type == "image/png") {
                if(input.files[0].size/1024 <= 250) {
                    document.getElementById("status").innerHTML="";
					
					//To read the image url
                    var reader = new FileReader();
					
					//FileReader onload function to display the selected image
                    reader.onload = function (e) {
                        $('#labelImg').attr('src', e.target.result);
                    }					
                    reader.readAsDataURL(input.files[0]);
                }
                else{
                    err="Invalid size - Max 250 KB";
                }
            }
            else {
                err = "Invalid file type. Select jpeg or png";
            }
            if(err != '') {
                clearFile();
                $("#status").removeClass("load");
                $("#status").addClass("error");
                document.getElementById("status").innerHTML=err;
            }
        }
    }

    $("#fileupload").change(function(){
        readURL(this);
    });

    $("#radiobtn1").change(function(){
        <?php $defImgUrl="{$hosturl}{$defaultImg}"; ?>
        clearFile();
        var imgSrc="<?php echo $defImgUrl; ?>";
        $('#labelImg').attr('src', imgSrc);
    });

	/**
	* To change the slider value
	* @param {number} value : new value
	*/
    function changeSlider(value){
        $("#slider").slider("value",value);
        $("#altVal").text($("#slider").slider("value")+" m");
        $("#altValHidden").val($( "#slider").slider("value"));
    }

    
	/**
	* Ajax function to add/update labels
	*/
    function ajax_post2(){

        var formElement = document.getElementById("form1");
        var formData = new FormData(formElement);
        var locId = <?php echo $id; ?>;
		
		//To check that current location already has a label
        var labelStatus = <?php echo $status; ?>;
        formData.append("locId",locId);
        formData.append("labelStatus",labelStatus);
        var hr = new XMLHttpRequest();
        var url = "http://sep.esy.es/admin/admin_labels/add_label_process.php";
        hr.open("POST", url, true);
        hr.onreadystatechange = function () {
            if (hr.readyState == 4 && hr.status == 200) {
                var return_data = hr.responseText;
                document.getElementById("status").innerHTML = return_data;
                if(return_data.split(" ")[0]=="Label"){
                    $("#status").removeClass("error");
                    $("#status").addClass("load");
                    location.reload();
                }
                else{
                    $("#status").removeClass("load");
                    $("#status").addClass("error");
                }
            }
        }
        hr.send(formData);
        var hosturlr = "<?php echo $hosturl; ?>";

        $("#status").removeClass("error");
        $("#status").addClass("load");

        var loader= hosturlr+"sep/images/ar/ajax-loader.gif";
        document.getElementById("status").innerHTML = "<img src="+loader+">";//"processing...";
        return false;
    }

	/*After page load*/
    $(function(){
        $("#undoImg").click(function(){
            clearFile();
            document.getElementById("status").innerHTML ="";
            var imgSrc="<?php echo $imgLink; ?>";
            $('#labelImg').attr('src', imgSrc);
            <?php
             if($dbImg == "default" || $dbImg == ""){
             ?>
            radioCheck("radiobtn1");
            <?php
             }
             else{
            ?>
            radioUncheck("radiobtn1");
            <?php
            }
             ?>
        });
    });
	
	//To scroll up the page after reload
	window.onload = function() {
		setTimeout (function () {
			scrollTo(0,0);
			$(function(){
				$("#hiddenLabel").delay(500).effect("shake", {times:4},1000);
			});
		}, 100);
	}
</script>
<div id="status"></div>