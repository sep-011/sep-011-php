<style>
    table{
        border-collapse: collapse;
        width: 50%;
    }
    td{
        border-bottom: 1px solid black;
        border-top: 1px solid black;
        padding: 10px;
    }
    tr:hover{
        background-color: lightgray;
        cursor: pointer;
    }
</style>