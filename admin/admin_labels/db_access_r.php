<?php
/**
 * Created by PhpStorm.
 * User: Roshin
 * Date: 10/28/2015
 * Time: 9:32 PM
 */
include_once('db.php');

class db_access_r{

    /**
     * To add or update(if label already exist for the location) a location label
     * @param $status - status of the label availability for the location
     * @param $id - Location id
     * @param $newTitle - Label title
     * @param $newDescr - Label description
     * @param $visibility - Label visibility
     * @param $altitude - Label altitude
     */
    public function addUpdate_label($status, $id, $newTitle, $newDescr, $visibility, $altitude){

        $fileTmp = $_FILES['fileUpload']['tmp_name'];
        $fileName = basename($_FILES['fileUpload']['name']);

        $fileType=$_FILES['fileUpload']['type'];
        $fileSize=$_FILES['fileUpload']['size']/1024;

        $dbImgPath = "sep/images/ar/{$fileName}";
        $imgExist=0;
        $currImg='';

        //User want to use the previous label image.
        //Get the previous image
        if(empty($fileName) && !isset($_POST['radiobtn1'])){
            $query = "SELECT * FROM ar_labels WHERE location={$id}";
            $result = mysql_query($query) or die(mysql_error());
            while($row=mysql_fetch_array($result)){
                $currImg=$row['labelPic'];

                //Check there's a label image except "default"
                if($currImg != "default" && !empty($currImg)){
                    $imgExist=1;
                }
            }
        }
        if(isset($_POST['radiobtn1'])){
            $dbImgPath = "default";
        }
        else if(!empty($fileName)){
            if($fileType == "image/jpeg" || $fileType == "image/png") {
                if($fileSize<=250) {
                    if (!move_uploaded_file($fileTmp, "../../sep/images/ar/{$fileName}")) {
                        echo "Error uploading Image !";
                        return;
                    } else {
                        $dbImgPath = "sep/images/ar/{$fileName}";
                        //$dbImgPath = "PhpProject1/sep_3rd/images/ar/{$fileName}"; // remove !!!!!!!!!!!!
                    }
                }
                else{
                    $dbImgPath = "SizeError";
                }
            }
            else {
                $dbImgPath = "TypeError";
            }
        }
        else{
            if($imgExist){
                $dbImgPath=$currImg;
            }
            else{
                $dbImgPath="noImg";
            }
        }

        if($dbImgPath != "TypeError" && $dbImgPath != "SizeError" && $dbImgPath != "noImg") {
            if ($status) {
                $queryNewDet = "UPDATE ar_labels SET title='{$newTitle}',description='{$newDescr}',labelPic='{$dbImgPath}',visibility='{$visibility}',altitude='{$altitude}' WHERE location={$id}";
            } else {
                $queryNewDet = "INSERT INTO ar_labels (title,description,location,labelPic,visibility,altitude) VALUES('{$newTitle}','{$newDescr}','{$id}','{$dbImgPath}','{$visibility}','{$altitude}')";
            }
            $resultAdd = mysql_query($queryNewDet) or die(mysql_error());
            if($resultAdd){
                echo "Label Successfully Added/Updated";
            }

        }
        else{
            if($dbImgPath == "TypeError")
                echo "Invalid file type. Select jpeg or png";
            else if($dbImgPath == "SizeError")
                echo "Invalid size - Max 250 KB";
            else if($dbImgPath == "noImg")
                echo "Please choose a label image or select default label";

        }
    }
	
	/**
     * To get details of a location
     * @param $id - Location id     
     */
	public function getLocationDetails($id){
		$query = "SELECT * FROM locations WHERE Location_id={$id}";
		$result = mysql_query($query) or die(mysql_error());
		return $result;
	}
	
	/**
     * Get details of a label(AR label) for specific location
     * @param $id - Location id     
     */
	public function getLabelDetails($id){
		$query = "SELECT * FROM ar_labels WHERE location={$id}";
        $result = mysql_query($query) or die(mysql_error());
		return $result;
	}
	
	/**
     * Get all the visible label(AR label) details when client request
     */
	public function getVisibleLabels(){
		$getDataQuery = "SELECT * FROM ar_labels a,locations l WHERE a.location=l.Location_id and a.visibility=1";
		$result = mysql_query($getDataQuery) or die(mysql_error());
		return $result;
	}
}