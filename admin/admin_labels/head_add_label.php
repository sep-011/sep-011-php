<script src="//code.jquery.com/jquery-1.11.3.min.js"></script>
<link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
<script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>

<style>
    .load{
        text-align: center;
        font-size: 18px;
        font-weight: bold;
    }
    .error{
        color: red;
    }
    #labelImg:hover{
        /*opacity: 0.5;*/
    }
    #slider{
        width: 75%;
    }
    #undoImg{
        margin-left: 10px;
        width: 25px;
        height: 25px;
    }
    #undoImg:hover{
        opacity: 0.5;
        cursor: pointer;
    }
    .loc{
        text-align: center;
        font-size: 25px;
    }
    #table1{
        /*margin: 0 auto;*/
        font-size: 17px;
        border-spacing: 2px 2px;
    }
    #table1 td{
        padding: 15px;
        background-color: #FFECDA;
    }
    #table1 tr:nth-child(even) td{
        background-color:#FFF3E6;
    }
    #table1 tr:hover td{
        background-color:#FFE0C1;
        padding: 18px 15px;
    }
    .inpField{
        width: 65%;
        height: 23px;
        font-size: 14px;
        -moz-border-radius:5px;
        -webkit-border-radius:5px;
        border-radius:5px;
        border:solid 1px black;
        padding:1px;
    }

    .myButton {
        -moz-box-shadow:inset 0px 1px 3px 0px #91b8b3;
        -webkit-box-shadow:inset 0px 1px 3px 0px #91b8b3;
        box-shadow:inset 0px 1px 3px 0px #91b8b3;
        background-color:#768d87;
        -moz-border-radius:11px;
        -webkit-border-radius:11px;
        border-radius:11px;
        border:1px solid #566963;
        display:inline-block;
        cursor:pointer;
        color:#ffffff;
        font-family:Arial;
        font-size:15px;
        font-weight:bold;
        padding:8px 10px;
        text-decoration:none;
        text-shadow:0px -1px 0px #2b665e;
    }
    .myButton:hover {
        background-color:#606969;
    }
    .myButton:active {
        position:relative;
        top:1px;
    }
</style>
<script>
    $(function() {
        $( "#slider" ).slider({
            min:-1000,
            max:1000,
            value:0,
            slide: function( event, ui ) {
                $("#altVal").text(ui.value+" m");
                $("#altValHidden").val(ui.value);
                radioUncheck("radiobtn2");
            }
        });
        $("#altVal").text($("#slider").slider("value")+" m");
        $("#altValHidden").val($( "#slider").slider("value"));
    });
</script>