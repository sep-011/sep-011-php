<?php
if(!isset($_SESSION['uid_sep'])){
    header("Location: Login.php");
}

else {
    echo "Welcome {$_SESSION['uid_sep']}";
}

if(isset($_GET['logout'])){
    unset($_SESSION['uid_sep']);
    header("Location: Login.php");
}
?>