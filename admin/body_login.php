<?php

include_once('db.php');

//Administrator login
if(isset($_POST['login']))
{
	$ip = $_SERVER['REMOTE_ADDR'];
 
    if (!empty($_SERVER['HTTP_CLIENT_IP'])) {
        $ip = $_SERVER['HTTP_CLIENT_IP'];
    } elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
        $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
    }
	
	//To retrieve the false login attempts within the given time interval(15 minutes)
    $query = "SELECT * FROM admin_log_attempts WHERE (date_time > now() - INTERVAL 15 MINUTE) AND ipaddress = '".$ip."'";
    $result = mysqli_query($conn,$query) or die(mysqli_connect_error());
    $rowCount=mysqli_num_rows($result);
	
	//To block the login if there are 3 false login attempts within the time interval (15 minutes)	
    if($rowCount==3){
		//Get the waiting time
        $query = "SELECT TIMEDIFF(CURRENT_TIMESTAMP,min(date_time)) FROM admin_log_attempts WHERE (date_time > now() - INTERVAL 15 MINUTE) AND ipaddress = '".$ip."'";
        $result = mysqli_query($conn,$query) or die(mysqli_connect_error());
        $wait=0;
        while($row=mysqli_fetch_array($result)){
            $times=explode(":",$row[0]);
            $wait_min = 14-$times[1];
            $wait_sec = 59-$times[2];
            if(strlen($wait_sec)==1){
                $wait_sec = "0{$wait_sec}";
            }
            $wait = "{$wait_min}:{$wait_sec}";
        }
        echo "<div align='center' style='color: red'>Please wait {$wait} minutes</div>";
    }
    else {
        $uName = $_POST['username'];
        $pwd = $_POST['password'];
        $uName = mysqli_real_escape_string($conn,$uName);
        $pwd = mysqli_real_escape_string($conn,$pwd);
        $encryptpw = md5($pwd);
        $query = "SELECT * FROM admins where userName='$uName' and password='$encryptpw'";
        $result = mysqli_query($conn,$query) or die(mysqli_connect_error());
        $noRows = mysqli_num_rows($result);
		
		//If successfully login, add the username(unique) to a session and redirect to the main page
        if ($noRows == 1) {
            $_SESSION['uid_sep'] = $uName;
            header("Location: Admin.php");
        } 
		//If error login
		else {
            $date = date('Y-m-d h:i:s', time());
			
			//Add a false login attempt record with the ip address and the current time
            $query = "INSERT INTO admin_log_attempts(ipaddress, date_time) VALUES('{$ip}',CURRENT_TIMESTAMP)";
            $result = mysqli_query($conn,$query) or die(mysqli_connect_error());
            echo
                "<script type=\"text/javascript\">" .
                "window.alert('User name or password is incorrect!!!.');" .
                "</script>";
            if($rowCount+1==3){
                echo "<div align='center' style='color: red'>Please wait 15:00 minutes</div>";
            }
            elseif($rowCount+1<3){
                $used=$rowCount+1;
                echo "<div align='center' style='color: red'>You have used <b>{$used} out of 3</b> login attempts. After all 3 have been used, you will be unable to login for 15 minutes.</div>";
            }
        }
    }
}
?>

<section id="loginBox">
    <?php if(!isset($_SESSION['uid_sep'])){ ?>
	<h2>Login</h2>


	<form method="post" class="minimal" name="login" action="<?php $_SERVER['PHP_SELF']?>">
		<label for="username">
			Username:
			<input type="text" name="username" id="username" placeholder="Enter User Name" required="required" />
		</label>
		<label for="password">
			Password:
			<input type="password" name="password" id="password" placeholder="Enter Password" required="required" />
		</label>
		<div align="right">
			<input type="submit" class="btn-minimal"  name="login" value="Sign in">
            <br/>
            <a href="forgotpw.php"><span style="color: blue">Forgot Your Password?</span></a>
		</div>
	</form>
    <?php } else{ ?>
    <h3>You are already logged in as <?php echo $_SESSION['uid_sep'] ?> you need to log out before logging in again.</h3>
    <?php } ?>
</section>

