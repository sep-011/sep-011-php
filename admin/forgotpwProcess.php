<?php
include './phpmailer/PHPMailerAutoload.php';
include_once('db.php');

if(isset($_POST["sel"])){
    $toMail = $_POST["sel"];
    $genNo = rand(100000,999999);
    $query = "UPDATE admins set resetCode={$genNo} where email = '{$toMail}'";
    $result=mysqli_query($conn,$query);
    $affectedRow = mysqli_affected_rows($conn);
    if($affectedRow == 0){
        echo "Invalid E-mail address";
    }
    else {
        if (mysqli_query($conn,$query)) {

            $link = "http://sep.esy.es/admin/recover_pw_link.php?u=".urlencode(md5($toMail))."&c=".urlencode(md5($genNo));

            $mail = new PHPMailer;
            //$mail->SMTPDebug = 3;                               	// Enable verbose debug output

            $mail->isSMTP();                                      // Set mailer to use SMTP
			$mail->Host = 'mx1.hostinger.co.uk';  // Specify main and backup SMTP servers
			$mail->SMTPAuth = true;                               // Enable SMTP authentication
			$mail->Username = 'webmaster@sep.esy.es';                 // SMTP username
			$mail->Password = 'sep011';                               // SMTP password
			$mail->SMTPSecure = 'tls';                            // Enable TLS encryption, `ssl` also accepted
			$mail->Port = 587;                                    // TCP port to connect to

			$mail->From = 'webmaster@sep.esy.es';
            $mail->FromName = 'Mailer';
            $mail->addAddress($toMail);     			// Add a recipient

            $mail->isHTML(true);                                  	// Set email format to HTML

            $mail->Subject = 'Tropical Vacations - Password reset request';
            $mail->Body = "<div align='center'><a href={$link}>Click here to change your password.</a><br><br>Alternatively, you can enter the following password reset code:<br>Your password reset code is <b>{$genNo}</b></div>";


            if (!$mail->send()) {
                echo 'Message could not be sent.';
                echo 'Mailer Error: ' . $mail->ErrorInfo;
            } else {
                echo 'Check your email for six-digit reset confirmation code';
                echo "<input type=\"text\" name=\"reset\" id=\"reset\" placeholder=\"Enter Reset Code\" required=\"required\" />";
                echo "<input type=\"password\" name=\"newpw\" id=\"newpw\" placeholder=\"Enter New Password\" required=\"required\" />";
                echo "<input type=\"password\" name=\"confpw\" id=\"confpw\" placeholder=\"Confirm Your Password\" required=\"required\" />";
                echo "<div align=\"right\">";
                echo "<input type=\"button\" align=\"right\" class=\"btn-minimal\"  name=\"send\" id=\"send\" value=\"Reset Password\" onclick=\"resetPwd(document.getElementById('email').value,document.getElementById('reset').value)\">";
                echo "</div>";
            }

        }

    }
}

if(isset($_POST["mail"]) && isset($_POST["code"]) && isset($_POST["pwd"])){
    $mail = $_POST["mail"];
    $code = $_POST["code"];
    $pwd = $_POST["pwd"];
    $encryptpw=md5($pwd);

    $query = "Select resetCode from admins WHERE email='{$mail}'";
    $result2 = mysqli_query($conn,$query) or die(mysqli_connect_error());
    while($row=mysqli_fetch_array($result2)){
        if($row[0]==$code) {
            $query2 = "UPDATE admins set password='{$encryptpw}' where email = '{$mail}'";
            /*$result=mysql_query($query2);
            $affectedRow = mysql_affected_rows();
            if($affectedRow == 1){
                $query = "UPDATE admins set resetCode=0 where email = '{$mail}'";
                $result=mysql_query($query);
                echo "success";
            }*/	
			if(mysqli_query($conn,$query2)){
				$query = "UPDATE admins set resetCode=0 where email = '{$mail}'";
                $result=mysqli_query($conn,$query);
                echo "success";
			}
        }
        else{
            echo "Invalid Reset Code";
        }
    }
}

?>