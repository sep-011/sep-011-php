/**
 * Created by Thilina on 18/08/2015.
 */

function getdetails(fname){
//alert("this is from table "+fname);


    var hr = new XMLHttpRequest();

    var url = "userdetails.php";
    var query = fname;
    var vars = "q=" + query;
    hr.open("POST", url, true);

    hr.setRequestHeader("Content-type", "application/x-www-form-urlencoded");

    hr.onreadystatechange = function () {
        if (hr.readyState == 4 && hr.status == 200) {
            var return_data = hr.responseText;
            document.getElementById("status").innerHTML = return_data;

        }
    }

    hr.send(vars);
    document.getElementById("status").innerHTML = "processing...";
    document.getElementById("form").innerHTML = "";

}


function revokeuser(uid,rsn){


    //alert("function runs");
    var hr = new XMLHttpRequest();

    var url = "revoke_user.php";
    var query = uid;
    var res = rsn;
    var vars = "q=" + query + "&r=" + res ;
    //var vars = "q=" + query;
    hr.open("POST", url, true);

    hr.setRequestHeader("Content-type", "application/x-www-form-urlencoded");

    hr.onreadystatechange = function () {
        if (hr.readyState == 4 && hr.status == 200) {
            var return_data = hr.responseText;
            document.getElementById("form").innerHTML = return_data;
            //alert(return_data);
        }
    }

    hr.send(vars);
    document.getElementById("form").innerHTML = "processing...";

}

function activateuser(uid,rsn){


    //alert("function runs");
    var hr = new XMLHttpRequest();

    var url = "useractivate.php";
    var query = uid;
    var res = rsn;
    var vars = "q=" + query + "&r=" + res ;
    //var vars = "q=" + query;
    hr.open("POST", url, true);

    hr.setRequestHeader("Content-type", "application/x-www-form-urlencoded");

    hr.onreadystatechange = function () {
        if (hr.readyState == 4 && hr.status == 200) {
            var return_data = hr.responseText;
            document.getElementById("form").innerHTML = return_data;
            //alert(return_data);
        }
    }

    hr.send(vars);
    document.getElementById("form").innerHTML = "processing...";

}


function revokeuserForm(uid){

    var hr = new XMLHttpRequest();

    var url = "form.php";
    var query = uid;
    var vars = "q=" + query;
    hr.open("POST", url, true);

    hr.setRequestHeader("Content-type", "application/x-www-form-urlencoded");

    hr.onreadystatechange = function () {
        if (hr.readyState == 4 && hr.status == 200) {
            var return_data = hr.responseText;
            document.getElementById("form").innerHTML = return_data;

        }
    }

    hr.send(vars);
    document.getElementById("form").innerHTML = "processing...";

}


function activateuserForm(uid){

    var hr = new XMLHttpRequest();

    var url = "acform.php";
    var query = uid;
    var vars = "q=" + query;
    hr.open("POST", url, true);

    hr.setRequestHeader("Content-type", "application/x-www-form-urlencoded");

    hr.onreadystatechange = function () {
        if (hr.readyState == 4 && hr.status == 200) {
            var return_data = hr.responseText;
            document.getElementById("form").innerHTML = return_data;

        }
    }

    hr.send(vars);
    document.getElementById("form").innerHTML = "processing...";

}

function clickbutton(uid){

    //alert("click");

    var reason= document.getElementById("message").value;

    if(reason == ""){
        //alert("Please fill the reason !!");
        document.getElementById("lbl").innerText="Please Fill";

    }
    else{
        revokeuser(uid,reason);

    }
}

function clickbutton2(uid){

    //alert("click");

    var reason= document.getElementById("message").value;

    if(reason == ""){
        //alert("Please fill the reason !!");
        document.getElementById("lbl").innerText="Please Fill";

    }
    else{
        activateuser(uid,reason);

    }
}




function yes(uid){

    //alert("yes");

    var hr = new XMLHttpRequest();

    var url = "removeComRateRep.php";
    var query = uid;
    var vars = "y=" + query;
    hr.open("POST", url, true);

    hr.setRequestHeader("Content-type", "application/x-www-form-urlencoded");

    hr.onreadystatechange = function () {
        if (hr.readyState == 4 && hr.status == 200) {
            var return_data = hr.responseText;
            document.getElementById("form").innerHTML = return_data;

        }
    }

    hr.send(vars);
    document.getElementById("form").innerHTML = "processing...";



}

function yestwo(uid){

    //alert("yes");

    var hr = new XMLHttpRequest();

    var url = "removeRepProb.php";
    var query = uid;
    var vars = "y=" + query;
    hr.open("POST", url, true);

    hr.setRequestHeader("Content-type", "application/x-www-form-urlencoded");

    hr.onreadystatechange = function () {
        if (hr.readyState == 4 && hr.status == 200) {
            var return_data = hr.responseText;
            document.getElementById("form").innerHTML = return_data;

        }
    }

    hr.send(vars);
    document.getElementById("form").innerHTML = "processing...";



}

function no(uid){

    //alert("no");
    var hr = new XMLHttpRequest();

    var url = "removeComRateRep.php";
    var query = uid;
    var vars = "n=" + query;
    hr.open("POST", url, true);

    hr.setRequestHeader("Content-type", "application/x-www-form-urlencoded");

    hr.onreadystatechange = function () {
        if (hr.readyState == 4 && hr.status == 200) {
            var return_data = hr.responseText;
            document.getElementById("form").innerHTML = return_data;

        }
    }

    hr.send(vars);
    document.getElementById("form").innerHTML = "processing...";

}


function notwo(uid){

    //alert("yes");

    var hr = new XMLHttpRequest();

    var url = "removeRepProb.php";
    var query = uid;
    var vars = "n=" + query;
    hr.open("POST", url, true);

    hr.setRequestHeader("Content-type", "application/x-www-form-urlencoded");

    hr.onreadystatechange = function () {
        if (hr.readyState == 4 && hr.status == 200) {
            var return_data = hr.responseText;
            document.getElementById("form").innerHTML = return_data;

        }
    }

    hr.send(vars);
    document.getElementById("form").innerHTML = "processing...";



}

function cancel(){

    document.getElementById("form").innerHTML = "";


}

function cleartags(){

    //alert("clear");
    document.getElementById("form").innerHTML = "";
    document.getElementById("status").innerHTML = "";



}

function clearother(uid){

    var hr = new XMLHttpRequest();

    var url = "ClearRevokeUserOther.php";
    var query = uid;
    var vars = "q=" + query;
    hr.open("POST", url, true);

    hr.setRequestHeader("Content-type", "application/x-www-form-urlencoded");

    hr.onreadystatechange = function () {
        if (hr.readyState == 4 && hr.status == 200) {
            var return_data = hr.responseText;
            document.getElementById("form").innerHTML = return_data;

        }
    }

    hr.send(vars);
    document.getElementById("form").innerHTML = "processing...";


}


function clearyes(uid){

    var hr = new XMLHttpRequest();

    var url = "ClearRevokeUserOther.php";
    var query = uid;
    var vars = "s=" + query;
    hr.open("POST", url, true);

    hr.setRequestHeader("Content-type", "application/x-www-form-urlencoded");

    hr.onreadystatechange = function () {
        if (hr.readyState == 4 && hr.status == 200) {
            var return_data = hr.responseText;
            document.getElementById("form").innerHTML = return_data;

        }
    }

    hr.send(vars);
    document.getElementById("form").innerHTML = "processing...";


}

function clearFromtag(){
    document.getElementById("form").innerHTML = "";

}

function clearothert(uid){


    var hr = new XMLHttpRequest();

    var url = "ClearRevokeUserOther.php";
    var query = uid;
    var vars = "fav=" + query;
    hr.open("POST", url, true);

    hr.setRequestHeader("Content-type", "application/x-www-form-urlencoded");

    hr.onreadystatechange = function () {
        if (hr.readyState == 4 && hr.status == 200) {
            var return_data = hr.responseText;
            document.getElementById("form").innerHTML = return_data;

        }
    }

    hr.send(vars);
    document.getElementById("form").innerHTML = "processing...";


}

function clearyest(uid){

    var hr = new XMLHttpRequest();

    var url = "ClearRevokeUserOther.php";
    var query = uid;
    var vars = "ss=" + query;
    hr.open("POST", url, true);

    hr.setRequestHeader("Content-type", "application/x-www-form-urlencoded");

    hr.onreadystatechange = function () {
        if (hr.readyState == 4 && hr.status == 200) {
            var return_data = hr.responseText;
            document.getElementById("form").innerHTML = return_data;

        }
    }

    hr.send(vars);
    document.getElementById("form").innerHTML = "processing...";


}