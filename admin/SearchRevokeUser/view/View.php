<?php
/**
 * Created by PhpStorm.
 * User: Thilina
 *
 *  Class is use to view some data
 */

class View
{

    /*  To search users
     * @param $string - search letter/word
     */
    public function Show_Users($string)
    {

        $selectString = "select * from userinfo where fname like '%$string%' or email like '%$string%'";
        $comments = mysql_query($selectString);

        ?>
        <table data-role="table" data-mode="columntoggle" class="ui-responsive ui-shadow" id="myTable">
            <?php
            while ($row = mysql_fetch_array($comments)) {

                ?>
                <tr><?php
                $username = $row['fname'];
                $email = $row['email'];
                $b_username = '<strong>' . $string . '</strong>';
                $b_email = '<strong>' . $string . '</strong>';
                $final_username = str_ireplace($string, $b_username, $username);
                $final_email = str_ireplace($string, $b_email, $email);

                ?>
                <div class="show" align="left" onclick="javascript:getdetails(<?php echo $row['uid'] ?>);">
                    <img src="images/default-user.png" style="width:50px; height:50px; float:left; margin-right:6px;"/>
                    <span class="name"><?php echo $final_username; ?></span>&nbsp;<br/><?php echo $final_email; ?><br/>
                </div>
                </tr>
            <?php


            }
            ?>
        </table>
    <?php


    }


    /*  To get selected user from search result
     * @param $uid - userid
     */
    public function Get_SelectedUser($uid)
    {


        $selectString = "select * From userinfo where uid={$uid}";
        ?>
        <br>
        <h2>User Basic Details</h2>
        <br>
        <table data-role="table" data-mode="columntoggle" id="myTable">
            <tr>
                <td><?php echo "User id" ?></td>
                <td><?php echo "First name" ?></td>
                <td><?php echo "Last name" ?></td>
                <td><?php echo "Email" ?></td>
            </tr>
            <tr>
                <?php
                $comments = mysql_query($selectString);
                while ($row = mysql_fetch_array($comments)) {
                    //echo $row['lname'];
                    ?>
                    <td><?php echo $row['uid'] ?></td>
                    <td><?php echo $row['fname'] ?></td>
                    <td><?php echo $row['lname'] ?></td>
                    <td><?php echo $row['email'] ?></td>


                <?php

                }
                ?></tr>

        </table>

        <?php
        $selectStringn = "select * from revoked WHERE uid= {$uid}";

        $commentsn = mysql_query($selectStringn);

        $row = mysql_fetch_array($commentsn);

        if (mysql_num_rows($commentsn) > 0 && $row['revoked'] == "yes") {
            ?>
            <br>
            <div>
                <button type="button" onclick="javascript:activateuserForm(<?php echo $uid ?>)">Activate
                    User
                </button>
            </div>



            <br>
            <hr>
            <br>

        <?php


        } else {
            ?>
            <br>
            <div>
                <button type="button" onclick="javascript:revokeuserForm(<?php echo $uid ?>)">Revoke
                    User
                </button>
            </div>



            <br>
            <hr>
            <br>

        <?php
        }


    }


    /*  To get details about revoked users
     * @param $num - dropdown value (0/1)
     */
    public function Get_RevokedDetails($num)
    {
        ?>
        <table>
            <?php
            if ($num == 0) {
                $selectString = "select * from revoked r,userinfo u where r.revoked=\"yes\" and r.uid=u.uid";

                $comments = mysql_query($selectString);

                ?>
                <div><h3>Revoked Users Details</h3></div>
                <br>
                <tr>
                    <td><?php echo "User ID"; ?></td>
                    <td><?php echo "First Name"; ?></td>
                    <td><?php echo "Last Name"; ?></td>
                    <td><?php echo "Email"; ?></td>
                    <td><?php echo "Revoked"; ?></td>
                    <td><?php echo "Revoked Date"; ?></td>
                    <td><?php echo "Revoked Reason"; ?></td>
                    <td><?php echo "Re Activate Count"; ?></td>
                    <td><?php echo "Activate User"; ?></td>
                </tr>

                <?php
                while ($row = mysql_fetch_array($comments)) {
                    ?>
                    <tr>
                        <td><?php echo $row['uid'] ?></td>
                        <td><?php echo $row['fname'] ?></td>
                        <td><?php echo $row['lname'] ?></td>
                        <td><?php echo $row['email'] ?></td>
                        <td><?php echo $row['revoked'] ?></td>
                        <td><?php echo $row['revokedDate'] ?></td>
                        <td><?php echo $row['revokereason'] ?></td>
                        <td><?php echo $row['reactivate'] ?></td>
                        <td>
                            <button onclick="javascript:activateuserForm(<?php echo $row['uid'] ?>)">Activate</button>
                        </td>
                    </tr>
                <?php
                }
            } else {
                $selectString = "select * from revoked r,userinfo u where r.revoked=\"no\" and r.uid=u.uid";

                $comments = mysql_query($selectString);

                ?>
                <div><h3>Re Activated Users Details</h3></div>
                <br>
                <tr>
                    <td><?php echo "User ID" ?></td>
                    <td><?php echo "First Name" ?></td>
                    <td><?php echo "Last Name" ?></td>
                    <td><?php echo "Email" ?></td>
                    <td><?php echo "Revoked" ?></td>
                    <td><?php echo "Activate Date" ?></td>
                    <td><?php echo "ReActivate Reason" ?></td>
                    <td><?php echo "Re Activate Count" ?></td>
                    <td><?php echo "De Activate" ?></td>
                </tr>
                <?php
                while ($row = mysql_fetch_array($comments)) {
                    ?>
                    <tr>
                        <td><?php echo $row['uid'] ?></td>
                        <td><?php echo $row['fname'] ?></td>
                        <td><?php echo $row['lname'] ?></td>
                        <td><?php echo $row['email'] ?></td>
                        <td><?php echo $row['revoked'] ?></td>
                        <td><?php echo $row['activatedate'] ?></td>
                        <td><?php echo $row['reactreason'] ?></td>
                        <td><?php echo $row['reactivate'] ?></td>
                        <td>
                            <button onclick="javascript:revokeuserForm(<?php echo $row['uid'] ?>)">DeActivate</button>
                        </td>
                    </tr>
                <?php


                }
                ?>  <?php
            }

            ?>
        </table>

    <?php

    }


    /*  To get Favorite locations order by location name
     *  with user details
     */
    public function Get_FavLocationDet()
    {



        $selectString ="select * from favplaces f,locations l where l.Location_id=f.placeid order by l.Location_Name";
        $comments= mysql_query($selectString);

        ?>
        <table>

            <tr>

                <td><?php echo "Image" ?></td>
                <td><?php echo "Location Name" ?></td>
                <td><?php echo "Area" ?></td>
                <td><?php echo "User Details" ?></td>


            </tr>
            <?php

            while($row = mysql_fetch_array($comments))
            {

                ?>

                <tr>

                    <td><?php echo "<div align=\"center\"> <img src=\"{$row['Image']}\" width=\"75\" height=\"75\" border=\"3\"> </div>"; ?></td>
                    <td><?php echo $row['Location_Name'] ?></td>
                    <td><?php echo $row['Area'].",".$row['District'] ?></td>

                    <?php

                        $selectString2 = "Select * from userinfo where uid={$row['userId']}";
                        $comments2 = mysql_query($selectString2);
                        while ( $row2 = mysql_fetch_array($comments2))
                        {

                            ?>
                            <td>
                                <?php
                                echo "FirstName : ".$row2['fname'];
                                echo '<br>';
                                echo "LastName : ".$row2['lname'];
                                echo '<br>';
                                echo "Email : ".$row2['email'];
                                ?>
                            </td>
                            <?php

                        }
                    ?>


                </tr>

            <?php

            }

            ?>
        </table>

        <?php

    }







} 