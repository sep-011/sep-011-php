<?php
/**
 * Created by PhpStorm.
 * User: Thilina
 *
 * Class is use to load forms
 */

class forms {

    /*  revoke form
     * @param $uid - userid
     */
    public function revoke_form($uid)
    {

        ?>

        <html>

        <head>
            <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
            <title>Contact Form</title>
            <style>
                label {
                    display:block;
                    margin-top:20px;
                    letter-spacing:2px;
                }

                /* Centre the page */
                .body {
                    display:block;
                    margin:0 auto;
                    width:576px;
                }

                /* Centre the form within the page */
                form {
                    margin:0 auto;
                    width:459px;
                }

                /* Style the text boxes */
                input, textarea {
                    width:439px;
                    height:27px;
                    background:#efefef;
                    border:1px solid #dedede;
                    padding:10px;
                    margin-top:3px;
                    font-size:0.9em;
                    color:#3a3a3a;
                }

                textarea {
                    height:213px;

                }

                input, textarea {
                    width:439px;
                    height:27px;
                    background:#efefef;
                    border:1px solid #dedede;
                    padding:10px;
                    margin-top:3px;
                    font-size:0.9em;
                    color:#3a3a3a;
                    -moz-border-radius:5px;
                    -webkit-border-radius:5px;
                    border-radius:5px;
                }

                input:focus, textarea:focus {
                    border:1px solid #97d6eb;
                }

                #submit {
                    width:127px;
                    height:38px;
                    background:url(../images/submit.jpg);
                    text-indent:-9999px;
                    border:none;
                    margin-top:20px;
                    cursor:pointer;
                }

                #submit:hover {
                    opacity:.9;
                }

                #lbl{
                    display: inline-block ;
                    color: crimson;
                    margin-left: 20px;

                }

                #submit2{
                    width:127px;
                    height:38px;
                    background:url(../images/cancel.png);
                    text-indent:-9999px;
                    border:none;
                    margin-top:20px;
                    cursor:pointer;

                }

                #im:hover {
                    opacity:.9;
                }

            </style>



        </head>

        <body>

            <header class="body">
            </header>

            <section class="body">

                <h3>User Revoking</h3>

                <label>Date</label>
                <input name="Date" placeholder="<?php echo date("Y/m/d") ?>" readonly>


                <label>Reason for revoking</label>
                <textarea  id="message" name="message" placeholder="Type Here"></textarea><label id="lbl">*</label>



                <img id="im" onclick="clickbutton(<?php echo $uid ?>)" src="images/ok.png" style="width:127px;height:34px;">

                <img id="im"onclick="cancel()" src="images/cancel.png" style="width:127px;height:34px;">





            </section>

            <footer class="body">
            </footer>


        </body>

        </html>

        <?php

    }



    /*  activation form
     * @param $uid - userid
     */
    public function activation_form($uid)
    {

        ?>

        <html>

        <head>
            <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
            <title>Contact Form</title>
            <style>
                label {
                    display:block;
                    margin-top:20px;
                    letter-spacing:2px;
                }

                /* Centre the page */
                .body {
                    display:block;
                    margin:0 auto;
                    width:576px;
                }

                /* Centre the form within the page */
                form {
                    margin:0 auto;
                    width:459px;
                }

                /* Style the text boxes */
                input, textarea {
                    width:439px;
                    height:27px;
                    background:#efefef;
                    border:1px solid #dedede;
                    padding:10px;
                    margin-top:3px;
                    font-size:0.9em;
                    color:#3a3a3a;
                }

                textarea {
                    height:213px;

                }

                input, textarea {
                    width:439px;
                    height:27px;
                    background:#efefef;
                    border:1px solid #dedede;
                    padding:10px;
                    margin-top:3px;
                    font-size:0.9em;
                    color:#3a3a3a;
                    -moz-border-radius:5px;
                    -webkit-border-radius:5px;
                    border-radius:5px;
                }

                input:focus, textarea:focus {
                    border:1px solid #97d6eb;
                }

                #submit {
                    width:127px;
                    height:38px;
                    background:url(../images/submit.jpg);
                    text-indent:-9999px;
                    border:none;
                    margin-top:20px;
                    cursor:pointer;
                }

                #submit:hover {
                    opacity:.9;
                }

                #lbl{
                    display: inline-block ;
                    color: crimson;
                    margin-left: 20px;

                }

                #submit2{
                    width:127px;
                    height:38px;
                    background:url(../images/cancel.png);
                    text-indent:-9999px;
                    border:none;
                    margin-top:20px;
                    cursor:pointer;

                }

                #im:hover {
                    opacity:.9;
                }


            </style>



        </head>

        <body>


            <header class="body">
            </header>

            <section class="body">

                <h3>User Activating</h3>

                <label>Date</label>
                <input name="Date" placeholder="<?php echo date("Y/m/d") ?>" readonly>


                <label>Reason for re activating</label>
                <textarea  id="message" name="message" placeholder="Type Here"></textarea><label id="lbl">*</label>



                <img id="im" onclick="clickbutton2(<?php echo $uid ?>)" src="images/ok.png" style="width:127px;height:34px;">

                <img id="im"onclick="cancel()" src="images/cancel.png" style="width:127px;height:34px;">





            </section>

            <footer class="body">
            </footer>


        </body>

        </html>


        <?php



    }

} 