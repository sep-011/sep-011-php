<?php
/**
 * Created by PhpStorm.
 * User: Thilina
 *
 * Class is use to simplify some database access
 */

class Model {

    /*  To do database transaction ( add/update/delete)
     * @param $query - sql query
     */
    public function transaction($query)
    {
        if (!mysql_query($query)) {
            (/*'Error : '.mysql_error()*/
            'Erorr occurred ');

            return false;

        } else {
            return true;
        }


    }

    /*  To get one value
     * @param $query - sql query
     * @param $colname - column name to retrieve
     */
    public function Get_onevalue($query,$colname){

        $selectString=$query;
        $comments=mysql_query($selectString);
        $row=mysql_fetch_array($comments);

        return $row["{$colname}"];

    }

    /*  To check records are available
     * @param $query - sql query
     */
    public function Check_records($query)
    {

        $comments = mysql_query($query);

        if(mysql_num_rows($comments) > 0 )
            return true;
        else
            return false;

        
    }

} 