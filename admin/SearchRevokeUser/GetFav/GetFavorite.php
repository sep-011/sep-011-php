<html>

<head>

    <style type="text/css">

        table, th, td {
            border: 1px solid black;
        }

        table {
            width: 100%;
        }

        th {
            height: 50px;
        }
        table, td, th {
            border: 1px solid green;
        }
        th {
            background-color: green;
            color: white;
        }

    </style>

<script type="text/javascript">

    function GetFavPlaces(){

        var hr = new XMLHttpRequest();

        var url = "controller/requests.php";

        var vars = "fav=" + "a";

        hr.open("POST", url, true);

        hr.setRequestHeader("Content-type", "application/x-www-form-urlencoded");

        hr.onreadystatechange = function () {
            if (hr.readyState == 4 && hr.status == 200) {
                var return_data = hr.responseText;
                document.getElementById("fav").innerHTML = return_data;

            }
        }

        hr.send(vars);
        document.getElementById("fav").innerHTML = "processing...";

    }

</script>

</head>
<body>


<div class="container" >

    <h3>Users Most Travelled Places</h3>
    <br>
    <button onclick="GetFavPlaces();">Get Details</button>
    <br>
    <div id="fav"></div>

</div>


</body>


</html>