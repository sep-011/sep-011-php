<html>

<style type="text/css">

    #searchid
    {
        width:100%;
        border:solid 1px #000;
        padding:10px;
        font-size:14px;

        height:27px;
        background:#efefef;
        color:#3a3a3a;
        -moz-border-radius:5px;
        -webkit-border-radius:5px;
        border-radius:5px;

    }

    table, th, td {
        border: 1px solid black;
    }

    table {
        width: 100%;
    }

    th {
        height: 50px;
    }
    table, td, th {
        border: 1px solid green;
    }
    th {
        background-color: green;
        color: white;
    }


</style>



<script>

    // To select User
    function showResult(str) {



        if (str.length == 0) {
            document.getElementById("livesearch").innerHTML = "";
            document.getElementById("livesearch").style.border = "0px";
            return;
        }

        xmlhttp = new XMLHttpRequest();

        xmlhttp.onreadystatechange = function () {
            if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
                document.getElementById("livesearch").innerHTML = xmlhttp.responseText;
                document.getElementById("livesearch").style.border = "1px solid #A5ACB2";


            }
        }
        xmlhttp.open("GET", "controller/requests.php?q=" + str, true);

        xmlhttp.send();


    }


    function getdetails(uid){


        var hr = new XMLHttpRequest();

        var url = "controller/requests.php";
        var query = uid;
        var vars = "uid=" + query;
        hr.open("POST", url, true);

        hr.setRequestHeader("Content-type", "application/x-www-form-urlencoded");

        hr.onreadystatechange = function () {
            if (hr.readyState == 4 && hr.status == 200) {
                var return_data = hr.responseText;
                document.getElementById("status").innerHTML = return_data;

            }
        }

        hr.send(vars);
        document.getElementById("status").innerHTML = "processing...";
        document.getElementById("form").innerHTML = "";

    }

    function revokeuserForm(uid){

        var hr = new XMLHttpRequest();

        var url = "controller/formsrequest.php";
        var query = uid;

        var vars = "uid=" + query;

        hr.open("POST", url, true);

        hr.setRequestHeader("Content-type", "application/x-www-form-urlencoded");

        hr.onreadystatechange = function () {
            if (hr.readyState == 4 && hr.status == 200) {
                var return_data = hr.responseText;
                document.getElementById("form").innerHTML = return_data;

            }
        }

        hr.send(vars);
        document.getElementById("form").innerHTML = "processing...";

    }


    function activateuserForm(uid){

        var hr = new XMLHttpRequest();

        var url = "controller/formsrequest.php";
        var query = uid;

        var vars = "usid=" + query;

        hr.open("POST", url, true);

        hr.setRequestHeader("Content-type", "application/x-www-form-urlencoded");

        hr.onreadystatechange = function () {
            if (hr.readyState == 4 && hr.status == 200) {
                var return_data = hr.responseText;
                document.getElementById("form").innerHTML = return_data;

            }
        }

        hr.send(vars);
        document.getElementById("form").innerHTML = "processing...";

    }

    // To validatin for the revoke form
    function clickbutton(uid){

        var reason= document.getElementById("message").value;

        if(reason == ""){
            alert("Please fill the reason !!");
            document.getElementById("lbl").innerText="Please Fill";

        }
        else{
            revokeuser(uid,reason);

        }
    }

    // To validatin for the activation form
    function clickbutton2(uid){


        var reason= document.getElementById("message").value;

        if(reason == ""){
            alert("Please fill the reason !!");
            document.getElementById("lbl").innerText="Please Fill";

        }
        else{
            activateuser(uid,reason);

        }
    }



    function revokeuser(uid,rsn){

        var query = uid;
        var res = rsn;

        xmlhttp = new XMLHttpRequest();

        xmlhttp.onreadystatechange = function () {
            if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
                document.getElementById("form").innerHTML = xmlhttp.responseText;

            }
        }
        xmlhttp.open("GET", "controller/transrequests.php?uid=" + query + "&rsn=" + res, true);

        xmlhttp.send();

        document.getElementById("form").innerHTML = "processing...";
    }

    function activateuser(uid,rsn){

        var query = uid;
        var res = rsn;

        xmlhttp = new XMLHttpRequest();

        xmlhttp.onreadystatechange = function () {
            if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
                document.getElementById("form").innerHTML = xmlhttp.responseText;

            }
        }
        xmlhttp.open("GET", "controller/transrequests.php?uida=" + query + "&rsna=" + res, true);

        xmlhttp.send();

        document.getElementById("form").innerHTML = "processing...";


    }

    //form cancel
    function cancel(){

        document.getElementById("form").innerHTML = "";


    }

    function cleartags(){

        document.getElementById("form").innerHTML = "";
        document.getElementById("status").innerHTML = "";

    }



</script>

<body>

<input type="text" id="searchid" size="50" onkeyup="showResult(this.value)">
<div id="livesearch"></div>

<br>

<div id="status"></div>

<br>

<div id="form"></div>

</body>


</html>


