<?php

include_once($_SERVER["DOCUMENT_ROOT"].'/admin/SearchRevokeUser/dbconnection/connection.php');
include_once($_SERVER["DOCUMENT_ROOT"].'/admin/SearchRevokeUser/view/View.php');
include_once($_SERVER["DOCUMENT_ROOT"].'/admin/SearchRevokeUser/Model/Model.php');


$connection=new connection();
$constat=$connection->connect();
$view= new View();
$model = new Model();

if(!$constat)
{
    echo "Server connection terminated. please try again in few seconds";

}
else
{
    /*
     * To call the method for search
     * users
     */
    if (isset($_GET['q'])) {
        $string = $_GET["q"];

        $view->Show_Users($string);
    }

    /*
     * To call the method for
     * form load selected user details
     */
    if (isset($_POST['uid'])) {
        $uid = $_POST["uid"];

        $view->Get_SelectedUser($uid);
    }

    /*
     * To call the method for
     * Get revoked details
     */
    if (isset($_POST['n'])) {
        $num = $_POST["n"];

        $view->Get_RevokedDetails($num);
    }

    /*
     * To call the method for
     * favorite location details of the user
     */
    if (isset($_POST['fav'])) {


        $view->Get_FavLocationDet();
    }


}

?>