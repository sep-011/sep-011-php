<?php
include_once($_SERVER["DOCUMENT_ROOT"].'/admin/SearchRevokeUser/dbconnection/connection.php');
include_once($_SERVER["DOCUMENT_ROOT"].'/admin/SearchRevokeUser/view/View.php');
include_once($_SERVER["DOCUMENT_ROOT"].'/admin/SearchRevokeUser/Model/Model.php');
include_once($_SERVER["DOCUMENT_ROOT"].'/admin/SearchRevokeUser/view/forms.php');


$connection=new connection();
$constat=$connection->connect();
$view= new View();
$forms=new forms();
$model = new Model();

if(!$constat)
{
    echo "Server connection terminated. please try again in few seconds";

}
else
{
    /*
     * To call the method for revoke
     * form load
     */
    if (isset($_POST['uid'])) {
        $uid = $_POST["uid"];

        $forms->revoke_form($uid);
    }

    /*
     * To call the method for activation
     * form load
     */
    if (isset($_POST['usid'])) {
        $uid = $_POST["usid"];

        $forms->activation_form($uid);
    }
}