<?php

include_once($_SERVER["DOCUMENT_ROOT"].'/admin/SearchRevokeUser/dbconnection/connection.php');
include_once($_SERVER["DOCUMENT_ROOT"].'/admin/SearchRevokeUser/view/View.php');
include_once($_SERVER["DOCUMENT_ROOT"].'/admin/SearchRevokeUser/Model/Model.php');


$connection=new connection();
$constat=$connection->connect();
$view= new View();
$model = new Model();

if(!$constat)
{
    echo "Server connection terminated. please try again in few seconds";

}
else {

    /*
     * To save user revoked details
     */
    if (isset($_GET["uid"]) & isset($_GET["rsn"])) {

        $userid= $_GET["uid"];
        $date = date("Y/m/d");
        $reason = $_GET["rsn"];

        $selectString="select * from revoked where uid={$userid}";

        $stat = $model->Check_records($selectString);

        if($stat)
        {
            $String="Update revoked set revoked=\"yes\",revokedDate=\"$date\",revokereason=\"$reason\" where uid=$userid";
        }
        else{
            $String = "insert into revoked (uid,revoked,revokedDate,revokereason,reactivate,activatedate,reactreason) VALUES({$userid},'yes',\"$date\",\"$reason\",0,\"NoRecord\",\"NoRecord\");";
        }

        $success= $model->transaction($String);

        if($success)
        {
            echo '<br/>';
            echo 'User Revoked Successful !';
            ?>
            <img src="images/success.png" height="22px" width="22px">
            <?php
            echo '<br/>';
            ?>
            <button onclick="cleartags()">Ok</button>
        <?php

        }
        else
        {
            echo '<br/>';
            echo 'User Revoked UnSuccessful ! Erorr occurred';
        }

    }


    /*
     * To save user reactivate details
     */
    if (isset($_GET["uida"]) & isset($_GET["rsna"])) {

        $userid= $_GET["uida"];
        $date = date("Y/m/d");
        $reason = $_GET["rsna"];

        $selectString="select reactivate from revoked where uid={$userid}";
        $count = $model->Get_onevalue($selectString,"reactivate");

        $count++;

        $updateString="Update revoked set revoked=\"no\",activatedate=\"$date\",reactreason=\"$reason\",reactivate=$count where uid=$userid";

        $stat = $model->transaction($updateString);

        if($stat)
        {
            echo '<br/>';
            echo 'User activated Successful !';
            ?>
            <img src="images/success.png" height="22px" width="22px">
            <?php
            echo '<br/>';
            ?>
            <button onclick="cleartags()">Ok</button>
        <?php

        }
        else{
            echo '<br/>';
            echo 'User activated UnSuccessful !.';
        }

    }


}