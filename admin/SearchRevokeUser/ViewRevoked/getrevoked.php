<html>
<head>

    <style type="text/css">

        table, th, td {
            border: 1px solid black;
        }

        table {
            width: 100%;
        }

        th {
            height: 50px;
        }
        table, td, th {
            border: 1px solid green;
        }
        th {
            background-color: green;
            color: white;
        }

    </style>

    <script src="../js/rev.js"></script>

    <script type="text/javascript">

        function getFilterData(){

            var hr = new XMLHttpRequest();

            var url = "controller/requests.php";

            var query;
            if(document.getElementById('opt').selectedIndex == 0)
            {
                query=0;
            }
            else if(document.getElementById('opt').selectedIndex == 1){
                query=1;
            }

            var vars = "n=" + query;
            hr.open("POST", url, true);

            hr.setRequestHeader("Content-type", "application/x-www-form-urlencoded");

            hr.onreadystatechange = function () {
                if (hr.readyState == 4 && hr.status == 200) {
                    var return_data = hr.responseText;
                    document.getElementById("filterdata").innerHTML = return_data;

                }
            }

            hr.send(vars);
            document.getElementById("filterdata").innerHTML = "processing...";



        }


        function revokeuserForm(uid){

            var hr = new XMLHttpRequest();

            var url = "controller/formsrequest.php";
            var query = uid;

            var vars = "uid=" + query;

            hr.open("POST", url, true);

            hr.setRequestHeader("Content-type", "application/x-www-form-urlencoded");

            hr.onreadystatechange = function () {
                if (hr.readyState == 4 && hr.status == 200) {
                    var return_data = hr.responseText;
                    document.getElementById("form").innerHTML = return_data;

                }
            }

            hr.send(vars);
            document.getElementById("form").innerHTML = "processing...";

        }


        function activateuserForm(uid){

            var hr = new XMLHttpRequest();

            var url = "controller/formsrequest.php";
            var query = uid;

            var vars = "usid=" + query;

            hr.open("POST", url, true);

            hr.setRequestHeader("Content-type", "application/x-www-form-urlencoded");

            hr.onreadystatechange = function () {
                if (hr.readyState == 4 && hr.status == 200) {
                    var return_data = hr.responseText;
                    document.getElementById("form").innerHTML = return_data;

                }
            }

            hr.send(vars);
            document.getElementById("form").innerHTML = "processing...";

        }

        // To validatin for the revoke form
        function clickbutton(uid){

            var reason= document.getElementById("message").value;

            if(reason == ""){
                alert("Please fill the reason !!");
                document.getElementById("lbl").innerText="Please Fill";

            }
            else{
                revokeuser(uid,reason);

            }
        }

        // To validatin for the activation form
        function clickbutton2(uid){


            var reason= document.getElementById("message").value;

            if(reason == ""){
                alert("Please fill the reason !!");
                document.getElementById("lbl").innerText="Please Fill";

            }
            else{
                activateuser(uid,reason);

            }
        }



        function revokeuser(uid,rsn){

            var query = uid;
            var res = rsn;

            xmlhttp = new XMLHttpRequest();

            xmlhttp.onreadystatechange = function () {
                if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
                    document.getElementById("form").innerHTML = xmlhttp.responseText;

                }
            }
            xmlhttp.open("GET", "controller/transrequests.php?uid=" + query + "&rsn=" + res, true);

            xmlhttp.send();

            document.getElementById("form").innerHTML = "processing...";
        }

        function activateuser(uid,rsn){

            var query = uid;
            var res = rsn;

            xmlhttp = new XMLHttpRequest();

            xmlhttp.onreadystatechange = function () {
                if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
                    document.getElementById("form").innerHTML = xmlhttp.responseText;

                }
            }
            xmlhttp.open("GET", "controller/transrequests.php?uida=" + query + "&rsna=" + res, true);

            xmlhttp.send();

            document.getElementById("form").innerHTML = "processing...";


        }

        //form cancel
        function cancel(){

            document.getElementById("form").innerHTML = "";


        }

        function cleartags(){

            document.getElementById("form").innerHTML = "";
            document.getElementById("status").innerHTML = "";
            document.getElementById("filterdata").innerHTML = "";

        }

    </script>



</head>
<body>

<div class="container" >

    <p>Select an Option to Filter the View</p>
    <select name="Filter" id="opt">
        <option value=1>Revoked Users</option>
        <option value=2>ReActivated Users</option>
    </select>
    <button onclick="getFilterData()">Get Details</button>

    <br>
    <br>
    <div id="filterdata"></div>

    <div id="status"></div>

    <div id="msg"></div>
    <div id="form"></div>



</div>




</body>

</html>