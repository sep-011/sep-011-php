<?php
/**
 *
 * Use to control the transactions request from the application
 */

include_once($_SERVER["DOCUMENT_ROOT"].'/sep/plan/Model/Model.php');
include_once($_SERVER["DOCUMENT_ROOT"].'/sep/plan/connection.php');
include_once($_SERVER["DOCUMENT_ROOT"].'/sep/plan/Views/View.php');


$connection=new connection();
$model = new Model();
$constat=$connection->connect();
$view= new View();

if(!$constat)
{
    echo "Server connection terminated. please try again in few seconds";

}
else{


    /*
     * To receive plan details and save plan
     * transaction() method is used
     */
    if(isset($_GET['pname']) && isset($_GET['adtr']) && isset($_GET['chtr']) && isset($_GET['bcls']) && isset($_GET['tm']) && isset($_GET['locid']) && isset($_GET['id']))
    {

        $uid = $_GET["id"];
        $pname = $_GET["pname"];
        $adtr = $_GET["adtr"];
        $chtr = $_GET["chtr"];
        $bcls = $_GET["bcls"];
        $tm = $_GET["tm"];
        $locid = $_GET["locid"];



            $insertString = "insert into usertravelplan (planName,adults,child,budgetclass,travelmethod,origin,uid)
                              values (\"$pname\",{$adtr},{$chtr},{$bcls},{$tm},{$locid},{$uid})";


            $stat = $model->transaction($insertString);

            if (!$stat) {
                echo "Erorr occurred";
            } else {

                echo "Plan Added Successfully";

            }



    }


    /*
     * To receive location id user id and insert
     * transaction() method is used
     */
    if(isset($_POST['locid']) && isset($_POST['uid']))
    {
        $locid=$_POST['locid'];
        $uid = $_POST['uid'];

        $insertString= "insert into dplaces (uid,locid) values ($uid,$locid)";

        $stat = $model->transaction($insertString);

        if (!$stat) {
            echo "Erorr occurred";
        } else {

            echo "Added Successfully";

        }

    }


    /*
     * To receive location id user id and delete
     * transaction() method is used
     */
    if(isset($_POST['lid']) && isset($_POST['id']))
    {
        $locid=$_POST['lid'];
        $uid = $_POST['id'];

        $DeleteString= "delete from dplaces where uid = $uid and locid = $locid";

        $stat = $model->transaction($DeleteString);

        if (!$stat) {
            echo "Erorr occurred";
        } else {

            echo "Removed Successfully";

        }

    }

    /*
     * To receive location id user id plan id distance and duration
     * save data
     * transaction() method is used
     */
    if(isset($_GET['pid']) && isset($_GET['locid']) && isset($_GET['dist']) && isset($_GET['du']) && isset($_GET['uid']))
    {

        $pid=$_GET['pid'];
        $locid=$_GET['locid'];
        $dist = $_GET['dist'];
        $du = $_GET['du'];
        $uid = $_GET['uid'];

        $insertString= "insert into plan_locations (planid, locid, distance, duration)
                          values ($pid,$locid,$dist,\"$du\")";

        $stat = $model->transaction($insertString);

        if (!$stat) {
            echo "Erorr occurred";
        } else {

            $DeleteString= "delete from dplaces where uid = $uid and locid = $locid";
            $stat2 = $model->transaction($DeleteString);

            if (!$stat2) {
                echo "Erorr occurred";
            } else {

                echo "Added Successfully";
                echo '<br>';
                echo "Please Choose Show Time and Distance button to get distance and duration from previous location to rest of the desired places";

            }


        }


    }

    /*
     * To receive location id user id plan id rooms and days
     * update data
     * transaction() method is used
     */
    if(isset($_GET['loid']) && isset($_GET['hid']) && isset($_GET['plid']) && isset($_GET['rooms']) && isset($_GET['day']))
    {
        $loid=$_GET['loid'];
        $hid=$_GET['hid'];
        $pid = $_GET['plid'];
        $rooms=$_GET['rooms'];
        $days=$_GET['day'];

        $updateString="UPDATE plan_locations SET hotelid={$hid},days={$days},rooms={$rooms} WHERE planid = {$pid} and locid={$loid}";


        $stat = $model->transaction($updateString);

        if (!$stat) {
            echo "Erorr occurred";
        } else {

            echo "Added Successfully";

        }

    }


    /*
     * To receive user id plan id
     * save destination to plan
     * transaction() method used
     * Get_onevalue() method used
     */
    if(isset($_POST['userid']) && isset($_POST['planid']))
    {
        $pid=$_POST['planid'];
        $uid = $_POST['userid'];

        $selectString = "SELECT max( planid ) AS max FROM `usertravelplan` WHERE uid =$uid";

        $max=$model->Get_onevalue($selectString,"max");

        $selectloc = "select locid from plan_locations where planid=$max";

        $loc=$model->Get_onevalue($selectloc,"locid");

        $updateString="UPDATE usertravelplan SET destination={$loc} WHERE planid = {$pid}";


        $stat = $model->transaction($updateString);

        if (!$stat) {
            echo "Erorr occurred";
        } else {

            echo "Added Successfully";

        }

    }


}



?>