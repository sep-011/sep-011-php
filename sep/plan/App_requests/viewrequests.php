<html>
<body>

<?php
/**
 *
 * Use to control the view request from the application
 */

include_once($_SERVER["DOCUMENT_ROOT"].'/sep/plan/Model/Model.php');
include_once($_SERVER["DOCUMENT_ROOT"].'/sep/plan/connection.php');
include_once($_SERVER["DOCUMENT_ROOT"].'/sep/plan/Views/View.php');


$connection=new connection();
$model = new Model();
$constat=$connection->connect();
$view= new View();

if(!$constat)
{
    echo "Server connection terminated. please try again in few seconds";

}
else{


    /*
     * LiveSearch() method is used
     */
    if(isset($_GET['q']) && isset($_GET['type']))
    {
        $string=$_GET["q"];
        $type=$_GET["type"];

        $view->LiveSearch($string,$type);
    }

    /*
     * View_Origin() method is used
     */
    if(isset($_POST['locid']))
    {
        $id=$_POST['locid'];

        $view->View_Origin($id);

    }

    /*
     * View_Dplaces() method is used
     */
    if(isset($_POST['did']))
    {
        $id=$_POST['did'];

        $view->View_Dplaces($id);

    }


    /*
     * View_AllDplaces() method is used
     */
    if(isset($_POST['ad']))
    {

        $uid=$_POST['ad'];
        $view->View_AllDplaces($uid);

    }

    /*
     * Get_groupId() method is used
     */
    if(isset($_POST['id']))
    {

        $uid=$_POST['id'];
        $view->Get_groupId($uid);

    }

    /*
     * Get_latlnt() method is used
     */
    if(isset($_POST['uid']) && isset($_POST['pid']))
    {
        $uid=$_POST["uid"];
        $pid=$_POST["pid"];

        $view->Get_latlnt($uid,$pid);
    }

    /*
     * Get_latlntMod() method is used
     */
    if(isset($_POST['userid']) && isset($_POST['planid']))
    {
        $uid=$_POST["userid"];
        $pid=$_POST["planid"];

        $view->Get_latlntMod($uid,$pid);
    }


    /*
     * get_plan_places() method is used
     */
    if(isset($_POST['pid']))
    {

        $pid=$_POST['pid'];
        $view->get_plan_places($pid);

    }

    /*
     * View_hotels() method is used
     */
    if(isset($_POST['location']) && isset($_POST['plid']))
    {
        $id=$_POST['location'];
        $pid=$_POST['plid'];

        $view->View_hotels($id,$pid);

    }

    /*
     * Show_results() method is used
     */
    if(isset($_POST['usid']) && isset($_POST['pnid']))
    {
        $id=$_POST['usid'];
        $pid=$_POST['pnid'];

        $view->Show_results($id,$pid);



    }

    /*
     * SelectPlan() method is used
     */
    if(isset($_POST['user_id']) && isset($_POST['plan_id']))
    {
        $id=$_POST['user_id'];
        $pid=$_POST['plan_id'];

        $view->SelectPlan($id,$pid);



    }

    /*
     * Get_Selectedpid() method is used
     */
    if(isset($_POST['selpid']))
    {

        $pid=$_POST['selpid'];
        $view->Get_Selectedpid($pid);

    }


}




?>

</body>
</html>