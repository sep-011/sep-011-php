<?php
/**
 *
 * Use to view data through application
 */
include_once($_SERVER["DOCUMENT_ROOT"].'/sep/plan/Model/Model.php');

class View {



    /*
     * To show search results
     * @param $string - letter/word
     * @param $type - number (1/2)
     */
    public function LiveSearch($string,$type)
    {

        $selectString = "select * from locations where Location_Name LIKE '%$string%'";
        $comments=mysql_query($selectString);

        ?>
        <table data-role="table" data-mode="columntoggle" class="ui-responsive ui-shadow" id="myTable" >
        <?php
        while($row=mysql_fetch_array($comments))
        {

            ?>
            <tr>
                <td><?php echo "<div align=\"center\"> <img src=\"{$row['Image']}\" width=\"75\" height=\"75\" border=\"3\"> </div>"; ?></td>
                <td><?php echo $row['Location_Name']; ?></td>
                <?php if ($type == 1) { ?>
                <td><a href="javascript:ShowSelect(<?php echo $row['Location_id'] ?>);" class="ui-btn ui-btn">Select</a></td>
                <?php }?>
                <?php if ($type == 2) {?>
                <td><a href="javascript:Showdplaces(<?php echo $row['Location_id'] ?>);" class="ui-btn ui-btn">Select</a></td>
                <?php }?>
            </tr>
        <?php


        }
        ?>
        </table>
        <?php

    }

    /*
     * To view selected place details as origin
     * @param $locid - location id
     */
    public function View_Origin($locid)
    {
        $selectString = "select * from locations where Location_id = {$locid}";
        $comments=mysql_query($selectString);

        ?>
        <table data-role="table" data-mode="columntoggle" class="ui-responsive ui-shadow" id="myTable" >
            <?php
            while($row=mysql_fetch_array($comments))
            {

                ?>
                <tr>
                    <td><?php echo "<div align=\"center\"> <img src=\"{$row['Image']}\" width=\"75\" height=\"75\" border=\"3\"> </div>"; ?></td>
                    <td><?php echo $row['Location_Name']; ?></td>

                </tr>
            <?php


            }
            ?>
        </table>
    <?php
    }


    /*
     * To view selected place details as desired place
     * @param $locid - location id
     */
    public function View_Dplaces($locid)
    {
        $selectString = "select * from locations where Location_id = {$locid}";
        $comments=mysql_query($selectString);

        ?>
        <table data-role="table" data-mode="columntoggle" class="ui-responsive ui-shadow" id="myTable" >
            <?php
            while($row=mysql_fetch_array($comments))
            {

                ?>
                <tr>
                    <td><?php echo "<div align=\"center\"> <img src=\"{$row['Image']}\" width=\"75\" height=\"75\" border=\"3\"> </div>"; ?></td>
                    <td><?php echo $row['Location_Name']; ?></td>
                    <td><a href="javascript:SaveDplaces(<?php echo $row['Location_id'] ?>);" class="ui-btn ui-btn">Add</a></td>
                </tr>
            <?php


            }
            ?>
        </table>
    <?php
    }

    /*
     * To view all desired places
     * @param $uid - userid
     */
    public function View_AllDplaces($uid)
    {
        $selectString = "select * from locations l,dplaces d where l.Location_id=d.locid and d.uid=$uid";
        $comments=mysql_query($selectString);

        ?>
        <table data-role="table" data-mode="columntoggle" class="ui-responsive ui-shadow" id="myTable" >
            <?php
            while($row=mysql_fetch_array($comments))
            {

                ?>
                <tr>
                    <td><?php echo "<div align=\"center\"> <img src=\"{$row['Image']}\" width=\"75\" height=\"75\" border=\"3\"> </div>"; ?></td>
                    <td><?php echo $row['Location_Name']; ?></td>
                    <td><a href="javascript:RemoveDplace(<?php echo $row['Location_id'] ?>);" class="ui-btn ui-btn">Remove</a></td>
                </tr>
            <?php


            }
            ?>
        </table>
    <?php
    }


    /*
     * To get plan id
     * @param $uid - userid
     */
    public function Get_groupId($uid)
    {

        $selectString = "SELECT max( planid ) AS max FROM `usertravelplan` WHERE uid =$uid";

        $model = new Model();
        $max=$model->Get_onevalue($selectString,"max");

        $selectName = "SELECT planName FROM `usertravelplan` WHERE planid =$max";

        $pname=$model->Get_onevalue($selectName,"planName");

        ?><p><input type="hidden" id="pid" value="<?php echo $max ?>" /></p><br /><?php

        echo "Plan Name :".$pname;

    }

    /*
     * To get selected plan id
     * @param $pid - plan id
     */
    public function Get_Selectedpid($pid)
    {


        $selectName = "SELECT planName FROM `usertravelplan` WHERE planid =$pid";
        $model = new Model();
        $pname=$model->Get_onevalue($selectName,"planName");

        ?><p><input type="hidden" id="pid" value="<?php echo $pid ?>" /></p><br /><?php

        echo "Plan Name :".$pname;

    }


    /*
     * To get latitude longitude of the origin location
     * @param $pid - plan id
     * @param $uid - userid
     */
    public  function Get_latlnt($uid,$pid)
    {
        $selectString = "select * from locations where Location_id = (select origin from usertravelplan where planid=$pid)";

        $comments=mysql_query($selectString);

        ?>
        <table data-role="table" data-mode="columntoggle" class="ui-responsive ui-shadow" id="myTable" >
            <?php

            while ($row = mysql_fetch_array($comments)) {


                ?>


                <td><textarea style="display:none;" id="origin"><?php echo $row['latitude'] ?>,<?php echo $row['longitude'] ?></textarea></td>
            <?php

            }

            ?>
        </table>
    <?php


        $SelectString2 = "select * from dplaces dp,locations l where l.Location_id=dp.locid and dp.uid={$uid}";

        $Comments2 = mysql_query($SelectString2);

        ?>
        <table data-role="table" data-mode="columntoggle" class="ui-responsive ui-shadow" id="myTable" >



            <?php

            $i=0;

            while ($row = mysql_fetch_array($Comments2))
            {

                ?>


                <tr>
                    <td><textarea style="display:none;" id="<?php echo $i ?>"><?php echo $row['latitude'] ?>,<?php echo $row['longitude'] ?>,<?php echo $row['locid'] ?></textarea></td>
                </tr>
                <?php
                $i++;
            }


            ?>
            <textarea style="display:none;" id="count"><?php echo $i ?></textarea>
        </table>


        </body>

    <?php


    }


    /*
     * To get latitude longitude of of the desire place
     * @param $pid - plan id
     * @param $uid - userid
     */
    public  function Get_latlntMod($uid,$pid)
    {
        $selectmax="SELECT max(recid) as maxrecid FROM plan_locations WHERE planid=$pid";

        $model = new Model();
        $max=$model->Get_onevalue($selectmax,"maxrecid");

        $selectString = "select * from locations where Location_id =(select locid from plan_locations where recid=$max ) ";

        $comments=mysql_query($selectString);

        ?>
        <table data-role="table" data-mode="columntoggle" class="ui-responsive ui-shadow" id="myTable" >
            <?php

            while ($row = mysql_fetch_array($comments)) {


                ?>


                <td><textarea style="display:none;" id="origin"><?php echo $row['latitude'] ?>,<?php echo $row['longitude'] ?></textarea></td>
            <?php

            }

            ?>
        </table>
        <?php


        $SelectString2 = "select * from dplaces dp,locations l where l.Location_id=dp.locid and dp.uid={$uid}";

        $Comments2 = mysql_query($SelectString2);

        ?>
        <table data-role="table" data-mode="columntoggle" class="ui-responsive ui-shadow" id="myTable" >



            <?php

            $i=0;

            while ($row = mysql_fetch_array($Comments2))
            {

                ?>


                <tr>
                    <td><textarea style="display:none;" id="<?php echo $i ?>"><?php echo $row['latitude'] ?>,<?php echo $row['longitude'] ?>,<?php echo $row['locid'] ?></textarea></td>
                </tr>
                <?php
                $i++;
            }


            ?>
            <textarea style="display:none;" id="count"><?php echo $i ?></textarea>
        </table>


        </body>

    <?php


    }


    /*
     * To get all planned places
     * @param $pid - plan id
     */
    public function get_plan_places($pid)
    {

        $selectString = "select * from locations l,plan_locations pl where l.Location_id=pl.locid and pl.planid=$pid";
        $comments=mysql_query($selectString);

        ?>
        <table data-role="table" data-mode="columntoggle" class="ui-responsive ui-shadow" id="myTable" >
            <?php
            while($row=mysql_fetch_array($comments))
            {

                ?>
                <tr>
                    <td><?php echo "<div align=\"center\"> <img src=\"{$row['Image']}\" width=\"75\" height=\"75\" border=\"3\"> </div>"; ?></td>
                    <td><?php echo $row['Location_Name']; ?></td>
                    <td><a href="javascript:GetHotels(<?php echo $row['Location_id'] ?>);" class="ui-btn ui-btn">Get Hotels</a></td>
                </tr>
            <?php


            }
            ?>
        </table>
    <?php

    }


    /*
     * To view hotel details according to location area
     * @param $pid - plan id
     * @param $locid - location id
     */
    public function View_hotels($locid,$pid)
    {
        ?>
        <link rel="stylesheet" href="http://www.w3schools.com/lib/w3.css">
        <body>
        <?php
        $preString = "select Area from locations where Location_id=$locid";
        $preComments=mysql_query($preString);
        $prerow=mysql_fetch_array($preComments);
        $Area=$prerow['Area'];

        $SelectString="select * from hotels where area=\"$Area\"";

        $comments=mysql_query($SelectString);


        ?>

        <?php

        while ( $row = mysql_fetch_array($comments))
        {

            ?>

            <div class="w3-container w3-blue">
                <h2><?php echo $row['hotelName'] ?></h2>
            </div>

            <div class="w3-image">
                <?php echo "<img src=\"{$row['image']}\" width=\"100%\" height=\"40%\" >" ;?>
                <div class="w3-title w3-text-white"><?php echo "LKR ".$row['price']." per night" ?></div>
            </div>


            <div class="w3-container">
                <div class="w3-container w3-half">
                    <p><?php echo "Address ".$row['address'] ?></p>
                    <p><?php echo "Phone ".$row['phone'] ?></p>
                    <p><?php echo $row['star']." Star Hotel" ?></p>
                    <p><?php echo "LKR ".$row['price']." per night" ?></p>
                </div>
                <div class="w3-container w3-half">
                    <a href="javascript:SaveHotels(<?php echo $locid ?>,<?php echo $row['hotelid'] ?>);" class="ui-btn ui-btn">Save</a>
                </div>
            </div>


            <br>

        <?php


        }

        ?>
        <body>
    <?php

    }


    /*
     * To select a plan
     * @param $pid - plan id
     * @param $id - user id
     */
    public function SelectPlan($id,$pid)
    {

        $selectString="select * from usertravelplan where uid=$id";

        $comments=mysql_query($selectString);

        ?>
        <table data-role="table" data-mode="columntoggle" class="ui-responsive ui-shadow" id="myTable" >
            <?php
            while($row=mysql_fetch_array($comments))
            {
                ?>

                <tr>
                    <td><?php echo $row['planName'] ?></td>
                    <?php if ($pid != $row['planid']) { ?>
                        <td><a href="javascript:setplanlocal(<?php echo $row['planid'] ?>);" class="ui-btn ui-btn">Select</a></td>
                    <?php }
                    else{
                        ?>
                        <td><a href="javascript:setplanlocal(<?php echo $row['planid'] ?>);" class="ui-btn ui-btn">Current Selected</a></td>
                    <?php

                    }
                    ?>
                </tr>

            <?php

            }
            ?>
        </table>
    <?php

    }


    /*
     * To show final result
     * @param $pid - plan id
     * @param $id - user id
     */
    public function Show_results($id,$pid)
    {

        $totaldistance=0;
        $totalpricedays=0;
        $placecount=0;
        $budgetp=0;
        $travelmethod="";
        $origin="";

        $selectString="select * from usertravelplan where planid=$pid";

        $comments=mysql_query($selectString);

        while ($row=mysql_fetch_array($comments)){
            $origin=$row['origin'];
            ?>
            <div class="w3-container w3-teal">
                <h4><?php echo "Plan Name : ".$row['planName'] ?></h4>
            </div>

            <table>
                <tr>Travellers</tr>
                <tr>
                    <td>Adult :</td>
                    <td><?php echo $row['adults'] ?></td>
                </tr>
                <tr>
                    <td>Child :</td>
                    <td><?php echo $row['child'] ?></td>
                </tr>
                <tr>
                    <td>Budget Class :</td>
                    <?php
                    $selectBclass="select * from budgetclass where bid={$row['budgetclass']}";
                    $bclsscomments=mysql_query($selectBclass);
                    $rowbcls=mysql_fetch_array($bclsscomments);
                    $budgetcls=$rowbcls['bname'];
                    $avgbudget=($rowbcls['minprice']+$rowbcls['maxprice'])/2;
                    $budgetp=$avgbudget/10;
                    ?>
                    <td><?php echo $budgetcls ?></td>
                </tr>


            </table>



            <?php


            $selectTprice="select * from travelmethod where mid={$row['travelmethod']}";
            $commentT=mysql_query($selectTprice);
            $rowT=mysql_fetch_array($commentT);
            $travelmethod=$rowT['priceperkm'];

        }

        $selectorigin="select Location_Name from locations where Location_id=$origin";
        $commorigin=mysql_query($selectorigin);
        $rowOrigin=mysql_fetch_array($commorigin);

        $seletbplan="select * from plan_locations where planid=$pid ORDER BY recid";
        $commentsbplan=mysql_query($seletbplan);

        ?>
        <div class="w3-row-padding w3-margin-top">
            <div class="w3-card w3-red"><p>Travel Starting Point : <?php echo $rowOrigin['Location_Name'] ?></p></div>
        </div>
        <?php

        while ($rowbplan=mysql_fetch_array($commentsbplan)){

            $placecount++;

            $selectlocation="select * from locations where Location_id={$rowbplan['locid']}";
            $commentslocation=mysql_query($selectlocation);
            $rowlocation=mysql_fetch_array($commentslocation);
            $locationimage=$rowlocation['Image'];
            $locationname=$rowlocation['Location_Name'];


            if($rowbplan['hotelid']!=null) {
                $selecthotel = "select * from hotels where hotelid={$rowbplan['hotelid']}";
                $commentshotel = mysql_query($selecthotel);
                $rowhotel = mysql_fetch_array($commentshotel);
                $hotelname = $rowhotel['hotelName'];
                $hotelprice = $rowhotel['price'];
            }
            else{
                $hotelname = " ";
                $hotelprice = 0;
            }
            ?>

            <div class="w3-row-padding w3-margin-top">
                <div class="w3-card-12 w3-blue" style="width:100%">
                    <p><?php echo "Distance :".$rowbplan['distance']." km"  ?></p>
                    <?php $totaldistance=$totaldistance+$rowbplan['distance'] ?>
                    <p><?php echo "Duration :".$rowbplan['duration']  ?></p>
                </div>
            </div>

            <table>
                <tr>

                    <div class="w3-row-padding w3-margin-top">
                        <div class="w3-card-12" style="width:50%">
                            <?php echo "<img src=\"{$locationimage}\" width=\"100%\">" ;?>
                            <div class="w3-container">
                                <p><?php echo $locationname  ?></p>
                                <p><?php echo "Hotel :".$hotelname  ?></p>
                                <p><?php echo "Days :".$rowbplan['days']  ?></p>
                                <?php
                                $dd=$rowbplan['days'];
                                $totalpricedays=$totalpricedays+($dd*$hotelprice); ?>
                                <p><?php echo "Rooms :".$rowbplan['rooms']  ?></p>
                            </div>
                        </div>
                    </div>

                </tr>

            </table>



        <?php


        }

        $pricedist=$totaldistance*$travelmethod;

        $otherfacilities=($totaldistance/($placecount+1))*$budgetp;

        $approxprice=$pricedist+$otherfacilities+$totalpricedays;




        ?>
        <div class="w3-row-padding w3-margin-top">
            <div class="w3-card w3-red"><p><?php echo "Approximate price LKR ".number_format($approxprice,2);  ?></p></div>
        </div>
    <?php

    }


} 