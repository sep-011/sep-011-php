<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title></title>
    <style>
        #fontS
        {
            font-family: helvetica, impact, sans-serif;
            font-size: 14px;
            color="black";
        }
        #fonthead
        {
            font-family: helvetica, impact, sans-serif;
            font-size: 16px;
            color="blue";
        }
        #visited
        {
            text-decoration: none;
        }
	    #pdate
	    {
		    font-family: helvetica, impact, sans-serif;
            font-size: 12px;
           	color="ash";
	    }

    </style>
</head>
<body>

<?php
//get selected type from the html file
$type =$_POST['type'];

if($type=='Local')
{
    //url to the local rss feed reader
    $result = file_get_contents('http://www.dailymirror.lk/RSS_Feeds/travel-main');
    //get rss feed xml format
    $xml = simplexml_load_string($result);
?>  <table data-role="table" data-mode="columntoggle" class="ui-responsive ui-shadow" id="myTable" width="100%" cellspacing="0" cellpadding="0" >
    <?php
    //go to channel tag -> item teg
    foreach($xml->channel->item as $item){//store items in an array

        //get title tag value
        $title = $item->title;
        //get link tag value
        $link = $item->link;
        //get published tag value
        $pub = $item->pubDate;
        //get description tag value
        $descrip = $item->description;

        //get published date and time
        $pub = substr($pub,0,25);
        echo "<tr><td>";
        echo "<hr><h2 id='fonthead'><a id='visited' href='$link'>$title</a></h2>";
        echo "<p>";
        echo "<p id='pdate'>$pub</p>";
        echo "<p>";
        echo "<p id='fontS'>$descrip</p>";
        echo "<p>";
        echo "</td></tr>";

    }
    ?>
    </table>
    <?php
    // go up image
    echo "<div data-role='main' class='ui-content' >";
    echo "<input type='image' src='http://sep.esy.es/sep/iteration4/minoli/images/up.png' width='40' height='40' onclick='goUp()' />";
    echo "</div>";



}
else
{
    //url to the world rss feed reader
    $result = file_get_contents('http://www.travelmole.com/feeds/18_2.xml');
    //get rss feed xml format
    $xml = simplexml_load_string($result);
?>  <table data-role="table" data-mode="columntoggle" class="ui-responsive ui-shadow" id="myTable" width="100%" cellspacing="0" cellpadding="0" >
    <?php
    //go to channel tag -> item teg
    foreach($xml->channel->item as $item){//store items in an array

        //get title tag value
        $title = $item->title;
        //get link tag value
        $link = $item->link;
        //get published tag value
        $pub = $item->pubDate;
        //get description tag value
        $descrip = $item->description;

        //get published date and time
        $pub = substr($pub,0,25);
        echo "<tr><td>";
        echo "<hr><h2 id='fonthead'><a id='visited' href='$link'>$title</a></h2>";
        echo "<p>";
        echo "<p id='pdate'>$pub</p>";
        echo "<p>";
        echo "<p id='fontS'>$descrip</p>";
        echo "<p>";
        echo "</td></tr>";



    }
    ?>
</table>
    <?php
    // go up image
    echo "<div data-role='main' class='ui-content' >";
    echo "<input type='image' src='http://sep.esy.es/sep/iteration4/minoli/images/up.png' width='40' height='40' onclick='goUp()' />";
    echo "</div>";
}
?>

</body>
</html>