<?php
/**
 *
 * Use to view data through application
 */

class View {


    public function travelNews($type)
    {
        ?>
        <html>
        <head>
            <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
            <title></title>
            <style>
                #fontS
                {
                    font-family: helvetica, impact, sans-serif;
                    font-size: 14px;
                    color="black";
                }
                #fonthead
                {
                    font-family: helvetica, impact, sans-serif;
                    font-size: 16px;
                    color="blue";
                }
                #visited
                {
                    text-decoration: none;
                }
                #pdate
                {
                    font-family: helvetica, impact, sans-serif;
                    font-size: 12px;
                    color="ash";
                }

            </style>
        </head>
        <body>
        <?php
        if($type=='Local')
        {
            //url to the local rss feed reader
            $result = file_get_contents('http://www.dailymirror.lk/RSS_Feeds/travel-main');
            //get rss feed xml format
            $xml = simplexml_load_string($result);
            ?>  <table data-role="table" data-mode="columntoggle" class="ui-responsive ui-shadow" id="myTable" width="100%" cellspacing="0" cellpadding="0" >
            <?php
            //go to channel tag -> item teg
            foreach($xml->channel->item as $item){//store items in an array

                //get title tag value
                $title = $item->title;
                //get link tag value
                $link = $item->link;
                //get published tag value
                $pub = $item->pubDate;
                //get description tag value
                $descrip = $item->description;

                //get published date and time
                $pub = substr($pub,0,25);
                echo "<tr><td>";
                echo "<hr><h2 id='fonthead'><a id='visited' href='$link'>$title</a></h2>";
                echo "<p>";
                echo "<p id='pdate'>$pub</p>";
                echo "<p>";
                echo "<p id='fontS'>$descrip</p>";
                echo "<p>";
                echo "</td></tr>";

            }
            ?>
        </table>
            <?php
            // go up image
            echo "<div data-role='main' class='ui-content' >";
            echo "<input type='image' src='http://sep.esy.es/sep/iteration4/minoli/images/up.png' width='40' height='40' onclick='goUp()' />";
            echo "</div>";



        }
        else
        {
            //url to the world rss feed reader
            $result = file_get_contents('http://www.travelmole.com/feeds/18_2.xml');
            //get rss feed xml format
            $xml = simplexml_load_string($result);
            ?>  <table data-role="table" data-mode="columntoggle" class="ui-responsive ui-shadow" id="myTable" width="100%" cellspacing="0" cellpadding="0" >
            <?php
            //go to channel tag -> item teg
            foreach($xml->channel->item as $item){//store items in an array

                //get title tag value
                $title = $item->title;
                //get link tag value
                $link = $item->link;
                //get published tag value
                $pub = $item->pubDate;
                //get description tag value
                $descrip = $item->description;

                //get published date and time
                $pub = substr($pub,0,25);
                echo "<tr><td>";
                echo "<hr><h2 id='fonthead'><a id='visited' href='$link'>$title</a></h2>";
                echo "<p>";
                echo "<p id='pdate'>$pub</p>";
                echo "<p>";
                echo "<p id='fontS'>$descrip</p>";
                echo "<p>";
                echo "</td></tr>";



            }
            ?>
        </table>
            <?php
            // go up image
            echo "<div data-role='main' class='ui-content' >";
            echo "<input type='image' src='http://sep.esy.es/sep/iteration4/minoli/images/up.png' width='40' height='40' onclick='goUp()' />";
            echo "</div>";
        }
    ?>
        </body>
        </html>
    <?php

    }



    public function VehicleRent($vehicleType)
    { ?>
        <html>
        <body>
        <?php

        //get searched drop down value
        if($vehicleType == 1)// if selected type is vehicle type
        {

            $type_string = 'v_type';
            $vehicle = "select DISTINCT $type_string from vehical";

            $result = mysql_query($vehicle) or die(mysql_error());
            ?>
            <table data-role="table" data-mode="columntoggle" class="ui-responsive ui-shadow" id="myTable" width="100%">
                <tr>
                    <td>Vehicle Type:&nbsp;&nbsp;&nbsp;</td>
                    <td>
                        <select id="result_set" onchange="resultLoad('<?php echo $type_string ?>');">
                            <?php
                            while($row = mysql_fetch_row($result)) {

                                ?> <!-- pass vehicle type -->
                                <option value="<?php echo $row[0] ; ?>"> <?php echo $row[0] ; ?> </option>
                            <?php


                            }
                            ?>
                        </select>
                    </td>
                </tr>
            </table>
        <?php
        }
        else if($vehicleType == 2)//if selected type is no. of passengers
        {
            $type_string = 'nop_range';
            $nop = "select DISTINCT $type_string from vehical";

            // echo $vehicle;

            $result = mysql_query($nop) or die(mysql_error());
            ?>
            <table data-role="table" data-mode="columntoggle" class="ui-responsive ui-shadow" id="myTable" width="100%">
                <tr>
                    <td>No. of Passengers:&nbsp;&nbsp;&nbsp;</td>
                    <td>
                        <select id="result_set" onchange="resultLoad('<?php echo $type_string ?>');">
                            <?php
                            while($row = mysql_fetch_row($result)) {

                            ?><!-- pass no. of passengers -->
                            <option value="<?php echo $row[0] ; ?>"> <?php echo $row[0] ; ?></option>
                            <?php


                            }
                            ?>
                        </select>
                    </td>
                </tr>
            </table>
        <?php
        }
        else if($vehicleType == 3)//if selected type is price
        {
            $type_string = 'prce_range';
            $price = "select DISTINCT $type_string from vehical";

            // echo $vehicle;

            $result = mysql_query($price) or die(mysql_error());
            ?>
            <table data-role="table" data-mode="columntoggle" class="ui-responsive ui-shadow" id="myTable" width="100%">
                <tr>
                    <td>Rate per Month:&nbsp;&nbsp;&nbsp;</td>
                    <td>
                        <select id="result_set" onchange="resultLoad('<?php echo $type_string ?>');">
                            <?php
                            while($row = mysql_fetch_row($result)) {

                            ?><!-- pass price values-->
                            <option value="<?php echo $row[0] ; ?>"> <?php echo $row[0] ; ?> </option>
                            <?php


                            }
                            ?>
                        </select>
                    </td>
                </tr>
            </table>
        <?php
        }
        else//if selected type is name
        {
            $type_string = 'name';
            $name = "select DISTINCT $type_string from vehical";

            // echo $vehicle;

            $result = mysql_query($name) or die(mysql_error());
            ?>
            <table data-role="table" data-mode="columntoggle" class="ui-responsive ui-shadow" id="myTable" width="100%">
                <tr>
                    <td>Vehicle Type:&nbsp;&nbsp;&nbsp;</td>
                    <td>
                        <select id="result_set" onchange="resultLoad('<?php echo $type_string ?>');">
                            <?php
                            while($row = mysql_fetch_row($result)) {

                            ?><!-- pass vehicle name -->
                            <option value="<?php echo $row[0] ; ?>"> <?php echo $row[0] ; ?> </option>
                            <?php


                            }
                            ?>
                        </select>
                    </td>
                </tr>
            </table>
        <?php
        }
        ?>
        </body>
        </html>
        <?php
    }

    public function SelectedVehicle($ResultType,$category)
    {

        if($ResultType == 'v_type')// if selected type is vehicle type
        {

            $vehicle = "select * from vehical where $ResultType LIKE '$category%'";

            $result = mysql_query($vehicle) or die(mysql_error());
            ?>
            <table data-role="table" data-mode="columntoggle" class="ui-responsive ui-shadow" id="myTable" width="100%">

                <?php
                while($row = mysql_fetch_row($result)) {

                    ?> <tr>
                        <td><?php echo "<div align=\"center\"> <img src=\"{$row[5]}\" width=\"90\" height=\"90\" border=\"3\"> </div>" ;?></td>
                        <td><b nowrap><?php echo $row[2] ;?></b><br>
                            <!-- pass selected index of second drop down -->
                            <a href="javascript:viewMore(<?php echo $row[0]?>);" class="ui-btn ui-btn">View more</a></td>
                    </tr>
                <?php


                }
                ?>



            </table>
        <?php
        }
        else if($ResultType == 'nop_range')//if selected type is no. of passengers
        {
            $vehicle = "select * from vehical where $ResultType LIKE '$category%'";

            $result = mysql_query($vehicle) or die(mysql_error());
            ?>
            <table data-role="table" data-mode="columntoggle" class="ui-responsive ui-shadow" id="myTable" width="100%">


                <?php
                while($row = mysql_fetch_row($result)) {

                    ?> <tr>
                        <td><?php echo "<div align=\"center\"> <img src=\"{$row[5]}\" width=\"90\" height=\"90\" border=\"3\"> </div>" ;?></td>
                        <td><b nowrap><?php echo $row[2] ;?></b><br>
                            <!-- pass selected index of second drop down -->
                            <a href="javascript:viewMore(<?php echo $row[0]?>);" class="ui-btn ui-btn">View more</a></td>
                    </tr>
                <?php


                }
                ?>



            </table>
        <?php
        }
        else if($ResultType == 'prce_range')//if selected type is price
        {
            //if string is long break it to search
            $category= substr($category,0,12);
            $vehicle = "select * from vehical where $ResultType LIKE '$category%'";

            $result = mysql_query($vehicle) or die(mysql_error());
            ?>
            <table data-role="table" data-mode="columntoggle" class="ui-responsive ui-shadow" id="myTable" width="100%">

                <?php
                while($row = mysql_fetch_row($result)) {

                    ?> <tr>
                        <td><?php echo "<div align=\"center\"> <img src=\"{$row[5]}\" width=\"90\" height=\"90\" border=\"3\"> </div>" ;?></td>
                        <td><b nowrap><?php echo $row[2] ;?></b><br>
                            <!-- pass selected index of second drop down -->
                            <a href="javascript:viewMore(<?php echo $row[0]?>);" class="ui-btn ui-btn">View more</a></td>
                    </tr>
                <?php


                }
                ?>



            </table>
        <?php
        }
        else//if selected type is name
        {
            $ResultType = 'name';
            //if string is long break it to search
            $category= substr($category,0,12);
            $vehicle = "select * from vehical where $ResultType LIKE '$category%'";

            $result = mysql_query($vehicle) or die(mysql_error());
            ?>
            <table data-role="table" data-mode="columntoggle" class="ui-responsive ui-shadow" id="myTable" width="100%">

                <?php
                while($row = mysql_fetch_row($result)) {

                    ?> <tr>
                        <td><?php echo "<div align=\"center\"> <img src=\"{$row[5]}\" width=\"90\" height=\"90\" border=\"3\"> </div>" ;?></td>
                        <td><b nowrap><?php echo $row[2] ;?></b><br>
                            <!-- pass selected index of second drop down -->
                            <a href="javascript:viewMore(<?php echo $row[0]?>);" class="ui-btn ui-btn">View more</a></td>
                    </tr>
                <?php

                }
                ?>

            </table>
        <?php
        }

    }

    public function ViewMoreVehicle($id)
    {
        //get values of selected vehicle
        $specific_vehicle = "select * from vehical where id=$id";

        $result = mysql_query($specific_vehicle) or die(mysql_error());

        ?>
        <table data-role="table" data-mode="columntoggle" class="ui-responsive ui-shadow" id="myTable" width="100%">


    <?php
    while($row = mysql_fetch_row($result)) {

        ?>
        <tr>
            <?php echo "<div align=\"center\"> <img src=\"{$row[5]}\" width=\"150\" height=\"150\" border=\"3\"> </div>" ;?>
        </tr>
        <tr>
            <td>Name:</td><!-- get value from column 2 -name-->
            <td><b nowrap><?php echo $row[2] ;?></b><br></td>
        </tr>
        <tr>
            <td>A/C</td><!-- get value from column 3 -a/c -->
            <td><b nowrap><?php echo $row[3] ;?></b><br></td>
        </tr>
        <tr>
            <td>No. of Passengers</td><!-- get value from column 4 -passenger -->
            <td><b nowrap><?php echo $row[4] ;?></b><br></td>
        </tr>
        <tr>
            <td>Rate Per Month</td><!-- get value from column 5 -rate month-->
            <td><b nowrap>LKR.<?php echo $row[6] ;?>/=</b><br></td>
        </tr>
        <tr>
            <td>Rate Per Week</td><!-- get value from column 6 -rate week-->
            <td><b nowrap>LKR.<?php echo $row[7] ;?>/=</b><br></td>
        </tr>
        <tr>
            <td>Excess Mileage</td><!-- get value from column 7 -excess-->
            <td><b nowrap>LKR.<?php echo $row[8] ;?>/=</b><br></td>
        </tr>
        <tr>
            <td>With Driver</td><!-- get value from column 8 -driver-->
            <td><b nowrap>LKR.<?php echo $row[9] ;?>/=</b><br></td>
        </tr>
        <tr>
            <!-- pass selected index of second drop down -->
            <td>
            <a href="javascript:contactUs(<?php echo $row[0]?>);" class="ui-btn ui-btn">Contact Us</a>
            </td>
        </tr>
    <?php


    }
    ?>
    </table>
    <?php

    }
    // Contact information
    public function ContactUS()
    {?>
        <table data-role="table" data-mode="columntoggle" class="ui-responsive ui-shadow" id="myTable" width="100%">
            <caption>Contact Information</caption>
            <tr>
                <td>Name:</td>
                <td><b nowrap>Saman</b><br></td>
            </tr>
            <tr>
                <td>Telephone:</td>
                <td><b nowrap>0112154879</b><br></td>
            </tr>
        </table>
        <?php
    }
    public  function trainSchedule($start,$end)
    {
        //get train schedule
        $query = "select * from train_header where dep='$start' AND arrive='$end'";

        $result = mysql_query($query);

        //if returned no results
        if (mysql_num_rows($result)==0)
        {
            ?> <label>No Results Found</label>
        <?php
        }
        else // if returned a result set
        {

            while($row = mysql_fetch_row($result))
            {

                $trainID =$row[0];

            }

            $details = "Select * from train_details where tid=$trainID";

            $detail_result = mysql_query($details) or die(mysql_error());

            ?>

            <table data-role="table" data-mode="columntoggle" class="ui-responsive ui-shadow" id="myTable" width="100%" cellspacing="0" cellpadding="0" >
            <tr >
                <th><b>Departure&nbsp;&nbsp;</b></th>
                <th><b>Arrival&nbsp;&nbsp;</b></th>
                <th><b>Frequency&nbsp;&nbsp;</b></th>
                <th><b>View More</b></th>
            </tr>
            <?php

            while($row = mysql_fetch_row($detail_result)) {

                ?>
                <tr >
                    <!-- get values from column  'arr_time'-->
                    <td><?php echo $row[2] ;?></td>
                    <!-- get values from column 'dest_time'-->
                    <td ><?php echo $row[3] ;?></td>
                    <!-- get values from column 'freq'-->
                    <td><?php echo $row[4] ;?></td>
                    <td>
                        <!-- go to the function view more details ,pass train_id-->
                        <a href="javascript:viewMoreTrain(<?php echo $row[0]?>);" class="ui-btn ui-btn" width="30px"; height="30px";>
                        <img src="http://sep.esy.es/sep/iteration4/minoli/images/arrow.png"></a>
                    </td>
                </tr>
            <?php
            }
        }
    }


    public function ViewMoretrain($viewDetails)
    {

        $viewID = $viewDetails;

        //get selected train details
        $details = "Select * from train_details where detail_id=$viewID";

        $detail_result = mysql_query($details) or die(mysql_error());


        ?>
        <table data-role="table" data-mode="columntoggle" class="ui-responsive ui-shadow" id="myTable" >

        <?php

        while($row = mysql_fetch_row($detail_result)) {

            ?>
            <!-- get values from column 'type'-->
            <tr><td><b>Train Type</b></td><td><?php echo $row[5] ;?><br/></td></tr>
            <!-- get values from column  'arr_time'-->
            <tr><td><b>Departure</b></td><td><?php echo $row[2] ;?><br/></td></tr>
            <!-- get values from column 'dest_time'-->
            <tr><td><b>Arrival</b></td><td><?php echo $row[3] ;?><br/></td></tr>
            <!-- get values from column 'end_station'-->
            <tr><td><b>Final Station</b></td><td><?php echo $row[6] ;?><br/></td></tr>
            <!-- get values from column 'end_time' -->
            <tr><td><b>Reach to Final Station</b></td><td><?php echo $row[7] ;?><br/></td></tr>
            <!-- get values from column 'freq'-->
            <tr><td><b>Train Frequency</b></td><td><?php echo $row[4] ;?><br/></td></tr>
            <!-- go to the function view stations on map, pass selected row id-->
            <tr><td><a href="javascript:initGeolocation(<?php echo $row[1]?>);" class="ui-btn ui-btn">View Map</a></td></tr>

        <?php
        }

    }

    public function viewMap($viewMap)
    {
        ?>
        <html>
        <body>
        <?php

        $train_categoryID = $viewMap;

        $first = "select count(*) from stations where fid=$train_categoryID";

        $first_result = mysql_query($first);
        $count =0;
        while($row= mysql_fetch_array($first_result))
        {
            $count = $row[0];
        }

        //create global variables for places

        //first places list
        $f_list ="";
        //second places list
        $s_list ="";
        //third places list
        $t_list="";
        // identify the category
        $category =0;
        if($count<23)
        {
            $category =1;
            $f_list = "SELECT * FROM stations where fid=$train_categoryID ORDER BY longitude limit $count";
        }
        else if($count<43)
        {
            $category =2;
            $f_list = "SELECT * FROM stations where fid=$train_categoryID ORDER BY longitude limit 22";
            $count = $count -22;
            $s_list ="SELECT * FROM stations where fid=$train_categoryID ORDER BY longitude limit 21,$count";
        }
        else if($count<63)
        {
            $category =3;
            $f_list = "SELECT * FROM stations where fid=$train_categoryID ORDER BY longitude limit 22";
            $count = $count -22;
            $s_list ="SELECT * FROM stations where fid=$train_categoryID ORDER BY longitude limit 21,22";
            $count = $count -20;
            $t_list = "SELECT * FROM stations where fid=$train_categoryID  ORDER BY longitude limit 42,$count";
        }

        //create first string
        $all_f = "";
        //create second string
        $all_s = "";
        //create third string
        $all_t = "";

        if($category==1)
        {   //first
            $position = mysql_query($f_list);

            while($row= mysql_fetch_array($position)) {
                //latitude =3,longitude=4
                $comma =",";
                $slash ="/";
                $all_f = $all_f.$row[3].$comma.$row[4].$slash;
            }
        }
        else if($category==2){
            //first
            $position = mysql_query($f_list);
            while($row= mysql_fetch_array($position)) {
                //latitude =3,longitude=4
                $comma =",";
                $slash ="/";
                $all_f = $all_f.$row[3].$comma.$row[4].$slash;
            }

            //second
            $position2 = mysql_query($s_list);
            while($row= mysql_fetch_array($position2)) {
                //latitude =3,longitude=4
                $comma =",";
                $slash ="/";
                $all_s = $all_s.$row[3].$comma.$row[4].$slash;
            }
        }
        else if($category==3)
        {
            //first
            $position = mysql_query($f_list);
            while($row= mysql_fetch_array($position)) {
                //latitude =3,longitude=4
                $comma =",";
                $slash ="/";
                $all_f = $all_f.$row[3].$comma.$row[4].$slash;
            }

            //second
            $position2 = mysql_query($s_list);
            while($row= mysql_fetch_array($position2)) {
                //latitude =3,longitude=4
                $comma =",";
                $slash ="/";
                $all_s = $all_s.$row[3].$comma.$row[4].$slash;
            }
            //three
            $position3 = mysql_query($t_list);
            while($row= mysql_fetch_array($position3)) {
                //latitude =3,longitude=4
                $comma =",";
                $slash ="/";
                $all_t = $all_t.$row[3].$comma.$row[4].$slash;

            }
        }

        //get stations count
        $loc = "SELECT * FROM stations where fid=$train_categoryID  ORDER BY longitude ";
        $detail = mysql_query($loc);
        $count_loc=0;

        while($row= mysql_fetch_array($detail))
        {
            $loc_name = "id".$count_loc;
            //pass location name
            ?><input type="hidden" id="<?php echo $loc_name ?>" value="<?php echo $row[2] ?>">
            <?php
            $count_loc++;
        }

        //locations which near to stations
        $nears = "select * from train_header where trainid=$train_categoryID";
        $ne_query = mysql_query($nears);
        $dep ="";
        $arrive ="";
        while($row= mysql_fetch_array($ne_query))
        {   //get departure station
            $dep = $row[1];
            //get arrival station
            $arrive = $row[2];
        }

        //get departure station coordinates
        $dep_s = "select * from stations where fid=$train_categoryID and city='$dep'";
        $dep_query =  mysql_query($dep_s);

        //get departure station latitude
        $dep_lat = "";
        //get departure station longitude
        $dep_long = "";

        while($row= mysql_fetch_array($dep_query))
        {
            $dep_lat = $row[3];
            $dep_long = $row[4];
        }

        //get end station coordinates
        $arr_s = "select * from stations where fid=$train_categoryID and city='$arrive'";
        $arr_query =  mysql_query($arr_s);

        //get arrival station latitude
        $arr_lat = "";
        //get arrival station longitude
        $arr_long = "";

        while($row= mysql_fetch_array($arr_query))
        {
            $arr_lat = $row[3];
            $arr_long = $row[4];
        }

        //in between places
        $betwn_pl ="select * from locations where latitude between $dep_lat and $arr_lat or longitude between $dep_long and $arr_long";
        $betwn_query =  mysql_query($betwn_pl);

        $loc_coordinates="";
        $loc_id=0;
        while($row= mysql_fetch_array($betwn_query))
        {
            //latitude=6,longitud=7
            $comma =",";
            $slash ="/";
            $loc_coordinates = $loc_coordinates.$row[6].$comma.$row[7].$slash;
            $loc_namel = "idloc".$loc_id;
            ?><input type="hidden" id="<?php echo $loc_namel ?>" value="<?php echo $row[1] ?>">
            <?php
            $loc_id++;
        }

        ?>
        <input type="hidden" id="len" value=<?php echo $all_f ?>>
        <input type="hidden" id="len2" value=<?php echo $all_s ?>>
        <input type="hidden" id="len3" value=<?php echo $all_t ?>>
        <input type="hidden" id="locLen" value=<?php echo $count_loc ?>>
        <input type="hidden" id="cat" value=<?php echo $category ?>>
        <input type="hidden" id="locP" value=<?php echo $loc_coordinates ?>>
        <input type="hidden" id="locNear" value=<?php echo $loc_id ?>>
        </body>
        </html>
        <?php
    }



} 