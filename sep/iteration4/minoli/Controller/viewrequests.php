<html>
<body>

<?php
/**
 *
 * Use to control the view request from the application
 */

//include_once($_SERVER["DOCUMENT_ROOT"].'/sep/iteration4/minoli/Model/Model.php');
include_once($_SERVER["DOCUMENT_ROOT"].'/sep/iteration4/minoli/connection.php');
include_once($_SERVER["DOCUMENT_ROOT"].'/sep/iteration4/minoli/View/View.php');


$connection=new connection();
//$model = new Model();
$conn=$connection->connect();
$view= new View();

if(!$conn)
{
    echo "Server connection terminated. please try again in few seconds";

}
else{

    // travel news
    if(isset($_POST['type']))
    {
        $type =$_POST["type"];

        $view->travelNews($type);
    }
    // vehicle rent
    if(isset($_POST['vhl']))
    {
        $vehicle=$_POST["vhl"];

        $view->VehicleRent($vehicle);
    }
    // vehicle rent - searched result
    if(isset($_POST['ResultType']) && isset($_POST['category']))
    {
        $ResultType=$_POST["ResultType"];
        $category=$_POST["category"];

        $view->SelectedVehicle($ResultType,$category);
    }
    // vehicle rent - view more
    if(isset($_POST["id"]))
    {
        $id=$_POST["id"];
        $view->ViewMoreVehicle($id);


    }
    // vehicle rent -contact infor
    if(isset($_POST['contact']))
    {
        $view->ContactUS();
    }
    // train schedule
    if(isset($_POST['start']) && isset($_POST['end']))
    {
        $start=$_POST["start"];
        $end=$_POST["end"];

        $view->trainSchedule($start,$end);
    }
    // train schedule - view more
    if(isset($_POST['view']))
    {
        $viewDetails=$_POST["view"];

        $view->ViewMoretrain($viewDetails);
    }
    // train schedule - view map
    if(isset($_POST['viewMap']))
    {
        $viewMap=$_POST["viewMap"];

        $view->viewMap($viewMap);
    }
}




?>

</body>
</html>