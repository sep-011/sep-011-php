<html>
<head>

    <style>
        .btnin {
            background: #3498db;
            background-image: -webkit-linear-gradient(top, #3498db, #2980b9);
            background-image: -moz-linear-gradient(top, #3498db, #2980b9);
            background-image: -ms-linear-gradient(top, #3498db, #2980b9);
            background-image: -o-linear-gradient(top, #3498db, #2980b9);
            background-image: linear-gradient(to bottom, #3498db, #2980b9);
            -webkit-border-radius: 7;
            -moz-border-radius: 7;
            border-radius: 7px;
            font-family: monospace;
            color: #ffffff;
            font-size: 8px;
            padding:3px 20px 3px 20px;
            border: solid #1f628d 1px;
            text-decoration: none;
        }

        .btnin:hover {
            background: #3cb0fd;
            background-image: -webkit-linear-gradient(top, #3cb0fd, #3498db);
            background-image: -moz-linear-gradient(top, #3cb0fd, #3498db);
            background-image: -ms-linear-gradient(top, #3cb0fd, #3498db);
            background-image: -o-linear-gradient(top, #3cb0fd, #3498db);
            background-image: linear-gradient(to bottom, #3cb0fd, #3498db);
            text-decoration: none;
        }
        .btnr {
            background: #bd3513;
            background-image: -webkit-linear-gradient(top, #bd3513, #d1341f);
            background-image: -moz-linear-gradient(top, #bd3513, #d1341f);
            background-image: -ms-linear-gradient(top, #bd3513, #d1341f);
            background-image: -o-linear-gradient(top, #bd3513, #d1341f);
            background-image: linear-gradient(to bottom, #bd3513, #d1341f);
            -webkit-border-radius: 20;
            -moz-border-radius: 20;
            border-radius: 20px;
            font-family: monospace;
            color: #ffffff;
            font-size: 8px;
            padding: 3px 15px 3px 15px;
            border: solid #1f628d 1px;
            text-decoration: none;
        }

        .btnr:hover {
            background: #c72b16;
            background-image: -webkit-linear-gradient(top, #c72b16, #e66747);
            background-image: -moz-linear-gradient(top, #c72b16, #e66747);
            background-image: -ms-linear-gradient(top, #c72b16, #e66747);
            background-image: -o-linear-gradient(top, #c72b16, #e66747);
            background-image: linear-gradient(to bottom, #c72b16, #e66747);
            text-decoration: none;
        }
        td{
            font-family:monospace;
            font-size:medium;

        }
        .msglguser{
            color:black;
            background-color: #3dd3ff;
            padding: 2px;
            border-radius: 25px;
            display:inline-block;


        }
        .searcheresult table{
            color:inherit;
            color: black;
            width: 1500px;
            border: 0px solid black;

            mso-cellspacing: 0px;
        }
        .searcheresult tr:hover {background-color:#cccccc;}
        .searcheresult td{
            width: 300px;
            border:#cccccc;
        }
        .frndlist table{
            width: 2000px;
        }
        .frndlist tr:hover{
            background-color: #eeeeee;
        }

    </style>
</head>
<body>
<?php

require_once("Chat.php");
header('Access-Control-Allow-Origin: *');
$chatobject=new Chat();
//to show the searched results for persons
if(isset($_GET['name']) && isset($_GET['uid']))
{

    $chatobject->showtheresults($_GET['uid'],trim($_GET['name']));

}


//to add the friends
if(isset($_GET['uid'] )&& isset($_GET['freinduid']))
{
    $chatobject->addfriends($_GET['uid'],$_GET['freinduid']);
}


//To remove the friends
if(isset($_GET['removefreinduid']) &&isset($_GET['uid']))
{
    $chatobject->removefriends($_GET['uid'],$_GET['removefreinduid']);
}


//To show the friednlist on the silde panel
if(isset($_GET['getfreindlist'])&&isset($_GET['uid']))
{
    //To show the all friends of a member
    $chatobject->showthefriendsonsidepanel($_GET['uid']);
}

//To show the chat messages
if(isset($_GET['toid'])&&isset($_GET['uid']))
{
    $chatobject->showthechatmessages($_GET['uid'],$_GET['toid']);
}

//To save the chat message
if(isset($_GET['toid'])&&isset($_GET['uid']) &&isset($_GET['message']))
{
    $msg=$_GET['message'];
    //to replace the message which having '
    $msg=str_replace("'","*!*",$msg);
    $chatobject->savechatmessages($_GET['uid'],$_GET['toid'],$msg);

}

//to update the online status time
if(isset($_GET['uid'] )&& isset($_GET['changestatus']))
{
    $chatobject->updateonlinestatus($_GET['uid']);
}


//get friend name
if(isset($_GET['uid']) && isset($_GET['frndid'])&& isset($_GET['frndname']))
{
    $chatobject->getfriendname($_GET['frndid']);
}

//This code segment is used to delete the chat messages of a user
if(isset($_GET['uid']) && isset($_GET['frndid'])&& isset($_GET['delete']))
{
    $chatobject->removechatmessages($_GET['uid'],$_GET['frndid']);

}
//This code segment is used to update online status of a user to OFF and ON
if(isset($_GET['uid']) && isset($_GET['onlinestaus'])&& isset($_GET['changeonlinemode']))
{
    $chatobject->turnonlineoffon($_GET['uid'],$_GET['onlinestaus']);

}

?>
</body>
</html>