<html>
<head>

    <style>
        .btnin {
            background: #3498db;
            background-image: -webkit-linear-gradient(top, #3498db, #2980b9);
            background-image: -moz-linear-gradient(top, #3498db, #2980b9);
            background-image: -ms-linear-gradient(top, #3498db, #2980b9);
            background-image: -o-linear-gradient(top, #3498db, #2980b9);
            background-image: linear-gradient(to bottom, #3498db, #2980b9);
            -webkit-border-radius: 7;
            -moz-border-radius: 7;
            border-radius: 7px;
            font-family: monospace;
            color: #ffffff;
            font-size: 8px;
            padding:3px 20px 3px 20px;
            border: solid #1f628d 1px;
            text-decoration: none;
        }

        .btnin:hover {
            background: #3cb0fd;
            background-image: -webkit-linear-gradient(top, #3cb0fd, #3498db);
            background-image: -moz-linear-gradient(top, #3cb0fd, #3498db);
            background-image: -ms-linear-gradient(top, #3cb0fd, #3498db);
            background-image: -o-linear-gradient(top, #3cb0fd, #3498db);
            background-image: linear-gradient(to bottom, #3cb0fd, #3498db);
            text-decoration: none;
        }
        .btnr {
            background: #bd3513;
            background-image: -webkit-linear-gradient(top, #bd3513, #d1341f);
            background-image: -moz-linear-gradient(top, #bd3513, #d1341f);
            background-image: -ms-linear-gradient(top, #bd3513, #d1341f);
            background-image: -o-linear-gradient(top, #bd3513, #d1341f);
            background-image: linear-gradient(to bottom, #bd3513, #d1341f);
            -webkit-border-radius: 20;
            -moz-border-radius: 20;
            border-radius: 20px;
            font-family: monospace;
            color: #ffffff;
            font-size: 8px;
            padding: 3px 15px 3px 15px;
            border: solid #1f628d 1px;
            text-decoration: none;
        }

        .btnr:hover {
            background: #c72b16;
            background-image: -webkit-linear-gradient(top, #c72b16, #e66747);
            background-image: -moz-linear-gradient(top, #c72b16, #e66747);
            background-image: -ms-linear-gradient(top, #c72b16, #e66747);
            background-image: -o-linear-gradient(top, #c72b16, #e66747);
            background-image: linear-gradient(to bottom, #c72b16, #e66747);
            text-decoration: none;
        }
        td{
            font-family:monospace;
            font-size:medium;

        }
        .msglguser{
            color:black;
            background-color: #3498db;
            padding: 2px;
            border-radius: 25px;
            display:inline-block;


        }
    </style>
</head>
<body>
<?php
/**
 * Created by PhpStorm.
 * User: samithn
 * Date: 26/09/2015
 * Time: 14:16
 */
include("db.php");
header('Access-Control-Allow-Origin: *');
//to show the searched results for persons
if(isset($_GET['name']) && isset($_GET['uid']))
{
    $uid=$_GET['uid'];
    $fname=trim($_GET['name']);
    $lname=trim($_GET['name']);
    $query="";

    //if only having white space in the fname

    if(strlen($fname)==0)
    {
        $query="select * from userinfo where uid!=$uid";
    }
    else
    {
        $query="select * from userinfo where uid!=$uid and (fname like '%$fname%' or lname like '%$lname%')";
    }



    $result=mysql_query($query) or die(mysql_error());

    if($result)
    {
        ?>

        <table >
        <?php
        while($row=mysql_fetch_array($result)) {
            echo "<tr><td><img src='#'/></td><td>" . $row['fname'] . " " . $row['lname'] . "</td>";
            ?>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <?php
            $fid = $row['uid'];
            $queryfriendcheck = "select * from friends where userid=$uid and frienduid=$fid";
            $resulfriendcheck = mysql_query($queryfriendcheck) or die(mysql_error());
            $friendid=0;
            $friendrelation=0;
            while ($rowcheck=mysql_fetch_array($resulfriendcheck))
            {
                $friendid=$rowcheck['frienduid'];
                $friendrelation=$rowcheck['id'];

            }
            if($friendid==$fid)
            {
                ?>
                <td><input class="btnr" type="button" value="Remove"/ onclick="removefriend(<?php echo $friendid ?>)"></td>
                <?php
            }
            else
            {
                ?>
                <td><input class="btnin" type="button" value="Add"/ onclick="addfriend(<?php echo $row['uid']?>)"></td>
                <?php
            }


            ?>



            <?php
            echo "</tr>";



        }
        ?>
        </table>
        <?php
    }


}


//to add the friends
if(isset($_GET['uid'] )&& isset($_GET['freinduid']))
{

    $uid=$_GET['uid'];
    $frienduid=$_GET['freinduid'];
    $query="insert into friends (userid,frienduid,addedon) values($uid,$frienduid,current_timestamp)";
    //need to remove if fix the show the
    $query2="insert into friends (userid,frienduid,addedon) values($frienduid,$uid,current_timestamp)";
    $result=mysql_query($query)or die(mysql_error());
    $result2=mysql_query($query2)or die(mysql_error());
    if(!$result or !$result2)
    {
        echo "error";
    }


}


//To remove the friends
if(isset($_GET['removefreinduid']) &&isset($_GET['uid']))
{
    $uid=$_GET['uid'];
    $freindid=$_GET['removefreinduid'];

    $query="delete from friends where userid=$uid && frienduid=$freindid";

    mysql_query($query)or die(mysql_error());
    //two way deletion
    $query2="delete from friends where userid=$freindid && frienduid=$uid";
    mysql_query($query2)or die(mysql_error());
}


//To show the friednlist on the silde panel
if(isset($_GET['getfreindlist'])&&isset($_GET['uid']))
{
    $uid=$_GET['uid'];
  //To show the all friends of a member
    if($_GET['searchedfriends']=="false")
    $query="select * from friends,userinfo where frienduid=uid and userid=$uid";
    else
    {
        $frid=$_GET['searchedfriends'];
        //To show the searched friend from the friend list
        $query="select * from friends,userinfo where frienduid=uid and  userid=$uid and (fname like '%$frid%' or lname like '%$frid%')";
    }
    $result=mysql_query($query)or die(mysql_error());
    ?>
    <table>
    <?php
    while($row=mysql_fetch_array($result))
    {
        $name=$row['fname']." ".$row['lname'];
        //This is taken send chat message
        $frienduid=$row['frienduid'];

        //the following code segement for check the online status
       // $onlinstatusquery="select * from useronlinestatus where id=$frienduid";
        $onlinstatusquery="SELECT TIMESTAMPDIFF(SECOND, `time`, NOW()),`status` FROM `useronlinestatus` where id=$frienduid";
        $resultonlinestatus=mysql_query($onlinstatusquery)or die(mysqli_error());
        $useronlinestatus="false";
        $useractivitiestime=0;
        while($rowonlinestatus=mysql_fetch_array($resultonlinestatus))
        {
            $useractivitiestime=$rowonlinestatus[0];
            $useronlinestatus=$rowonlinestatus[1];

        }

          //to get the online status
          if($useractivitiestime<=10 && $useronlinestatus=="true")
            {
                //echo "<tr  onclick=\"chatwindow($frienduid);triggerchatwindow()\"><td><img src='#'></td><td >$name</td></tr>";
                ?>
                <tr  onclick="assignfrienduid(<?php echo $frienduid?>)"><td><img src='#'>/td><td>
                        <?php echo $name?></td><td><img src="http://localhost/sep/images/emoticons_s/chat-icon.png" height="10" width="10">
                    </td></tr>
                <?php
            }
            else{
                //echo "<tr  onclick=\"chatwindow($frienduid);triggerchatwindow()\"><td><img src='#'></td><td >$name</td></tr>";
                ?>
                <tr  onclick="assignfrienduid(<?php echo $frienduid?>)"><td><img src='#'></td><td><?php echo $name?></td></tr>
                <?php
            }




    }
    ?>
    </table>

    <?php
}

//To show the chat messages
if(isset($_GET['toid'])&&isset($_GET['uid']))
{
    $toid=$_GET['toid'];
    $uid=$_GET['uid'];
    //to get the all messages from two way
    $query="select * from chatmessages where `userid`=$uid and `to`=$toid or (userid=$toid and `to`=$uid) order by `recieved`";
    $result=mysql_query($query)or die(mysql_error());
    ?>
    <!--table-->
    <?php
    while($row=mysql_fetch_array($result))
    {
        $msg=$row['message'];
        $id=$row['userid'];
        //to get the name
        $querytogetname="select * from userinfo where uid=$id";
        $resulttogetname=mysql_query($querytogetname)or die(mysql_error());
        $name;
        while($rowtogetname=mysql_fetch_array($resulttogetname))
        {
            $name=$rowtogetname['fname'].":";
        }
        //to show the ' instead of the *!*
        $msg=str_replace("*!*","'",$msg);
        //to show the emoticons
        $emoiconsquery="select * from emoticons";
        $resultemocions=mysql_query($emoiconsquery)or die(mysql_error());
        while($rowicons=mysql_fetch_array($resultemocions))
        {
            $ionschar=$rowicons['chars'];
            $imgicons="<img src=\"".$rowicons['image']."\"/ height=\"15\" width=\"15\">";
            $msg=str_replace($ionschar,$imgicons,$msg);
        }
        if($uid==$id)
        {
            //echo "<tr style='color:#3cb0fd;padding-right: 50%'><td>$name</td><td>$msg</td></tr>";
            echo "<div class='msglguser'>$name$msg</div>";
        }
        else
        {
            //echo "<tr><td>$name</td><td>$msg</td></tr>";
            echo "<div>$name$msg</div>";
        }

    }
    ?>
        <!--/table-->
            <?php
}

//To save the chat message
if(isset($_GET['toid'])&&isset($_GET['uid']) &&isset($_GET['message']))
{

    $uid=$_GET['uid'];
    $toid=$_GET['toid'];
    $msg=$_GET['message'];
    //to replace the message which having '
    $msg=str_replace("'","*!*",$msg);

    $query="INSERT INTO `chatmessages`(`userid`, `message`, `to`, `recieved`) VALUES ($uid,'$msg',$toid,current_timestamp)";

    $result=mysql_query($query)or die(mysql_error());
    if(!$result)
    {
        echo "error";
    }
}

//to update the online status time
if(isset($_GET['uid'] )&& isset($_GET['changestatus']))
{
    $uid=$_GET['uid'];
    $query="";
    //code segment inside this block going to create query which is updating the time and status
    if(isset($_GET['statusTF']))
    {
       $status=$_GET['statusTF'];
       $query="update useronlinestatus set status='$status', time=CURRENT_TIMESTAMP where id=$uid";
    }
    else
    {
        $query="update useronlinestatus set time=CURRENT_TIMESTAMP where id=$uid";
    }
    $result=mysql_query($query)or die(mysql_error());

}

//get friend name

if(isset($_GET['uid']) && isset($_GET['frndid'])&& isset($_GET['frndname']))
{
    $frnduid=$_GET['frndid'];
   // $query="select * from userinfo u,userimage i where u.uid=i.uid and u.uid=$frnduid";
    //select * from userinfo u,userimage i where u.uid=i.uid and u.uid=1
    $query="select * from userinfo u,userimage i where u.uid=i.uid and u.uid=$frnduid";

    $result=mysql_query($query)or die(mysql_error());

    while($row=mysql_fetch_array($result))
    {

        $imgurl=$row['imgurl'];
        echo "<img src='$imgurl' height=\"25\" width=\"20\"/>"." ".$row['fname']." ".$row['lname'];
    }
}
?>
</body>
</html>
