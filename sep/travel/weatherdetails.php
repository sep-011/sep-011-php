
<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title></title>
</head>
<body>

<?php
if(isset($_POST['zipcode'])){
    $zipcode = $_POST['zipcode'];

}else{
    $zipcode = '2189781';
}
$result = file_get_contents('http://weather.yahooapis.com/forecastrss?w=' . $zipcode . '&u=f');
$xml = simplexml_load_string($result);



$xml->registerXPathNamespace('yweather', 'http://xml.weather.yahoo.com/ns/rss/1.0');
$location = $xml->channel->xpath('yweather:location');

if(!empty($location)){
    foreach($xml->channel->item as $item){
        $current = $item->xpath('yweather:condition');
        $forecast = $item->xpath('yweather:forecast');
        $current = $current[0];


            echo "<h1>Weather for {$location[0]['city']}, {$location[0]['region']}</h1>";
            echo "<small>{$current['date']}</small>";
            echo "<h2>Current Conditions</h2>";
            echo "<p>";
            echo "<span style=\"font-size:72px; font-weight:bold;\">{$current['temp']}&deg;F</span>";
            echo "<br/>";
            echo "<img src=\"http://l.yimg.com/a/i/us/we/52/{$current['code']}.gif\" width=\"100\" height=\"100\" style=\"vertical-align: middle;\"/>";
            echo "{$current['text']}";
            echo "</p>";
            echo "<h2>Forecast</h2>";
            echo "<h4>";
            echo "{$forecast[0]['day']} - {$forecast[0]['text']}. High: {$forecast[0]['high']} Low: {$forecast[0]['low']}";
            echo "<br/>";
            echo "{$forecast[1]['day']} - {$forecast[1]['text']}. High: {$forecast[1]['high']} Low: {$forecast[1]['low']}";
            echo "<br/>";
            echo "{$forecast[2]['day']} - {$forecast[2]['text']}. High: {$forecast[2]['high']} Low: {$forecast[2]['low']}";
            echo "<br/>";
            echo "{$forecast[3]['day']} - {$forecast[3]['text']}. High: {$forecast[3]['high']} Low: {$forecast[3]['low']}";
            echo "<h4>";
            echo "</p>";




    }
}else{
    $output = '<h1>Weather data not available at the moment.</h1>';
}
?>

</body>
</html>