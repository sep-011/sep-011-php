<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title></title>
</head>
<body>
<table data-role="table"  data-mode="columntoggle" class="ui-responsive ui-shadow" id="myTable" width="100%">
    <?php
    include_once('db.php');

    if(isset($_POST["sug"])) {
        $id= $_POST['sug'];

        //selecting favorite places category and get the count
        $selectString = "select * From favplaces f,locationcategory lc where lc.LocationId=f.placeid and f.userId={$id}";

        $comments = mysql_query($selectString);

        $historicalcount=0;
        $religiouscount=0;
        $naturalcount=0;
        $meseumcount=0;
        $catarray=array();


        while($row= mysql_fetch_array($comments)) {

            if ($row['category'] == 'historical') {
                $historicalcount += 1;
            } elseif ($row['category'] == 'religious') {
                $religiouscount += 1;
            } elseif ($row['category'] == 'natural') {
                $naturalcount += 1;
            } elseif ($row['category'] == 'museum') {
                $meseumcount += 1;
            }

        }//end of while loop
            /////

            //make array for key=> value

            $catarray['historical']=$historicalcount;

            $catarray['religious']=$religiouscount;

            $catarray['natural']=$naturalcount;

            $catarray['museum']=$meseumcount;


            for ($x = 0; $x <= sizeof($catarray); $x++) {



                //get the max value from the array with relevant key

                $max_key = 'key';
                $max_val = -1;

                foreach ($catarray as $key => $value) {
                   // echo "max value " . $max_val . " and value " . $value;
                    //echo " max_key=" . $max_key;
                    //echo "<br>";
                    if ($value > $max_val) {
                        $max_key = $key;
                        $max_val = $value;
                    }

                }//end of foreach

                //echo "max key -->" . $max_key;
                //echo "<br>";



                //}


                ?>

                <?php

                //to filter favorite locations from suggestions
                $selectStringfavorite = "SELECT * FROM favplaces WHERE userId=$id";

                $commentsfavorite = mysql_query($selectStringfavorite);

                $placenumbers = array();
                $count = 0;

                while ($row = mysql_fetch_array($commentsfavorite)) {
                    $place = $row['placeid'];
                    $placenumbers[$count++] = $place;
                }

                $stat;

                //selecting suggested locations details according to favorites
                $category = $max_key;

                $selectStringsuggestion = "select * From locations l,locationcategory lc where l.Location_id=lc.LocationId and lc.category='{$category}';";

                $commentssuggest = mysql_query($selectStringsuggestion);



                //to control displaying buttons if travel plans returns

                $selectStringTp="SELECT * FROM travelplan WHERE user_id=$id";
                $commentsTp=mysql_query($selectStringTp);

                $getplaces=array();

                while( $row = mysql_fetch_array($commentsTp))
                {
                    if($row['plan'] != null)
                    {
                    $getplaces= (explode(" ",$row['plan']));}
                    else{
                        $getplaces=$row['plan'];
                    }



                }


                if($x==0) {
                    ?>

                    <tr>
                        <p><?php echo "Preferences : ".$max_key ?></p>
                        <h3><?php echo "According to your preferences first suggestions" ?></h3>
                    </tr>

                <?php
                }
                if($x==1) {
                    ?>

                    <tr>
                        <p><?php echo "Preferences : ".$max_key ?></p>
                        <h3><?php echo "According to your preferences second suggestions" ?></h3>
                    </tr>

                <?php
                }
                if($x==2) {
                    ?>

                    <tr>
                        <p><?php echo "Preferences : ".$max_key ?></p>
                        <h3><?php echo "According to your preferences third suggestions" ?></h3>
                    </tr>

                <?php
                }
                if($x==3) {
                    ?>

                    <tr>
                        <p><?php echo "Preferences : ".$max_key ?></p>
                        <h3><?php echo "According to your preferences fourth suggestions" ?></h3>
                    </tr>

                <?php
                }

                while ($row = mysql_fetch_array($commentssuggest)) {

                    //going through loop and check $row[location_id] is there in the list
                    foreach ($placenumbers as $i => $pi) {
                        if ($pi == $row['Location_id']) {
                            //echo "equals";
                            $stat = 0;
                            break;
                        } else {
                            $stat = 1;
                            //echo "notequals";
                        }
                    }

                    //stat == 1 mean favorite locations id is not equal to location id (filtering)
                    if ($stat == 1) {
                        ?>
                        <table data-role="table" data-mode="columntoggle" class="ui-responsive ui-shadow" id="myTable" width="100%">
                        <tr>
                        <td><?php echo "<div align=\"justify\"> <img src=\"{$row['Image']}\" width=\"85\" height=\"85\" border=\"3\"></div>"; ?></td>
                        <td><h4><?php echo $row['Location_Name']; ?></h4></td>
                        <td><a href="javascript:getRecplacesdetails(<?php echo $row['Location_id'] ?>);"
                               class="ui-btn ui-btn">Qiuck view</a>
                            <?php
                                if($getplaces == null){
                            ?>
                            <a href="javascript:Addtravelplan(<?php echo $row['Location_id'] ?>,<?php echo $id ?>);"
                               class="ui-btn ui-btn">Add</a>
                                <?php }// end of if(getplaces)
                                elseif ($getplaces !=null)
                                {
                                    $check;
                                    for ($y = 0; $y < sizeof($getplaces); $y++) {


                                        if($getplaces[$y] == $row['Location_id'])
                                        {
                                           $check=0;
                                            break;
                                        }
                                        else{
                                            $check=1;
                                        }

                                    }

                                    if($check == 1) {
                                        ?><a href="javascript:Addtravelplan(<?php echo $row['Location_id'] ?>,<?php echo $id ?>);"
                               class="ui-btn ui-btn">Add</a><?php

                                    }
                                    elseif($check==0)
                                    {
                                        ?><a href="javascript:removetravelplan(<?php echo $row['Location_id'] ?>,<?php echo $id ?>);"
                                             class="ui-btn ui-btn">Remove from plan</a><?php
                                    }

                                }

                            ?>
                                    </td>


                    <?php
                    }//end of if


                }//end of while

                unset($catarray[$max_key]);

                ?>

                </tr>
                </table>
            <?php

            }// end of for loop

    }

    ?>
</table>
</body>
</html>