<html>
<body>

<?php
/**
 *
 * Use to control the view request from the application
 */

include_once($_SERVER["DOCUMENT_ROOT"].'/sep/optimalnew/connection.php');
include_once($_SERVER["DOCUMENT_ROOT"].'/sep/optimalnew/Views/View.php');


$connection=new connection();
$constat=$connection->connect();
$view= new View();

if(!$constat)
{
    echo "Server connection terminated. please try again in few seconds";

}
else{

    /*
     * To display all locations
     * Get_AllLocations() method is used
     */
    if(isset($_POST["all"]))
    {
        $stringLong = $_POST["all"];

        $view->Get_AllLocations($stringLong);

    }

    /* Receive location id
     * To get one latitude longitude value
     * GetOneLatlng() method is used
     */
    if(isset($_POST["li"])) {

        $locid=$_POST["li"];

        $view->GetOneLatlng($locid);

    }

    /* Receive location id and selected location id
    * To get filtered location details
    * Filter_locations() method is used
    */
    if(isset($_GET["lid"]) & isset($_GET["slid"]))
    {
        $misLid=$_GET["lid"];
        $setLid=$_GET["slid"];

        $view->Filter_locations($misLid,$setLid);
    }

    /* Receive location id and selected location id
    * To get filtered location details again
    * ReFilter_Locations() method is used
    */
    if(isset($_GET["sellid"]) & isset($_GET["setlid"]) & isset($_GET["prelid"]))
    {

        $misLid=$_GET["sellid"];
        $setLid=$_GET["setlid"];
        $preselect=$_GET["prelid"];

        $view->ReFilter_Locations($misLid,$setLid,$preselect);

    }



}




?>

</body>
</html>