<?php
/**
 *
 * Use to view data through application
 */

class View
{

    /* To Get all locations
     * @param $string - set of selected locations concatenate with "a"
     */
    public function Get_AllLocations($String)
    {
        $stringLong = $String;

        $selectString = "select * from locations";

        $comments = mysql_query($selectString);


        if ($stringLong == null) {
            ?>
            <table data-role="table" data-mode="columntoggle" class="ui-responsive ui-shadow" id="myTable" width="100%">
            <?php

            while ($row = mysql_fetch_array($comments)) {
                $location = $row['Location_Name'];
                ?>
                <tr>
                    <td><?php echo "<div align=\"center\"> <img src=\"{$row['Image']}\" width=\"75\" height=\"75\" border=\"3\"> </div>"; ?></td>
                    <td><?php echo $location; ?></td>

                    <td>
                        <a href="javascript:Add(<?php echo $row['latitude']; ?>,<?php echo $row['longitude']; ?>,<?php echo $row['Location_id'] ?>);"
                           class="ui-btn ui-btn">Add</a></td>

                </tr>
                <tr>
                    <td></td>
                </tr>
            <?php


            }

            ?>
            <table>

        <?php


        } //end of stringLong==null


        else {

            $arrayid = array();

            $arrayid = (explode("a", $stringLong));



            ?>
            <table data-role="table" data-mode="columntoggle" class="ui-responsive ui-shadow" id="myTable" width="100%">
            <?php

            while ($row = mysql_fetch_array($comments)) {
                foreach ($arrayid as $i => $pi) {
                    if ($pi == $row['Location_id']) {
                        //echo "equals";
                        $stat = 0;
                        break;
                    } else {
                        $stat = 1;
                        //echo "notequals";
                    }
                }

                if ($stat == 1) {
                    $location = $row['Location_Name'];
                    ?>
                    <tr>
                        <td><?php echo "<div align=\"center\"> <img src=\"{$row['Image']}\" width=\"75\" height=\"75\" border=\"3\"> </div>"; ?></td>
                        <td><?php echo $location; ?></td>

                        <td>
                            <a href="javascript:Add(<?php echo $row['latitude']; ?>,<?php echo $row['longitude']; ?>,<?php echo $row['Location_id'] ?>);"
                               class="ui-btn ui-btn">Add</a></td>

                    </tr>
                    <tr>
                        <td></td>
                    </tr>
                <?php

                } else {
                    $location = $row['Location_Name'];
                    ?>
                    <tr>
                        <td><?php echo "<div align=\"center\"> <img src=\"{$row['Image']}\" width=\"75\" height=\"75\" border=\"3\"> </div>"; ?></td>
                        <td><?php echo $location; ?></td>

                        <td>
                        </td>

                    </tr>
                    <tr>
                        <td></td>
                    </tr>
                <?php
                }

            }

            ?>
            <table>

        <?php


        }   //end of else

    }


    /* To one latitude longitude value
     * @param $locid - location id
     */
    public function GetOneLatlng($locid)
    {
        $selectString = "select * from locations where Location_id=$locid";

        $comments = mysql_query($selectString);

        while ($row = mysql_fetch_array($comments)) {
            echo $row['latitude'] . "," . $row['longitude'] . "," . $row['Location_id'];

        }

    }


    /* To Filter location from selected location
     * @param $selectedid - Selected location id
     * @param $locationset - Set of location ids
     */
    public function Filter_locations($selectedid, $locationset)
    {
        $misLid = $selectedid;
        $setLid = $locationset;

        $arrayid = array();

        $arrayid = (explode("a", $setLid));


        echo '<br>';
        echo '<h3>Please Add Places And Press Proceed</h3>';

        ?>
        <table data-role="table" data-mode="columntoggle" class="ui-responsive ui-shadow" id="myTable" width="100%">
        <?php
        $t = 0;
        for ($i = 0; $i < sizeof($arrayid) - 1; $i++) {
            //echo "times ".$t++." ".$arrayid[$i];
            if ($arrayid[$i] == $misLid) {
                //echo "equals";
                $stat = 0;
                continue;
            } else if ($arrayid[$i] != $misLid) {
                $stat = 1;
                //echo "notequals";

                $selectString = "select * from locations where Location_id=$arrayid[$i]";

                $comments = mysql_query($selectString);

                while ($row = mysql_fetch_array($comments)) {

                    $location = $row['Location_Name'];
                    ?>
                    <tr>
                        <td><?php echo "<div align=\"center\"> <img src=\"{$row['Image']}\" width=\"75\" height=\"75\" border=\"3\"> </div>"; ?></td>
                        <td><?php echo $location; ?></td>

                        <td>
                            <a href="javascript:Addnext(<?php echo $row['latitude']; ?>,<?php echo $row['longitude']; ?>,<?php echo $row['Location_id'] ?>);"
                               class="ui-btn ui-btn">Add</a></td>

                    </tr>
                    <tr>
                        <td></td>
                    </tr>
                <?php

                }


            }
        }

        ?>
        <table><?php


    }

    /* To Re Filter location from selected location
     * @param $selectedid - Selected location id
     * @param $locationset - Set of location ids
     * @param $preselected - previously selecte location id
     */
    public function ReFilter_Locations($selectedid, $locationset, $preselected)
    {

        $misLid=$selectedid;
        $setLid=$locationset;
        $preselect=$preselected;

        $arrayid = array();

        $arrayid = (explode("a", $setLid));


        echo '<br>';
        echo '<h3>Please Add Places And Press Proceed</h3>';

        ?>
        <table data-role="table" data-mode="columntoggle" class="ui-responsive ui-shadow" id="myTable" width="100%">
        <?php
        $t=0;
        for($i=0; $i<sizeof($arrayid)-1;$i++) {
            //echo "times ".$t++." ".$arrayid[$i];
            if($arrayid[$i] == $misLid)
            {
                //echo "Selected";
                $stat=0;
                continue;
            }
            elseif($arrayid[$i] == $preselect){
                //echo "Pre selected";
                continue;
            }
            else if($arrayid[$i] != $misLid)
            {
                $stat=1;
                //echo "notequals";

                $selectString="select * from locations where Location_id=$arrayid[$i]";

                $comments=mysql_query($selectString);

                while($row=mysql_fetch_array($comments)){

                    $location = $row['Location_Name'];
                    ?>
                    <tr>
                        <td><?php echo "<div align=\"center\"> <img src=\"{$row['Image']}\" width=\"75\" height=\"75\" border=\"3\"> </div>"; ?></td>
                        <td><?php echo $location; ?></td>

                        <td><a href="javascript:Addnext(<?php echo $row['latitude']; ?>,<?php echo $row['longitude']; ?>,<?php echo $row['Location_id'] ?>);"
                               class="ui-btn ui-btn">Add</a></td>

                    </tr>
                    <tr>
                        <td></td>
                    </tr>
                <?php

                }



            }
        }

        ?><table><?php


    }



} 