<html>
<body>

<?php
/**
 *
 * Use to control the view request from the application
 */

include_once($_SERVER["DOCUMENT_ROOT"].'sep/group/Model/Model.php');
include_once($_SERVER["DOCUMENT_ROOT"].'sep/group/connection.php');
include_once($_SERVER["DOCUMENT_ROOT"].'sep/group/Views/View.php');


$connection=new connection();
$model = new Model();
$constat=$connection->connect();
$view= new View();

if(!$constat)
{
    echo "Server connection terminated. please try again in few seconds";

}
else{

    /*
     * To receive user id and group id
     *  display groups to join or leave
     * Group_JoinLeave() method is used
     */
    if(isset($_POST['id']) && isset($_POST['gid']))
    {

        $uid = $_POST["id"];
        $grpid=$_POST["gid"];

        $check="select * from zusergrps where grpid={$grpid} and uid={$uid}";

        $recid=$model->Get_onevalue($check,"rid");

        $view->Group_JoinLeave($uid,$grpid,$recid);
    }

    /*
     * To receive letter/word
     *  display search result
     *  LiveSearch() method is used
     */
    if(isset($_GET['q']))
    {
        $string=$_GET["q"];

        $view->LiveSearch($string);
    }

    /*
     * To receive user id group id
     *  SelectGroup() method is used
     */
    if(isset($_POST['uid']) && isset($_POST['grpid']))
    {
        $uid=$_POST["uid"];
        $grpid=$_POST["grpid"];

        $view->SelectGroup($uid,$grpid);
    }

    /*
     * To receive user id group id
     *  To get the latitude longitude
     *  Get_Users() method is used
     */
    if(isset($_POST["grp"]) & isset($_POST["id"]) )
    {


        $gid=$_POST["grp"];

        $uid= $_POST["id"];


        $SelectString="SELECT * FROM userinfo ui,zusergrps ug, zuslocation ul WHERE ug.uid=ul.uid and ug.uid=ui.uid and ug.grpid={$gid}";

        $view->Get_Users($SelectString,$uid);


    }


}




?>

</body>
</html>