<?php
/**
 *
 * Use to control the transactions request from the application
 */

include_once($_SERVER["DOCUMENT_ROOT"].'sep/group/Model/Model.php');
include_once($_SERVER["DOCUMENT_ROOT"].'sep/group/connection.php');
include_once($_SERVER["DOCUMENT_ROOT"].'sep/group/Views/View.php');


$connection=new connection();
$model = new Model();
$constat=$connection->connect();
$view= new View();

if(!$constat)
{
    echo "Server connection terminated. please try again in few seconds";

}
else{

    /*
     * To receive group info to insert to database
     *  transaction() method used
     */
    if(isset($_GET['q']) && isset($_GET['uid']) && isset($_GET['code']))
    {
        $string = $_GET["q"];
        $uid = $_GET["uid"];
        $code = $_GET["code"];

        $insertString = "insert into groupname (group_name,creatorid,code) values (\"$string\",{$uid},\"$code\")";

        $stat = $model->transaction($insertString);

        if (!$stat) {
            echo "Erorr occurred";
        } else {
            //echo "Add success";

            $maxSelect = "select max(grpid) from groupname where creatorid ={$uid}";

            $max = $model->Get_onevalue($maxSelect,"max(grpid)");

            $insertString = "insert into zusergrps (uid,grpid) values ({$uid},{$max})";

            $stat2 = $model->transaction($insertString);

            if (!$stat2) {

                echo "Erorr occurred";

            } else {

                //check user location record available
                $query="select count(*) as count from zuslocation where uid={$uid}";
                $count=$model->Get_onevalue($query,"count");

                if($count ==0)
                {
                    $insertString="insert into zuslocation (uid,latitude,longitude) values ({$uid},null,null) ";

                    $model->transaction($insertString);

                }

                echo "New Group Created Successful";

            }

        }
    }


    /*
     * To receive user info
     *  when user join to a group
     * check for user location details available or not
     * Then make entry if user location details unavailable
     *  transaction() method used
     */
    if(isset($_GET['id']) && isset($_GET['gid']) && isset($_GET['code']))
    {

        $uid = $_GET["id"];
        $grpid = $_GET["gid"];
        $code = $_GET["code"];

        $oricode;

        $selectString = "select * from groupname where grpid={$grpid}";

        $oricode = $model->Get_onevalue($selectString,"code");


        if ($oricode == $code) {

            $insertString = "insert into zusergrps (uid,grpid) values ({$uid},{$grpid})";
            $stat = $model->transaction($insertString);

            if (!$stat) {
                echo "Erorr occurred";
            } else {

                //check user location record available
                $query="select count(*) as count from zuslocation where uid={$uid}";
                $count=$model->Get_onevalue($query,"count");

                if($count ==0)
                {
                    $insertString="insert into zuslocation (uid,latitude,longitude) values ({$uid},null,null) ";

                    $model->transaction($insertString);

                }

                echo "User Join Successful";

            }

        } else {
            echo "Please insert correct secret code and try again";
        }

    }


    /*
     * To receive user info to delete user from group
     *  transaction() method used
     */
    if(isset($_POST['id']) && isset($_POST['gid']))
    {
        $uid = $_POST["id"];
        $grpid = $_POST["gid"];

        $deleteString = "delete from zusergrps where uid={$uid} and grpid={$grpid}";

        $stat = $model->transaction($deleteString);

        if (!$stat) {
            echo "Erorr occurred";
        } else {
            echo '<br/>';
            echo 'User Group leave successful';
        }

    }

    /*
     * To receive user id and latitude longitude and save
     *  transaction() method used
     */
    if(isset($_POST['uid']) && isset($_POST['lat']) && isset($_POST['lng']))
    {
        $uid= $_POST['uid'];

        $lat= $_POST['lat'];

        $lng= $_POST['lng'];

        $updateString="UPDATE zuslocation SET latitude=\"$lat\",longitude=\"$lng\" WHERE uid = {$uid}";

        $stat=$model->transaction($updateString);

        if (!$stat) {
            echo "Erorr occurred";
        } else {
            echo '<br/>';
            echo 'Added successful';

        }
    }


}



?>