<?php
/**
 *
 * Use to view data through application
 */

class View {


    /* To select Group display results
     * @param $uid - user id
     * @param $grpid - user group id
     */
    public function SelectGroup($uid,$grpid)
    {
        $selectString="select * from zusergrps ug,groupname gn where ug.grpid=gn.grpid and uid={$uid}";

        $comments=mysql_query($selectString);

        ?>
        <table data-role="table" data-mode="columntoggle" class="ui-responsive ui-shadow" id="myTable" >
            <?php
            while($row=mysql_fetch_array($comments))
            {
                ?>

                <tr>
                    <td><?php echo $row['group_name'] ?></td>
                    <?php if ($grpid != $row['grpid']) { ?>
                        <td><a href="javascript:setgrplocal(<?php echo $row['grpid'] ?>);" class="ui-btn ui-btn">Select</a></td>
                    <?php }
                    else{
                        ?>
                        <td><a href="javascript:setgrplocal(<?php echo $row['grpid'] ?>);" class="ui-btn ui-btn">Current Selected</a></td>
                    <?php

                    }
                    ?>
                </tr>

            <?php

            }
            ?>
        </table>
    <?php

    }


    /* To join or leave group display results
     * @param $uid - user id
     * @param $grpid - user group id
     * @param $recid - record id
     */
    public function Group_JoinLeave($uid,$grpid,$recid)
    {

        $selectString = "select * from groupname where grpid ={$grpid}";
        $comments=mysql_query($selectString);

        while($row=mysql_fetch_array($comments))
        {
            if($recid == null) {
                ?>
                <tr>
                    <td><?php echo $row['group_name'] ?></td>
                    <td><a href="javascript:Addmembers(<?php echo $row['grpid'] ?>);" class="ui-btn ui-btn">Join</a></td>
                </tr>
            <?php
            }
            else
            {
                ?>
                <tr>
                    <td><?php echo $row['group_name'] ?></td>
                    <td><a href="javascript:deletemembers(<?php echo $row['grpid'] ?>);" class="ui-btn ui-btn">Leave</a></td>
                </tr>
            <?php
            }

        }

    }


    /* To display search results
     * @param $string - letter/word
     */
    public function LiveSearch($string)
    {

        $selectString = "select * from groupname where group_name LIKE '%$string%'";
        $comments=mysql_query($selectString);

        ?>
        <table data-role="table" data-mode="columntoggle" class="ui-responsive ui-shadow" id="myTable" >
        <?php
        while($row=mysql_fetch_array($comments))
        {

            ?>
            <tr>
                <td><?php echo $row['group_name'] ?></td>
                <td><a href="javascript:getgrpnames(<?php echo $row['grpid'] ?>);" class="ui-btn ui-btn">select</a></td>
            </tr>
        <?php


        }
        ?>
        </table>
        <?php

    }

    /* To get group latitude longitude
     * @param $string - sql query
     * @param $uid - user id
     */
    public function Get_Users($string,$uid)
    {
        $no=0;

        $Comments=mysql_query($string);

        ?>
        <table>
            <?php

            while($row= mysql_fetch_array($Comments)) {

                if($uid != $row['uid']) {
                    ?>

                    <tr>

                        <td><textarea style="display:none;" id="<?php echo $no ?>"><?php echo $row['latitude'] ?>,<?php echo $row['longitude'] ?>,
                                <?php echo $row['uid'] ?>,<?php echo $row['fname'] ?>,<?php echo $row['lname'] ?></textarea>
                        </td>


                    </tr>

                    <?php
                    $no++;


                }

            }

            ?>
            <tr><textarea style="display:none;" id="count"><?php echo $no ?></textarea></tr>
        </table>
    <?php

    }


} 