<?php
/**
 * Created by PhpStorm.
 * User: Roshin
 * Date: 10/4/2015
 * Time: 11:34 PM
 */
include_once('db.php');


class db_access_r {

    //images directory in the server
    public $hostImg = "http://sep.esy.es/sep/img_tst/imgs/";


    /**
     * To add new sharing comment and location for a share post
     * @param int $uid  user id
     * @param string $description  shared post text
     * @param string $longitude  longitude of shared location
     * @param string $latitude  latitude of shared location
     */
    public function addComment($uid, $description, $longitude, $latitude){

        //Insert sharing details to database
        $new_image_name='';
        $queryInsert = "INSERT INTO share_notific (uid,description,image,lat,lon,shareDate,hidden) VALUE($uid,'$description','$new_image_name','$latitude','$longitude',current_timestamp,'N')";
        mysql_query($queryInsert) or die(mysql_error());

        //get the unique share id and return it
        $id = mysql_insert_id();
        echo "{$id}";
    }

    /**
     * To upload images of a sharing post
     * @param int $shareId  Share id for the images
     */
    public function uploadImages($shareId){
        $new_image_name="";

        //Check whether a image is received
        if (isset($_FILES['file'])) {

            //Give a unique name to the image file
            $new_image_name=uniqid() . ".jpg";
            //upload to the server
            move_uploaded_file($_FILES["file"]["tmp_name"], "./imgs/".$new_image_name);
        }
        else {

        }
        //insert a added image recode to the database
        $queryInsert = "INSERT INTO notific_images (notific_id,img_url) VALUE($shareId,'$new_image_name')";
        mysql_query($queryInsert) or die(mysql_error());
    }

    /**
     * To get notifications count(When client page loaded)
     * @param int $userId  logged user id     
     */
    public function getNewNotificationCount($userId){
        $queryNotificCnt = "SELECT COUNT(*) FROM share_notific s, friends f WHERE f.frienduid={$userId} AND f.userid=s.uid AND s.hidden='N' AND s.shareId NOT IN (SELECT r.notificId FROM remove_notific r)";
        $result=mysql_query($queryNotificCnt) or die(mysql_error());

        $notificCount="0";
        while($row=mysql_fetch_array($result)){
            $notificCount=$row[0];
        }
        echo $notificCount;
    }
    

    /**
     * To display all the new notifications and all own share posts
     * @param int $userId  logged user id
     */
    public function displayAllNotifications($userId){

        //If request to display all own posts
        if(isset($_GET['myShare'])){
            //$queryAllNotific = "SELECT * FROM userinfo u, share_notific s left outer join notific_images ni ON s.shareId=ni.notific_id WHERE s.uid={$userId} AND u.uid=s.uid GROUP BY s.shareId ORDER BY s.shareDate DESC";
			$queryAllNotific = "SELECT * FROM userinfo u, share_notific s left outer join notific_images ni ON s.shareId=ni.notific_id left outer join userimage ui ON s.uid=ui.uid WHERE s.uid={$userId} AND u.uid=s.uid GROUP BY s.shareId ORDER BY s.shareDate DESC";
        }

        //To display all the posts shared with the user(Shared with me)
        else if(isset($_GET['sharedWithMe'])){
            //$queryAllNotific = "SELECT * FROM friends f, userinfo u, share_notific s left outer join notific_images ni ON s.shareId=ni.notific_id WHERE f.frienduid={$userId} AND f.userid=s.uid AND u.uid=s.uid AND s.hidden='N' GROUP BY s.shareId ORDER BY s.shareDate DESC";
			$queryAllNotific = "SELECT * FROM friends f, userinfo u, share_notific s left outer join notific_images ni ON s.shareId=ni.notific_id left outer join userimage ui ON s.uid=ui.uid WHERE f.frienduid={$userId} AND f.userid=s.uid AND u.uid=s.uid AND s.hidden='N' GROUP BY s.shareId ORDER BY s.shareDate DESC";
        }

        //To display all the new received notifications
        else{
            //$queryAllNotific = "SELECT * FROM friends f, userinfo u, share_notific s left outer join notific_images ni ON s.shareId=ni.notific_id WHERE f.frienduid={$userId} AND f.userid=s.uid AND u.uid=s.uid AND s.hidden='N' AND s.shareId NOT IN (SELECT r.notificId FROM remove_notific r) GROUP BY s.shareId ORDER BY s.shareDate DESC";
			$queryAllNotific = "SELECT * FROM friends f, userinfo u, share_notific s left outer join notific_images ni ON s.shareId=ni.notific_id left outer join userimage ui ON s.uid=ui.uid WHERE f.frienduid={$userId} AND f.userid=s.uid AND u.uid=s.uid AND s.hidden='N' AND s.shareId NOT IN (SELECT r.notificId FROM remove_notific r) GROUP BY s.shareId ORDER BY s.shareDate DESC";
			
        }
        $result=mysql_query($queryAllNotific) or die(mysql_error());
		
		return $result;        
    }
    

    /**
     * To get details of a specific NEW notification
     * @param int $shareId  requesting post id
     * @param int $userId  logged user id
     */
    public function getNewNotificationDetails($shareId,$uid){
        //$queryGetNotification = "SELECT * FROM share_notific s, userinfo u WHERE u.uid=s.uid AND s.shareId={$shareId}";
		$queryGetNotification = "SELECT * FROM userinfo u, share_notific s left outer join userimage ui ON s.uid=ui.uid WHERE u.uid=s.uid AND s.shareId={$shareId}";
        $result=mysql_query($queryGetNotification) or die(mysql_error());     
		return $result;
    }
	
	/**
     * Add a record of viewed notifications
     * @param int $shareId  viewed post id
	 * @param int $uid  user id
     */
	public function viewedNotification($shareId, $uid){
		$queryNotificRemove = "INSERT INTO remove_notific(notificId,uid) VALUES({$shareId}, {$uid})";
        mysql_query($queryNotificRemove) or die(mysql_error());
	}
    
	/**
     * get shared images of a post
     * @param int $shareId  post id
     */
	public function getNotificationImages($shareId){
		$queryImages = "SELECT * FROM notific_images WHERE notific_id={$shareId}";
		$result=mysql_query($queryImages) or die(mysql_error());
		return $result;
	}

    /**
     * To get details of a users own notification details
     * @param int $shareId  requesting post id
     */
    public function getOwnNotificationDetails($shareId){
        //$queryGetNotification = "SELECT * FROM share_notific s, userinfo u WHERE u.uid=s.uid AND s.shareId={$shareId}";
		$queryGetNotification = "SELECT * FROM userinfo u, share_notific s left outer join userimage ui ON s.uid=ui.uid WHERE u.uid=s.uid AND s.shareId={$shareId}";
        $result = mysql_query($queryGetNotification) or die(mysql_error());

		return $result;		
    }

    
    /**
     * Delete a share post
     * @param int $shareId  requesting post id to delete
     */
    public function deletePost($shareId){

        $queryDelete = "DELETE FROM share_notific WHERE shareId={$shareId}";
        mysql_query($queryDelete) or die(mysql_error());
    }

    
    /**
     * To hide and allow a share post
     * @param int $shareId  requesting post id to hide/allow
     * @param string $action  requesting action(hide/allow)
     */
    public function hidePost($shareId,$action){
        
        $value="";
        
        //if request to hide the post
        if($action == 'hide'){
            $value = "Y";
        }

        //request to allow the post
        else{
            $value = "N";
        }

        //update the record
        $queryUpdate = "UPDATE share_notific SET hidden='{$value}' WHERE shareId={$shareId}";
        mysql_query($queryUpdate) or die(mysql_error());
    }
} 