<?php
include_once('db.php');
require_once('db_access_r.php');

//images directory in the server
$hostImg = "http://sep.esy.es/sep/img_tst/imgs/";

$db_access = new db_access_r();


/*To add new sharing comment and location*/
if(isset($_POST['desc']) && isset($_POST['lon'])){

    //get all the posted values
    $uid = $_POST['uid'];
    $desc = $_POST['desc'];
    $lon = $_POST['lon'];
    $lat = $_POST['lat'];

    $db_access->addComment($uid,$desc,$lon,$lat);

}


/*To upload images of a share post*/
else if(isset($_POST['image'])){

    $shareId = $_POST['shareId'];

    $db_access->uploadImages($shareId);
}

/*To get notifications count(When client page loaded)*/
else if(isset($_GET['notific'])){

    $usrid = $_GET['notific'];

    $db_access->getNewNotificationCount($usrid);
}

/*To display all the new notifications and all own share posts*/
else if(isset($_GET['allNotific'])){

    $usrid = $_GET['allNotific'];

    $result = $db_access->displayAllNotifications($usrid);
	
	//flag to check there are new notifications or not.
	$emptyNotifics = true;
	$notifications = "";
	$notifications .= '<TABLE data-role="table" data-mode="columntoggle" class="ui-responsive ui-shadow" id="myTable" width="100%" >';
	while($row=mysql_fetch_array($result)){

		$emptyNotifics = false;

		if(isset($_GET['myShare'])){
			$notifications .= "<TR onclick='getOwnNotificDetails({$row['shareId']});'>";
		}
		else{
			$notifications .= "<TR onclick='getNotificDetails({$row['shareId']});'>";
		}
		$notifications .= "<TD>";
		
		//if user doesn't have a profile picture use default profile picture
		if(empty($row['imgurl'])){
			$notifications .= "<IMG src='{$hostImg}default_profile.jpg' class='profPic'>";
		}
		else{
			$notifications .= "<IMG src='{$row['imgurl']}' class='profPic'>";
		}
		$notifications .= "</TD>";
		$notifications .= "<TD>";
		$notifications .= "<B>{$row['fname']} {$row['lname']}</B> Shared<BR>";
		$notifications .= "<IMG src=\"{$hostImg}comment_ico.png\"> {$row['description']}";
		$notifications .= "</TD>";

		if(!empty($row['img_url'])){
			$notifications .= "<TD class='imgTd'>";
			$notifications .= "<div align=\"center\"><IMG src=\"{$hostImg}{$row['img_url']}\" class=\"notificImg\"></div>";
			$notifications .= "</TD>";
		}
		else{
			$notifications .= "<TD></TD>";
		}
		$notifications .= "</TR>";
	}
	$notifications .= "</TABLE>";

	if($emptyNotifics){
		echo "";
	}
	else {
		echo $notifications;
	}
}

/*To get details of a specific NEW notification*/
else if(isset($_GET['getNotific'])){

    //notification id and user id
    $notific_Id = $_GET['getNotific'];
    $uid = $_GET['uid'];

    $result = $db_access->getNewNotificationDetails($notific_Id,$uid);
	
	$notificationDetails="";
	while($row=mysql_fetch_array($result)){

		//if available append the location details to the beginning of the string
		if(!empty($row['lat'])) {
			$notificationDetails .= "***";

			$notificationDetails .= "{$row['lat']}";
			$notificationDetails .= "/";
			$notificationDetails .= "{$row['lon']}";

			$notificationDetails .= "***";
		}
		$notificationDetails .= "<TABLE id='topTable'>";
		$notificationDetails .= "<TR>";
		$notificationDetails .= "<TD>";
		
		//if user doesn't have a profile picture use default profile picture
		if(empty($row['imgurl'])){
			$notificationDetails .= "<IMG src='http://sep.esy.es/sep/img_tst/imgs/default_profile.jpg' class='profPicDetail'>";
		}
		else{
			$notificationDetails .= "<IMG src='{$row['imgurl']}' class='profPicDetail'>";
		}
		
		$notificationDetails .= "</TD>";
		$notificationDetails .= "<TD>";
		$notificationDetails .= "<span class='usr_name'>{$row['fname']} {$row['lname']}</span><BR><I>On</I> {$row['shareDate']}";
		$notificationDetails .= "</TD>";
		$notificationDetails .= "</TR>";
		$notificationDetails .= "</TABLE>";
		$notificationDetails .= "<p><IMG src=\"http://sep.esy.es/sep/img_tst/comment_ico.png\"> {$row['description']}</p>";

		$notificationDetails .= "<DIV id='loc_Img'>";

		//if available add a map container to display location
		if(!empty($row['lat'])) {
			$notificationDetails .= "<SPAN>Location</SPAN>";
			$notificationDetails .= "<HR>";
			$notificationDetails .= "<div id='googleMap'></div>";
		}
	}

	//get images of the shared post
	$result=$db_access->getNotificationImages($notific_Id);

	//If there are any images
	if(mysql_num_rows($result)) {
		$notificationDetails .= "<BR>";
		$notificationDetails .= "<SPAN>Images</SPAN>";
		$notificationDetails .= "<HR>";
		$notificationDetails .= "<div class=\"thumbs\">";
		while($row=mysql_fetch_array($result)){
			$notificationDetails .= "<a href=\"{$hostImg}{$row['img_url']}\" style=\"background-image:url({$hostImg}{$row['img_url']})\"></a>";
		}
		$notificationDetails .= "</div>";
	}
	$notificationDetails .= "</DIV>";
	
	//Mark the notification as read
	$db_access->viewedNotification($notific_Id,$uid);
	
	echo $notificationDetails; 
	
}

/*To get details of a users own notification details*/
else if(isset($_GET['getOwnNotific'])){

    //notification id
    $notific_Id = $_GET['getOwnNotific'];

    $result = $db_access->getOwnNotificationDetails($notific_Id);
	
	$notificationDetails="";
	while($row=mysql_fetch_array($result)){

		//if available append the location details to the beginning of the string
		if(!empty($row['lat'])) {
			$notificationDetails .= "***";

			$notificationDetails .= "{$row['lat']}";
			$notificationDetails .= "/";
			$notificationDetails .= "{$row['lon']}";

			$notificationDetails .= "***";
		}

		$notificationDetails .= "<TABLE id='topTable'>";
		$notificationDetails .= "<TR>";
		$notificationDetails .= "<TD>";            
		
		//if user doesn't have a profile picture use default profile picture
		if(empty($row['imgurl'])){
			$notificationDetails .= "<IMG src='http://sep.esy.es/sep/img_tst/imgs/default_profile.jpg' class='profPicDetail'>";
		}
		else{
			$notificationDetails .= "<IMG src='{$row['imgurl']}' class='profPicDetail'>";
		}
		
		$notificationDetails .= "</TD>";
		$notificationDetails .= "<TD>";
		$notificationDetails .= "<span class='usr_name'>{$row['fname']} {$row['lname']}</span><BR><I>On</I> {$row['shareDate']}";
		$notificationDetails .= "</TD>";
		$notificationDetails .= "</TR>";
		$notificationDetails .= "</TABLE>";
		$notificationDetails .= "<p><IMG src=\"http://sep.esy.es/sep/img_tst/comment_ico.png\"> {$row['description']}</p>";

		$notificationDetails .= "<DIV id='loc_Img'>";

		//if available add a map container to display location
		if(!empty($row['lat'])) {
			$notificationDetails .= "<p>Location</p>";
			$notificationDetails .= "<HR>";
			$notificationDetails .= "<div id='googleMap'></div>";
		}

		//actions button for hide and delete a share post
		//Inside the loop is for add data attributes
		$notificationDetails .= "<a href='#popupMenu' data-rel='popup' data-transition='slideup' class='ui-alt-icon ui-btn ui-corner-all ui-btn-inline ui-icon-carat-d ui-btn-icon-notext ui-nodisc-icon' id='actions' data-shareid='{$row['shareId']}' data-ishidden='{$row['hidden']}'>carat-d</a>";
	}
	
	//get images of the shared post
	$result=$db_access->getNotificationImages($notific_Id);
	
	//If there are any images
	if(mysql_num_rows($result)) {
		$notificationDetails .= "<BR>";
		$notificationDetails .= "<SPAN>Images</SPAN>";
		$notificationDetails .= "<HR>";
		$notificationDetails .= "<div class=\"thumbs\">";
		while($row=mysql_fetch_array($result)){
			$notificationDetails .= "<a href=\"{$hostImg}{$row['img_url']}\" style=\"background-image:url({$hostImg}{$row['img_url']})\"></a>";
		}
		$notificationDetails .= "</div>";
	}
	$notificationDetails .= "</DIV>";

	echo $notificationDetails;
	
}

/*Delete a share post*/
else if(isset($_GET['deleteNotific'])){

    $shareId = $_GET['deleteNotific'];

    $db_access->deletePost($shareId);
}

/*To hide and allow a share post*/
else if(isset($_GET['hideNotific'])){

    $shareId = $_GET['hideNotific'];
    $action = $_GET['action'];

    $db_access->hidePost($shareId,$action);
}

else{
	echo "Invalid";
}

?>